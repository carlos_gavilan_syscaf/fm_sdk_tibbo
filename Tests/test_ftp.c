/* ___PROJECT INCLUDES____________________________________________________ */

#include "test_ftp.th"
#include "Net\ftp_client.th"
/* ___LOCAL VARIABLES_____________________________________________________ */
// Global timer tracking variable
tTMR_Handle     hTestftp_TimeoutTimer;

TEST_FTP_DATA testftp_Data;

void vTestFtp_Initialize(){                              
    
    testftp_Data.state = TEST_FTP_INIT;
    testftp_Data.errorCount = 0;
	
	fd.create("test.dat");
	if (fd.laststatus !=PL_FD_STATUS_OK && fd.laststatus !=PL_FD_STATUS_DUPLICATE_NAME){
		sys.debugprint("vTestFtp_Initialize: An Error ocurred while attempt to create a file, code: "+str(fd.laststatus)+ " \n");
		return;
	}
	
	fd.filenum = NUM_FILE_DEFAULT;
	if (fd.open("test.dat") == PL_FD_STATUS_OK){
		fd.setpointer(1);
		fd.transactionstart();
		fd.setdata(INFO);
		if(fd.laststatus != PL_FD_STATUS_OK){
			sys.debugprint("vTestFtp_Initialize: An Error ocurred while attempt to write on test file, code: "+str(fd.laststatus)+ " \n");
			return;
		} 
		fd.transactioncommit();
	}
	
	sys.debugprint("vTestFtp_Initialize()\n");
}


void vTestFtp_Tasks(){

    switch ( testftp_Data.state )
	{
		case TEST_FTP_INIT:
		{
			testftp_Data.state = TEST_FTP_IDLE;
			break;
		}
		
		case TEST_FTP_IDLE:
		{
			if (bTMR_HasTimerExpired(hTestftp_TimeoutTimer) == eTRUE)
            {
				  sys.debugprint("\n\nvTestFtp_Tasks: Its time to upload files!\n");
				  vFTP_Loggin(IVU_USERNAME, IVU_PWD);
				  sys.debugprint("vTestFtp_Tasks: try to Loggin in FTP Server...\n");
				  testftp_Data.state = TEST_FTP_UPLOAD_FILE;
				  hTestftp_TimeoutTimer = hTMR_StartTimer(TEST_FTP_REQUEST_PERIOD);
            }
			break;
		}	
		
		case TEST_FTP_UPLOAD_FILE:
		{
			if (bFTP_isLoggedOn()){
				
				sys.debugprint("vTestFtp_Tasks: Logged. Try to upload file...\n");
				vFTP_StoreFile("test.dat", "test2.dat"); 
				testftp_Data.state = TEST_FTP_WAITING_TRANSFER_ENDS;
			} else if (bFTP_ErrorFlagActive()) {
				sys.debugprint("vTestFtp_Tasks: An error ocurred. Reseting Machine...\n");
				testftp_Data.state = TEST_FTP_INIT;
			}	
				
			break;
		}
			
		case TEST_FTP_WAITING_TRANSFER_ENDS:
		{
			
			if(bFTP_isFileUploaded()){
				sys.debugprint("vTestFtp_Tasks: File uploaded!\n");
				vFTP_Quit();
				sys.debugprint("vTestFtp_Tasks: close session\n");
				testftp_Data.state = TEST_FTP_IDLE;
				
			} else if (bFTP_ErrorFlagActive()) {
				sys.debugprint("vTestFtp_Tasks: An fatal error ocurred\n");
				testftp_Data.state = TEST_FTP_INIT;
			}	
			break;
		}
		
    }
}