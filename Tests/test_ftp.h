#ifndef _TEST_FTP_H
#define _TEST_FTP_H

#define TEST_FTP_REQUEST_PERIOD 500


#include "system_config\system_config.th"
#include "StackFM\timer.th"



// DOM-IGNORE-END 

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application states

  Summary:
    Application states enumeration

  Description:
    This enumeration defines the valid application states.  These states
    determine the behavior of the application at various times.
*/

#define INFO "Ginette - Les Tetes Raides\n\nLa mer ca n's'invente pas\nEt nous on cr�ve � rester l�\nEt le funambule beau qu'il est\nMarchant sur son fil\nCharles il disait l'albatros\nIl en est mort\nA marcher sur la terre\n"


enum TEST_FTP_STATES
{
	// Application's state machine's initial state. 
	TEST_FTP_INIT=0,
	TEST_FTP_IDLE,
	// TODO: Define states used by the application state machine.
            
    TEST_FTP_UPLOAD_FILE,
    TEST_FTP_WAITING_TRANSFER_ENDS

} ;


// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */

struct TEST_FTP_DATA
{
    /* The application's current state */
    TEST_FTP_STATES state; 
	U8 errorCount;
    /* TODO: Define any additional data used by the FMSDKlication. */
} ;


EXTERN PUBLIC void vTestFtp_Initialize();
EXTERN PUBLIC void vTestFtp_Tasks();


#endif 


