/* ___PROJECT INCLUDES____________________________________________________ */

 #include "test_fm.th"

/* ___LOCAL VARIABLES_____________________________________________________ */
MIMO_DATA mimoData_test;

void vTestFM_Tasks(){
 
  vFM_ProcessRxTx();

	switch ( mimoData_test.state )
    {
        /* Application's initial state. */
        case MIMO_STATE_INIT:
        {     
            vFM_Initialize();
            break;
        }
		
		case MIMO_STATE_IDLE:
        {
			//sys.debugprint("----->FM_STATE_IDLE\n");
			if (bTMR_HasTimerExpired(hFM_RequestTimer) == eTRUE)
            {
				  sys.debugprint("\n");
				  hFM_RequestTimer = hTMR_StartTimer(MIMO_REQUEST_PERIOD);
				  hFM_TimeoutTimer_test = hTMR_StartTimer(FM_TIMEOUT);
				  
				  vTSP_RequestReset();
				  mimoData_test.state = MIMO_REQUEST_IGNITION_VALUE;
            }
			
			
            break;
        }
		case MIMO_REQUEST_IGNITION_VALUE:
		{
			if(bALP_RequestAcronymValue(IGNON)==eTRUE){
				hFM_TimeoutTimer_test = hTMR_StartTimer(FM_TIMEOUT);
				mimoData_test.state = MIMO_WAITING_IGNITION_VALUE;
			} else if (bTMR_HasTimerExpired(hFM_TimeoutTimer_test) == eTRUE){
				sys.debugprint("vFM_Tasks: Timeout occured while waiting try to connect to FM\n");
				mimoData_test.state = MIMO_ERROR;
      		}
			break;
		}
		
        case MIMO_WAITING_IGNITION_VALUE:
        {
            
			U32 pU32Acronym;
            if (bALP_GetAcronymValue(&pU32Acronym, &U32IgnValue_test) == eTRUE){
			
				mimoData_test.state = MIMO_EVAL_TRIP_STATE;
      		} else if (bTMR_HasTimerExpired(hFM_TimeoutTimer_test) == eTRUE){
				sys.debugprint("vFM_Tasks: Timeout occured while waiting response from FM\n");
				mimoData_test.state = MIMO_ERROR;
      		}
            break;
        }
		
		case MIMO_EVAL_TRIP_STATE:
		{
			mimoData_test.state = MIMO_STATE_IDLE;
			
			if(U32IgnValue_test == 1 && (mimoData_test.tripState == UNDEFINED || mimoData_test.tripState == TRIP_ENDED)){
				mimoData_test.tripState = TRIP_INPROGRESS;
				sys.debugprint("vFM_Tasks: Trip In progress\n");
				
			}

			if(U32IgnValue_test == 0 && mimoData_test.tripState == TRIP_INPROGRESS){
				mimoData_test.tripState = TRIP_ENDED;
				sys.debugprint("vFM_Tasks: Trip Ended\n");
				
			}
			
			break;
					

		}
		
		case MIMO_ERROR:
        {
			if (mimoData_test.errorCount > 3){
				sys.debugprint("vFM_Tasks: Three errors, Machine Reset!\n");
				//sys.reboot();
				mimoData_test.state = MIMO_STATE_INIT;
			} else {
				mimoData_test.errorCount++;
				sys.debugprint("vFM_Tasks: goto Init, error count: "+ str(mimoData_test.errorCount) +"\n");
				mimoData_test.state = MIMO_STATE_IDLE;
			}
			
            break;
        }

    }
}
