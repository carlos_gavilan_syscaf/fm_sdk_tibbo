
MIMO_firmware para ETM Cali, v3.0, 
Departamento IDI, SYSCAF SAS
26/01/2016
 
CONTENIDo
I.	�COMO COMPILAR EL FIRMWARE?
II.	REQUERIMIENTOS MINIMOS DEL SISTEMA
III.	SOPORTE TECNICO

I. �COMO COMPILAR EL FIRMWARE?

1. Dirigase a la siguiente direccion y descargue la version mas
   reciente del software TIDE. http://tibbo.com/downloads.html#tide
2. Instale la aplicacion.
3. Abra en su computadora el archivo "MIMO_2015.tpr" (sin comillas)
   que se encuentra en el mismo directorio de este archivo.

Version "Debug"
4. Si desea compilar una version de depuracion asegurese que la lista
   desplegable se encuentre seleccionada la opcion "debug".
5. Busque el archivo "debug.th" y asegurese que la linea 8, no se
   encuentre comentada (#define DBG_MODE 1) 
6. Dirigase al menu "Debug > Run" o presione la tecla F5

Version "Release"
7. Si desea compilar una version de produccion asegurese que la lista
   desplegable se encuentre seleccionada la opcion "release".
8. Busque el archivo "debug.th" y asegurese que la linea 8, se
   encuentre comentada (//#define DBG_MODE 1) 
9. Dirigase al menu "Debug > Run" o presione la tecla F5

Recuerde que puede realizar depuracion en version de produccion o "Release"
utilizando el software de depuracion mimo "debug_mimo".

II. REQUERIMIENTOS MINIMOS DEL SISTEMA

Windows
* Windows XP, 7
* Minimum 1 GB of RAM
* Screen resolution 800x600 or higher


III. SOPORTE TECNICO

Si usted necesita asistencia tecnica, puede contactarse con nuestro
departamento de investigacion, desarrollo e innovacion  dentro de syscaf.

1. Correo Coordinador IDI:    carlos.gavilan@syscaf.com.co
2. Correo Ingeniero Hardware: alejandro.morales@syscaf.com.co

