#ifndef __SERIAL_H__
#define __SERIAL_H__
/* ==========================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        Serial Communication functions
AUTHOR:           Pieter Conradie
DESCRIPTION:      These functions should be ported according to the particular
                  Operating System in use, e.g. a different "serial.c" or "serial.cpp"
                  should be supplied for DOS, Windows, PalmOS, ...

VERSION:          $Revision: 2 $
DATED:            $Date: 11/01/01 14:24 $
LAST MODIFIED BY: $Author: Pieterc $

========================================================================== */

/* ___PROJECT INCLUDES____________________________________________________ */
#include "..\System\cifms.h"
#include "..\System\system_config.h"

#define  br1200		0
#define  br2400 	1
#define  br4800 	2
#define  br9600 	3
#define  br19200	4

/* ___PUBLIC FUNCTION PROTOTYPES__________________________________________ */
EXTERN PUBLIC tBOOL bSERIAL_Open    (int iCommPort, int iBaudRate);
EXTERN PUBLIC tBOOL bSERIAL_Close   ();
//EXTERN PUBLIC U16   U16SERIAL_RxData(U8* pU8Buffer, U16 U16Count);
//EXTERN PUBLIC void  vSERIAL_TxData  (U8* pU8Buffer, U16 U16Count);


#endif
