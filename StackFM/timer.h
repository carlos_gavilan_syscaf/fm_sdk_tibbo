#ifndef __TIMER_H__
#define __TIMER_H__
/* ==========================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        Timer
AUTHOR:           P.J. Conradie
DESCRIPTION:      This component provides a timeout facility

VERSION:          $Revision: 2 $
DATED:            $Date: 2/02/01 15:04 $
LAST MODIFIED BY: $Author: Pieterc $

========================================================================== */

/* ___PROJECT INCLUDES____________________________________________________ */
#include "..\System\cifms.h"


/* ___TYPE DEFINITIONS____________________________________________________ */
typedef U32 tTMR_Handle;

/* ___PUBLIC FUNCTION PROTOTYPES__________________________________________ */
EXTERN PUBLIC tTMR_Handle hTMR_StartTimer(U32 U32TimeoutInMilliseconds);
EXTERN PUBLIC tBOOL       bTMR_HasTimerExpired(tTMR_Handle hTimer);
EXTERN PUBLIC void        vTMR_Tick();

#endif
