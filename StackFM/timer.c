/*
 * timer.c
 *
 *  Created on: 6/08/2015
 *      Author: Alejandro.Morales
 */

/* ___STANDARD INCLUDES___________________________________________________ */

/* ___PROJECT INCLUDES____________________________________________________ */
#include "timer.h"

static U32 clock = 0;

/* ___PUBLIC FUNCTIONS____________________________________________________ */
PUBLIC tTMR_Handle hTMR_StartTimer(U32 U32TimeoutInCents)
{

   return (tTMR_Handle)(clock + U32TimeoutInCents);
}

PUBLIC tBOOL bTMR_HasTimerExpired(tTMR_Handle hTimer)
{
	if( clock >= hTimer)
   {
      return eTRUE;
   }
   else
   {
      return eFALSE;
   }
}

PUBLIC void vTMR_Tick()
{
	clock++;
}
