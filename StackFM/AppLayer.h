#ifndef __APP_LAYER_H__
#define __APP_LAYER_H__
/* ==========================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        AppLayer
AUTHOR:           Carlo Putter
DESCRIPTION:      Application layer implementation

VERSION:          $Revision: 38 $
DATED:            $Date: 01/03/28 11:17 $
LAST MODIFIED BY: $Author: Carlop $
 *

========================================================================== */

/* ___STANDARD INCLUDES___________________________________________________ */
#include "..\System\cifms.h"
#include "..\System\debug.h"

/* ___PROJECT INCLUDES____________________________________________________ */
#include "Transprt.h"

/* ___DEFINITIONS ________________________________________________________ */
#define SIZE_PACKET 256
#define NUM_FILE_DEFAULT 1
//FM Download types
#define ALP_QUICK            0                   //Quick download type
#define ALP_FULL             1                   //Full download type
#define ALP_TIME_WINDOW      2                   //Download data between start and end time
#define ALP_TIME_START       3                   //Download data from a start time to now
//...

//FM Memory types (DumpCommsnd)
#define ALP_MEMORY_NVRAM      0                   //RTC non volatile memory read
#define ALP_MEMORY_RAMROM     1                   //FM micro RAM/ROM/FLASH memory


//Application layer error codes (U32)
#define ALP_ERROR_INVALID_CONFIG_SIZE 0x00000001 //The size of the uploaded data did not match
#define ALP_ERROR_INVALID_DDR_SIZE    0x00000002  //The size of the uploaded ddr dows not match the specified

#define FM_MD			257684877

#define IGNON			306279959 

#define SPDKH			476370193

#define VEHID			523834509 

#define TIMES			491348892
/*  == == == == == == == == == Protocol IDs == == == == == == == == == ==  */
/*   -- -- -- -- -- -- --  Protocol Version Info -- -- -- -- -- -- -- --   */
/*
  After a reception of a RESET command, the version information of the
  protocols is transmitted.
*/
#define ALP_DA_VERSION_INFO  0                   //Data Layer version information
/*
  Data : aU8, [U8TranportProtocolVersion|U8ApplicationLayerVersion]
*/

/* -- -- -- -- -- -- -- -- -- Get FM200 info -- -- -- -- -- -- -- -- -- -- */
/*
  An instant information structure can be requested from the FM200 at any
  time during the active session
*/
//Request:
#define ALP_RQ_INFO           1                   //Request FM200 Info
/*
  Parameters : [None]
*/
//Response-Size:.
#define ALP_SZ_INFO           2                   //FM200 Info Size packet
/*
  Data : U32, U32Size
*/
//Response-Data:.
#define ALP_DA_INFO           3                   //FM200 Info Data packet
/*
  Data : [tTSP_FM200Info], 1 byte packing
  DataEnd : Emty packet, {Count==0}
*/


/* -- -- -- -- -- -- -- -- Events data downloading -- -- -- -- -- -- -- -- */
/*
  The procedure is:
  1. Know the vehicle ID (if unknown, request with mALP_RqInfo
  2. Request the Events DDR version (request with mALP_RqDDRVersion, D#=12)
  3. Do a Get_Config(DownloadType)
  4. Do a Get_Events(DownloadType)
  5. Fill in the file header for DMI processing
*/
/* -- -- -- -- -- -- -- -- -- -- Get Config  -- -- -- -- -- -- -- -- -- -- */
//Request:.
#define ALP_RQ_CONFIG         4
//  Parameters : U8, {[mALP_Quick],[mALP_Full]}

//Response-Size:.
#define ALP_SZ_CONFIG         5
//  Data : U32, U32Size

//Response-Data:.
#define ALP_DA_CONFIG         6
//  Data : aU8, |........|
//  DataEnd : Emty packet, {Count==0}

/* -- -- -- -- -- -- -- -- -- -- Get Events  -- -- -- -- -- -- -- -- -- -- */
//Request:.
#define ALP_RQ_EVENTS         7                   //Request Events (Params = Download type)
//  Parameters : U8, {[mALP_Quick],[mALP_Full]}

//Response-Size:.
#define ALP_SZ_EVENTS         8                   //Events Size packet
//  Data : U32, U32Size

//Response-Data:.
#define ALP_DA_EVENTS         9                   //Events Data packet
//  Data : aU8, |........|
//  DataEnd : Emty packet, {Count==0}

/* -- -- -- -- -- -- -- -- Tacho data downloading  -- -- -- -- -- -- -- -- */
/*
  The procedure is:
  1. Know the vehicle ID (if unknown, request with mALP_RqInfo
  2. Request the Tacho DDR version (request with mALP_RqDDRVersion, D#=10)
  3. Do a Get_Tacho(DownloadType)
  4. Fill in the file header for DMI processing
*/
/* -- -- -- -- -- -- -- -- -- -- Get Tacho - -- -- -- -- -- -- -- -- -- -- */
//Request:.
#define ALP_RQ_TACHO          10                  //Request Tacho data (Params = Download type)
//  Parameters :{
//                U8          [mALP_Full],
//                U8|U32|U32  [mALP_TimeWindow | U32StartTime | U32EndTime],
//                U8|U32      [mALP_TimeStart  | U32StartTime]
//              }

//Response-Size:.
#define ALP_SZ_TACHO         11
//  Data : U32, U32Size

//Response-Data:.
#define ALP_DA_TACHO         12
//  Data : aU8, |........|
//  DataEnd : Emty packet, {Count==0}

/* -- -- -- -- -- -- -- Events configuration Uploading - -- -- -- -- -- -- */
/*
  For the purposes of the managenent of a vehicle fleet it would be very easy
  to update the config of all the fleet vehicles over the air...
  Process of uploading a new config:
  1. Check if the vehicle is armed, (RqInfo)
  2. Check if the FM is ready to receive the new config, (mALP_RqNewConfigStart)
  3. When the FM responded with (mALP_RqNewConfigReady) it is ready
  4. Start the upload of the new config data block, (mALP_SzNewConfig,mALP_DaNewConfig)
*/
//Request:.
#define ALP_RQ_NEW_CONFIG_START 13
//  Parameters : [None]

//Request:.
#define ALP_RQ_NEW_CONFIG_READY 14
//  Parameters : [U8 NotReady] {NotReady == 0 -> Ready}

//Response-Size:.
#define ALP_SZ_NEW_CONFIG      15
//  Data : U32, U32Size

//Response-Data:.
#define ALP_DA_NEW_CONFIG      16
//  Data : aU8, |........|
//  DataEnd : Emty packet, {Count==0}

//Complete actions
#define ALP_RQ_NEW_CONFIG_DONE 34
//  Parameters : [None]

/* -- -- -- -- -- -- -- -- -- DeviceDriver Uploading  -- -- -- -- -- -- -- */
/*
  To load new devicedrivers on the FM unit. This should be the last action
  of the session due to the fact that the FM would "reboot" to load the new
  drivers.
  Process of uploading new DDRs
  1. Check if the vehicle is armed, (RqInfo)
  2. Get the DDR version numbers (mALP_RqDDRVersion)
  3. Do a dependancy check before upload starts
  4. Check if the FM is ready to receive the new config, (mALP_RqDDRStart)
  5. When the FM responded with (mALP_RqDDRReady) it is ready
  6. Upload a DDR (mALP_SzNewDDR,mALP_DaNewDDR)
  7. Repeat step 4. until all requested drivers is loaded
  8. Signal to the FM that all DDRs is loaded (mALP_RqDDREnd)
*/
//Request:.
#define ALP_RQ_NEW_DDRS_START 17
//  Parameters : [None]

//Request-Response:.
#define ALP_RQ_NEW_DDRS_READY 18
//  Parameters : [U8 NotReady] {NotReady == 0 -> Ready}

//Request/ RequestResponse:.
#define ALP_RQ_START_DDR      36
//  Parameters : [None]
//  Called from the Server (PC) when a new driver upload is going to be started
//  Response, when the FM is ready to receive the new driver

//Response-Size:.
#define ALP_SZ_NEW_DDR        19
//  Data : U32, U32Size

//Response-Data:.
#define ALP_DA_NEW_DDR        20
//  Data : aU8, |........|
//  DataEnd : Emty packet, {Count==0}

//Request:.
#define ALP_RQ_DDRS_UPDATE    21
//  Parameters : [None]
//Called when all drivers is uploaded an FM may continue to upgrade firmware

/* -- -- -- -- -- -- DeviceDriver Descryption/Header request - -- -- -- -- */
/*
  To check the specific version and details of a DDR, its header can be
  requested
*/
//Request:.
#define ALP_RQ_DDR_DESCRIPTION 22
//  Parameters : U8, U8DDRNumber

//Response-Size:.
#define ALP_SZ_DDR_DESCRIPTION 23
//  Data : U32, U32Size

//Response-Data:.
#define ALP_DA_DDR_DESCRIPTION 24
//  Data : aU8, |........|
//  DataEnd : Emty packet, {Count==0}

/* -- -- -- -- -- -- -- -- Verify/Login to the vehicle - -- -- -- -- -- -- */
/*
  For security reasons the FM can be protected from unauthorised users
  The security levels can be set via the config.
  Levels of security:
  0. No security required
        [All Access]
  1. Vehicle ID Security
        Mismatch                : [No Access]
        Vehicle ID match        : [All Access]
  2. Site and Vehicle ID Security(Options)
        All Mismatch            : [No Access]
        Vehicle ID match        : [Vehicle info]
        Site & Vehicle ID match : [All Access]
  3. Pasword Security(Options)
        All Mismatch            : [No Access]
        Vehicle ID match        : [Vehicle info]
        Site & Vehicle ID match : [All Access, No uploads]
        16 byte Hashed password, Site & Vehicle ID macth
                                : [All Access]
  The options can be set to either allow partial access if some of the IDs
  pass, or access is only granted when all passwords and IDs match
*/
//Request:.
#define ALP_RQ_LOGIN         25
//  Parameters : U16, U32, U128 [U16VehicleID, U32SiteID, U128HashedPassword]

//Response-Data:.
#define ALP_DA_LOGIN          26
//  Data : U8, U8, U8 [U8VehicleIDPass, U8SiteIDPass, U8PasswordPass]

/* -- -- -- -- -- Request the loaded DeviceDriver versions  -- -- -- -- -- */
/*
  The devicedriver versions is needed to interpret the events and Tacho data
*/
//Request:.
#define ALP_RQ_DDR_VERSION    27
//  Parameters : [None]

//Response-Data:.
#define ALP_DA_DDR_VERSION     28
//  Data : aU8[32], |D0|D1|....|D30|D31|
//  If no driver loaded at the location -> value == 0

/* -- -- -- -- -- -- -- -- -- -- Do A DDR call  -- -- -- -- -- -- -- -- -- */
/*
  A requested devicedriver call can be executed,
  the result can either be a U32 or an block of data

  if a U32 result is requested, then the result is returned with
    mALP_DaDDRCallResult
  if a Block result is requested, then the result is returned with
    mALP_SzDDRCallBlkRes, mALP_DaDDRCallBlkRes
*/
//Request:.
#define ALP_RQ_DDR_CALL        29
//  Parameters : U8, U8, U32, U32, U16 [U8DDriverNo, U8DDAssignment, U32Param1, U32Param2, U16Param3]

//Response-Data:.
#define ALP_DA_DDR_CALL        30
//  Data : U32, U32DDRresult

/* -- -- -- -- -- -- -- Request a FM memory dump - -- -- -- -- -- -- -- -- */
/*
  Use:
  1. Reading a DDR result block
  2. Unimplemented functionality
  3. Debugging
*/
//Request:.
#define ALP_RQ_MEM_DUMP        31
//  Parameters : U8, U32, U32 [U8MemoryType|U32StartLocation|U32DumpSize]
// NVRAM, can only read 1 U32 value at a time!

//Response-Size:.
#define ALP_SZ_MEM_DUMP        32
//  Data : U32, U32Size

//Response-Data:.
#define ALP_DA_MEM_DUMP        33
//  Data : aU8, |........|
//  DataEnd : Emty packet, {Count==0}


//Backword referenced defines
//#define mALP_RqNewConfigDone  34


/* -- -- -- -- -- -- -- Request a new driver logon & disarm -- -- -- -- -- */
/*
  Use:
  1. Identify a new driver
  2. disarm the vehicle
*/
//Request:.
#define ALP_RQ_ID_NEW_DRIVER  35
//  Parameters : U16 [U16NewDriverID]

//Backword referenced defines
//#define ALP_RQ_START_DDR    36

/* -- -- -- -- -- -- --  Get specific device info  -- -- -- -- -- -- -- -- */
/*
  An instant information structure can be requested from the FM200 at any
  time during the active session
*/
//Request:.
#define ALP_RQ_GPS_INFO       37                  //Request GSP info block from the FM200
/*
  Parameters : [None]
*/
//Response-Size:.
#define ALP_SZ_GPS_INFO       38                  //GPS data size packet
//  Data : U32, U32Size
//Response-Data:.
#define ALP_DA_GPS_INFO       39                  //GPS data packet
/*
  No size specified (Data should be smaller than the packet size !!! {HDLC_BUFFER_SIZE}
  Data : [tTSP_FM200GPSInfo], 1 byte packing
  DataEnd : Emty packet, {Count==0}
*/

/* -- -- -- -- -- -- -- -- -- Data block status -- -- -- -- -- -- -- -- -- */
// Data block upload complete
#define ALP_DA_BLOCK_COMPLETE  254

/* -- -- -- -- -- -- -- -- -- ERROR reporting - -- -- -- -- -- -- -- -- -- */
/*
  Communication errors can be reported with a list of error codes (U32)
*/
//Data:.
#define ALP_DA_ERROR          255
//  Data : U32, U32ErrorCode


// Temporary buffer for data
#define ALP_DOWNLOAD_BUFFER_SIZE 255 //1024
/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */

#define ALP_RQ_ACRONYM_VALUE 44

/* -- -- -- -- -- -- -- -- User Command to Config  -- -- -- -- -- -- -- -- */
/*
  The FM is requested to add an Asynchronous command to the config command
  processing queue. The config would then process the command and could
  return a result (currently only a U32 value)
*/
//Request:.
#define ALP_RQ_NEW_COMMAND_TO_CONFIG 43                  //Request to add a new command
/*
Package payload:
   [CMND-ID][U32Param1][U32Param2][U32Param3]
Response payload:
   [CMND-ID][RES-TYPE]
                 [  0][NULL]
                 [  1][U32Result1]
                 [255][U32ErrorCode]
                               [  0] Unhandled command that timeout out
*/


/* -- -- -- -- -- -- -- -- -- Set FM Date-Time  -- -- -- -- -- -- -- -- -- */
/*
  The FM is requested to change its date and time to the new values specified
*/
//Request:.
#define ALP_SET_DATE_TIME     40                  //Request to set new date-time on the FM200
/*
  Parameters : [YY YY MM DD HH MM SS]
*/
//Response:.
// Use same value to confirm setting              //Confirmation of Date-Time set
/*
   Data : [Ctime], 4 bytes
*/

/* -- -- -- -- -- Commands to FM200, executed by Config  -- -- -- -- -- -- */
   
#define ALP_CQ_DISARM_VEHICLE_ID       35                //Request to disarm vehicle with specified ID
   //Param1 == New U16driver ID
   
#define ALP_CQ_SET_DATE_TIME           40                //Request to set vehicle date time
   //Param1 == Date [00YYMMDD]
   //Param2 == Time [00HHMMSS]
   
/*struct tALP_NewDateTime
{
   U8    U8Day;
   U8    U8Month;
   U8    U8Year;
   U8    U8Century;  
   U8    U8Sec;
   U8    U8Min;
   U8    U8Hour;
   U8    U8Blank;
};*/

/* File header for Download Data*/
struct tALP_DownloadFileHeader
{
   U8  U8GenerationFormatVersion;
   U8  U8SoftwareVersion;
   U8  U8CodePlugProductID;
   U8  U8VehicleNumberLSB;
   U8  U8VehicleNumberMSB;
   U8  U8StartPageEventData;
   U8  U8EndPageEventData;
   U8  U8Checksum_EventData;
   U8  U8StartPageEventData_Backup;
   U8  U8EndPageEventData_Backup;
   U8  U8Checksum_EventData_Backup;
   U8  U8EndPageEvents;
   U8  U8Checksum_EndPageEvents;
   U8  U8StartOfDdms_1;
   U8  U8StartOfDdms_2;
   U8  U8StartOfDdms_3;
   U8  U8Checksum_StartOfDdms;
   U8  U8StartOfEvents_1;
   U8  U8StartOfEvents_2;
   U8  U8StartOfEvents_3;
   U8  U8Checksum_StartOfEvents;
};

#define ALP_FILE_HEADER_PREDEFINED_VALUES           \
    {                                               \
     0x10,0x10,0x01,      /* Version Info */        \
     0x00,0x00,           /* Vehicle ID */          \
     0x01,0x00,0x00,      /* Start,End Page, CRC */ \
     0x01,0x00,0x00,      /* Backup */              \
     0x00,0x00,           /* End Page of Events */  \
     0x11,0x00,0x00,0x17, /* Start of DDM's */      \
     0x00,0x04,0x00,0x0A  /* Start of Events */     \
    }


enum tALP_DownloadType
{
   ALP_QUICK_EVENT_DOWNLOAD                = 0x01,
   ALP_FULL_EVENT_DOWNLOAD                 = 0x02,
   ALP_FULL_TACHO_DOWNLOAD                 = 0x03,
   ALP_WINDOWED_TACHO_DOWNLOAD             = 0x04,
   ALP_QUICK_TACHO_DOWNLOAD                = 0x05,
   ALP_BLOCK_EVENT_DOWNLOAD                = 0x06
};

struct tALP_DownloadParams{
   U8    U8DownloadType;
   U32   U32Param1;
   U32   U32Param2;
   U8    U8ParamSize;
};

struct ALP_tAcronymData{
   U32   U32Acronym;
   U32   U32Value;
};


struct ALP_tCommandToConfig{
   U8    U8CommandId;
   U32   U32Parameter1;
   U32   U32Parameter2;
   U32   U32Parameter3;
};

struct ALP_tCommandResult{
   U8   U8CommandID;
   U8 	U8ResultType;
   U32  U32ResultValue;
};

/* ___GLOBAL VARIABLES____________________________________________________ */


/* ___PUBLIC FUNCTION PROTOTYPES__________________________________________ */
EXTERN void vALP_Init           ();
EXTERN void vALP_OnNewPacket    (U8* pU8Data, U16 U16Count);
EXTERN void vALP_OnSendTimeout  ();
EXTERN void vALP_ProtocolVersions(U8* pU8Data, U16 U16Count);

//EXTERN void vALP_ContiniousEntry();

EXTERN tBOOL bALP_SaveBufferedData();
EXTERN void  vALP_ProcessTachoFile();
EXTERN void  vALP_ProcessEventFile();
EXTERN tBOOL bALP_BufferDownloadedData(U8* pU8Buffer, U16 U16NumberOfBytes);

EXTERN PUBLIC tBOOL bALP_StartDownload(string szFileName, tALP_DownloadType eDownloadType);
EXTERN PUBLIC tBOOL bALP_HasFileDownloaded();

EXTERN PUBLIC tBOOL bALP_RequestAcronymValue(U32 U32Acro);
EXTERN PUBLIC tBOOL bALP_GetAcronymValue(U32 *pU32Acronym, U32 *pU32Value);

EXTERN PUBLIC tBOOL bALP_HasCommsBeenEstablished();
EXTERN PUBLIC tBOOL bALP_HasErrorOcurred();
EXTERN PUBLIC tBOOL bALP_HasTimeConfirmed();
EXTERN PUBLIC tBOOL bALP_HasDriverLogged();
//EXTERN PUBLIC void  vALP_DownloadInfo           ();

EXTERN PUBLIC tBOOL bALP_SetDateTime(U32* pU32Date, U32* pU32Time);
EXTERN PUBLIC tBOOL bALP_IdentifyDriver(U16 U16NewDriverId);

EXTERN PUBLIC void setALP_VehicleIDValue(U16 U16VehiID);
EXTERN PUBLIC U16  getALP_VehicleIDValue();
/* ___MACROS______________________________________________________________ */

#endif
