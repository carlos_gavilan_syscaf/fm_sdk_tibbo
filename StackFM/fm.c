#include "fm.h"

tTMR_Handle   hFM_TimerTick;

void vFM_Initialize(){							 	
	
	 // Start 1/16th Second Timer
	hFM_TimerTick = hTMR_StartTimer(6);

	// Initialise HDLC Layer
	vHDLC_Init();
	// Initialise Transport Layer
	vTSP_Init();
	// Initialise Application Layer
	vALP_Init();
			
}

void vFM_Reset(){							 	
	
	// Initialise HDLC Layer
	vHDLC_Init();
	// Initialise Transport Layer
	vTSP_Init();
	// Initialise Application Layer
	vALP_Init();
			
}

void vFM_ProcessRxTx( )
{
   
   U8* pU8Data;
   U8  U8Count;
   U8  U8Data;
				
   // See if timer tick has expired
   if(bTMR_HasTimerExpired(hFM_TimerTick) == eTRUE)
   {
      // See if there is a new packet to be sent on the Transport Layer
      vTSP_GetNextPacket((U32*)&pU8Data, &U8Count);
      if (U8Count != 0)
      {
         // Send frame on HDLC Layer
         vHDLC_TxBuffer(pU8Data, U8Count);
      }
	  //Timer tick ocurred
	   vTSP_OnTimerTick();
      // Continue 1/16th Second Timer
      hFM_TimerTick = hTMR_StartTimer(6);
   }
   
  
}

void vFM_SetDateTime(U32* bcd_date, U32* bcd_time){
	vTSP_RequestReset();
	bALP_SetDateTime(bcd_date, bcd_time);
	
}

/*void vFM_IdentifyDriver(U16 U16NewDriverId)
{
	
	vTSP_RequestReset();
	bALP_IdentifyDriver(U16NewDriverId);
}

tBOOL bFM_HasDriverLoggedInFM()
{
	return bALP_HasDriverLogged();
}*/

tBOOL bFM_HasTimeSetOnFM()
{
	return bALP_HasTimeConfirmed();
}

void vFM_SleepListeners()
{
	ser.num = SER_NUM_FM;
	ser.enabled = NO;
}

void vFM_WakingListeners()
{
	ser.num = SER_NUM_FM;
	ser.enabled = YES;
}

