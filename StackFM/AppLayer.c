/* ==========================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        AppLayer
AUTHOR:           Carlo Putter
DESCRIPTION:      Application layer implementation

VERSION:          $Revision: 5 $
DATED:            $Date: 16/02/01 12:17 $
LAST MODIFIED BY: $Author: Pieterc $
HISTORY:          $Log: /products/COMMS/Software/WinSDK/AppLayer.c $
 *
 * 5     16/02/01 12:17 Pieterc
 *
 * 4     2/02/01 15:04 Pieterc
 *
 * 3     11/01/01 14:38 Pieterc
 *
 * 2     11/01/01 14:24 Pieterc

========================================================================== */

/* ___STANDARD INCLUDES___________________________________________________ */

/* ___PROJECT INCLUDES____________________________________________________ */
#include "AppLayer.h"


/* ___LOCAL VARIABLES_____________________________________________________ */
U32   U32ALP_BytesReceived      = 0;
U32   U32ALP_BlockSize          = 0;
U32   U32ALP_TachoSize			= 0;
U32   U32ALP_EventsSize			= 0;
U32   U32ALP_ReceivedTime		= 0;



tBOOL bALP_ValidInfoFlag       	 = eFALSE;
tBOOL bALP_CommsEstablishedFlag	 = eFALSE;
tBOOL bALP_FileDownloadedFlag	 = eFALSE;
tBOOL bALP_ErrorOcurredFlag		 = eFALSE;
tBOOL bALP_TimeConfirmedFlag 	 = eFALSE;
tBOOL bALP_DriverLoggedFlag	 	 = eFALSE;

U8  aU8ALP_DateTimeUpdate[9];
   
U8 	  U8Trprt_v;
U8 	  U8App_v;
U8	  pALP_DownloadFile;//FILE* pALP_DownloadFile;

string  U8ALP_DownloadBuffer;

U16   U16ALP_VehicleID		= 0;
U16   U16ALP_DriverID		= 0;

U16   U16ALP_DownloadBufferIndex;

tALP_DownloadParams 	xALP_DownloadParams;
ALP_tAcronymData 		xALP_AcronymData;
ALP_tCommandToConfig 	xALP_CommandToConfig;
ALP_tCommandResult 		xALP_CommandResult;

U32   ALP_U32AcronymToRequest;

/* ___PRIVATE FUNCTIONS____________________________________________________ */

U16 fwrite_packet(U8 *pBuffer, U8 U8Length, U8 U8fileNum);

U16 fwrite_blank(U16 U16Length, U8 U8fileNum);

tBOOL bALP_CommandToFm(U32 U32CommandId, U32 U32Parameter1, U32 U32Parameter2, U32 U32Parameter3);

/* ___PUBLIC FUNCTIONS____________________________________________________ */
PUBLIC void vALP_Init()
{
   U32ALP_BytesReceived      = 0;
   U32ALP_BlockSize          = 0;
   bALP_ValidInfoFlag        = eFALSE;
   bALP_CommsEstablishedFlag = eFALSE;
   bALP_FileDownloadedFlag  = eFALSE;
   bALP_ErrorOcurredFlag = eFALSE;
   // Reset FM200's communication layers
   vTSP_RequestReset();
   //vDBG_LogState(StackFM_AppLayer, Info, "Init OK");
}

PUBLIC void vALP_OnNewPacket(U8* pU8Data, U16 U16Count)
{
   
   // Store the Packet ID and strip from buffer
   U8 U8PacketID = *pU8Data++; U16Count--;

   switch(U8PacketID)
   {
   case ALP_DA_VERSION_INFO:
      // Received version info from FM200 in response to reset command
     
	  U8Trprt_v = *pU8Data++;
	  U8App_v   = *pU8Data++;
	  //vDBG_LogState(StackFM_AppLayer, Info,"FM Transport Layer version = "+str(U8Trprt_v)+ "; Application Layer = "+str(U8App_v));
      bALP_CommsEstablishedFlag = eTRUE;
      break;

      /* ----------------------- FM200 Events Data ------------------------------ */
	 case ALP_SZ_EVENTS:                                   //Events Size packet
		U32ALP_BytesReceived = 0;
		U32ALP_BlockSize     = *((U32*)pU8Data);

		/*if(eALP_State != ALP_STATE_DOWNLOAD_INITIATED)
		{
		   mDBG_PRINT((DBG_PROG, DBG_FL, "Unhandled Event Size Received\n"));
		   break;
		}*/
		U16ALP_DownloadBufferIndex = 0;
		
		vDBG_LogState(StackFM_AppLayer, Info,"Event Size Received : "+lstr(U32ALP_BlockSize)+"bytes");
		bALP_FileDownloadedFlag  = eFALSE;
		//eALP_State = ALP_STATE_DOWNLOAD_EVENT;
		break;

	 case ALP_DA_EVENTS:
		if (pALP_DownloadFile == NULL) return;

		//Events Data packet
		if(U16Count != 0)
		{
		   // Commit data to file
		   if (bALP_BufferDownloadedData(pU8Data, U16Count) == eFALSE) return;
		   // Track size of Event Data
		   U32ALP_BytesReceived += U16Count;
		   //vDBG_LogState(StackFM_AppLayer, Info,"Bytes received: "+lstr(U32ALP_BytesReceived));
		}
		else
		{
		   // Make sure everything reported was received
		   if (U32ALP_BytesReceived != U32ALP_BlockSize)
		   {
			  // Did not receive whole block
			  vDBG_LogError(StackFM_AppLayer, ErrUnableToRead, "Event Data incomplete. Only received "+lstr(U32ALP_BytesReceived)+" of "+lstr(U32ALP_BlockSize)+" bytes");
			  bALP_ErrorOcurredFlag = eTRUE;
			  return;
		   }

		   // Commit data to file
		   if (bALP_SaveBufferedData() == eFALSE) return;

		   // All event data has been received
		   U32ALP_EventsSize = U32ALP_BytesReceived;
		   vDBG_LogState(StackFM_AppLayer, Info,"All Event Data Received\n");
		   // After Event Block prepend header, close the file and indicate success
		   vALP_ProcessEventFile();
		   bALP_FileDownloadedFlag  = eTRUE;
		}
		break;

      /* ----------------------- FM200 TACHO Data ------------------------------- */
	 case ALP_SZ_TACHO:                                    //Tacho Size packet
		//Tacho Size packet
		U32ALP_BytesReceived = 0;
		U32ALP_BlockSize     = *((U32*)pU8Data);

		/*if(eALP_State != ALP_STATE_DOWNLOAD_INITIATED)
		{
		   vDBG_LogState(StackFM_AppLayer, Info,"Unhandled: Tacho Size Received\n");
		   break;
		}*/
		U16ALP_DownloadBufferIndex = 0;
		vDBG_LogState(StackFM_AppLayer, Info,"Tacho Size Received : "+str(U32ALP_BlockSize)+" bytes\n");
		bALP_FileDownloadedFlag  = eFALSE;
		//eALP_State = ALP_STATE_DOWNLOAD_TACHO;
		break;

	 case ALP_DA_TACHO:
		// Tacho Data packet
		if (pALP_DownloadFile == NULL) return;

		// Tacho Data packet
		if(U16Count != 0)
		{
		   // Commit data to file
		   if (bALP_BufferDownloadedData(pU8Data, U16Count) == eFALSE) return;
		   // Track size of Event Data
		   U32ALP_BytesReceived += U16Count;
		}
		else
		{
		   // Make sure everything reported was received
		   if (U32ALP_BytesReceived != U32ALP_BlockSize)
		   {
			  // Did not receive whole block
			  //vALP_LogDownloadError(ALP_ERR_DOWNLOAD_INCOMPLETE);
			  vDBG_LogError(StackFM_AppLayer, ErrUnableToRead, "Tacho Data incomplete. Only received "+str(U32ALP_BytesReceived)+" of "+str(U32ALP_BlockSize)+" bytes\n");
			  bALP_ErrorOcurredFlag = eTRUE;
			  return;
		   }

		   // Commit data to file
		   if (bALP_SaveBufferedData() == eFALSE) return;

		   // All event data has been received
		   U32ALP_TachoSize = U32ALP_BytesReceived;
		   vDBG_LogState(StackFM_AppLayer, Info,"All Tacho Data Received");

		   // After Event Block prepend header, close the file and indicate success
		   vALP_ProcessTachoFile();
		   bALP_FileDownloadedFlag  = eTRUE;
		}
		break;

	/* -- -- -- -- -- -- -- Result of Acronym request - -- -- -- -- -- -- -- -- */
	   case ALP_RQ_ACRONYM_VALUE:
		  if(U16Count < 8)
		  {
			 vDBG_LogError(StackFM_AppLayer, ErrUnableToRead, "Invalid response to acronym request");
			 bALP_ErrorOcurredFlag = eTRUE;
			 break;
		  }
		  xALP_AcronymData.U32Acronym = *((U32*)pU8Data);//Acronym name
		  pU8Data += 4;                                      //Next location
		  xALP_AcronymData.U32Value = *((U32*)pU8Data);  //Acronym value
		  //vDBG_LogState(StackFM_AppLayer, Info,"Acronym value received: ["+lstr(xALP_AcronymData.U32Acronym)+"] = "+str(xALP_AcronymData.U32Value));
		  //eALP_State = ALP_STATE_DONE;
		  //ALP_vSafeStateChange(ALP_STATE_DONE);
		  bALP_ValidInfoFlag = eTRUE;
		  break;
	/* -- -- -- -- -- -- -- -- -- Set FM Date-Time  -- -- -- -- -- -- -- -- -- */
		case ALP_SET_DATE_TIME:
		  U32ALP_ReceivedTime = *((U32*)pU8Data);
		   
		  vDBG_LogState(StackFM_AppLayer, Info,"New Date-Time set confirmed\n");
		  bALP_TimeConfirmedFlag = eTRUE;
		  break;
		  
		case ALP_RQ_NEW_COMMAND_TO_CONFIG:
			/* [CMND-ID][RES-TYPE]
							[  0][NULL]
							[  1][U32Result1]
							[255][U32ErrorCode]*/
		  xALP_CommandResult = *((ALP_tCommandResult*)pU8Data); 
		  if(xALP_CommandResult.U8ResultType == 255)//Error
		  {
			 vDBG_LogError(StackFM_AppLayer, ErrInvalidResponse, "Error from FM+ Config interpreter ID["+str(xALP_CommandResult.U8CommandID)+"]Err["+lstr(xALP_CommandResult.U32ResultValue)+"]");
			 break;
		  }

      
		  switch(xALP_CommandResult.U8CommandID)
		  {
			case ALP_CQ_SET_DATE_TIME:
				U32ALP_ReceivedTime = xALP_CommandResult.U32ResultValue;
				vDBG_LogState(StackFM_AppLayer, Info,"FM Time was set to "+lstr(U32ALP_ReceivedTime));
				bALP_TimeConfirmedFlag = eTRUE;
			break;
			
			case ALP_CQ_DISARM_VEHICLE_ID:
				//U16ALP_DriverID = (U16) (xALP_CommandResult.U32ResultValue);
				vDBG_LogState(StackFM_AppLayer, Info,"Driver Logged in FM");
				bALP_DriverLoggedFlag = eTRUE;
			break;
		  }
		  
      break;

   default:
      //mDBG_PRINT((DBG_ERR, DBG_FL, "Unhandled Packet ID: %d\n",U8PacketID));
	   vDBG_LogError(StackFM_AppLayer, ErrUnknowCode, "Unhandled Packet ID: "+str(U8PacketID));
      break;
   }
}

PUBLIC void vALP_OnSendTimeout()
{
   //A timeout ocurred
   //mDBG_PRINT((DBG_ERR, DBG_FL, "Timeout occured while trying to send command\n"));
   vDBG_LogError(StackFM_AppLayer, ErrTimeOut, "while trying to send command\n");
}


PUBLIC tBOOL bALP_HasCommsBeenEstablished()
{
   return bALP_CommsEstablishedFlag;
}

PUBLIC tBOOL bALP_HasFileDownloaded()
{
   return bALP_FileDownloadedFlag;
}

PUBLIC tBOOL bALP_HasErrorOcurred(){
	return bALP_ErrorOcurredFlag;
}

PUBLIC tBOOL bALP_HasTimeConfirmed(){
	return bALP_TimeConfirmedFlag;
}

PUBLIC tBOOL bALP_HasDriverLogged()
{
   return bALP_DriverLoggedFlag;
}

PUBLIC tBOOL bALP_BufferDownloadedData(U8* pU8Buffer, U16 U16NumberOfBytes)
{
   U8 U8Data;
   while(U16NumberOfBytes-- != 0)
   {
      // Copy specified data into the buffer
      U8Data = *pU8Buffer++;
      U8ALP_DownloadBuffer += chr(U8Data);
      // See if the buffer is full
      if (len(U8ALP_DownloadBuffer) == ALP_DOWNLOAD_BUFFER_SIZE)
      {
         // Save buffered data
         if (bALP_SaveBufferedData() == eFALSE) return eFALSE;
      }
   }

   return eTRUE;
}


PUBLIC tBOOL bALP_SaveBufferedData()
{
   // Make sure there is data in the buffer
   if (len(U8ALP_DownloadBuffer) == 0) return eTRUE;

   // Commit buffer content to file
   fd.transactionstart();
   fd.setdata(U8ALP_DownloadBuffer);
   if(fd.laststatus != PL_FD_STATUS_OK) {
      U8ALP_DownloadBuffer = "";
      return eFALSE;
   }
   fd.transactioncommit();

   // Reset buffer
   U8ALP_DownloadBuffer = "";
   return eTRUE;
}

void vALP_ProcessTachoFile()
{
   U8 U8EndPage;
   // File header
   tALP_DownloadFileHeader xFileHeader = ALP_FILE_HEADER_PREDEFINED_VALUES;

   // Make sure file is still open
   fd.filenum = pALP_DownloadFile;
   if(fd.fileopened == NO)
   {
     // mDBG_PRINT((DBG_ERR, DBG_FL, "Could not fix Tacho file header, because file is already closed.\n"));
	   vDBG_LogError(StackFM_AppLayer, ErrUnableToRead, "Could not fix Tacho file header, because file is already closed.");
	   return;
   }

   // Fill in values for file header
   U8EndPage                               = 0;//(U8)((U32ALP_TachoSize) / (U32)1024);
   //xFileHeader.U8SoftwareVersion           = aALP_DdrVersionBlock[ALP_TACHO_DEVICE_DRIVER];
   xFileHeader.U8VehicleNumberLSB          = (U8)(U16ALP_VehicleID&0x00FF);
   xFileHeader.U8VehicleNumberMSB          = (U8)((U16ALP_VehicleID&0xFF00)>>8);
   xFileHeader.U8StartPageEventData        = 0;
   xFileHeader.U8EndPageEventData          = 0;
   xFileHeader.U8Checksum_EventData        = 0+2+0+2;
   xFileHeader.U8StartPageEventData_Backup = 0;
   xFileHeader.U8EndPageEventData_Backup   = 0;
   xFileHeader.U8Checksum_EventData_Backup = 0+2+0+2;
   xFileHeader.U8EndPageEvents             = 0x01;
   xFileHeader.U8Checksum_EndPageEvents    = 0x01+2;
   xFileHeader.U8StartOfEvents_1           = 0x15;
   xFileHeader.U8StartOfEvents_2           = 0x00;
   xFileHeader.U8StartOfEvents_3           = 0x00;
   xFileHeader.U8Checksum_StartOfEvents    = 0x1B;

   // Go back to start of file
   if(fd.setpointer(1) != PL_FD_STATUS_OK)//if(fseek(pALP_DownloadFile, 0, SEEK_SET) != 0)
   {
	   vDBG_LogError(StackFM_AppLayer, ErrUnableToRead, "Unable to go back to start of file.");
	   bALP_ErrorOcurredFlag = eTRUE;
      return;
   }

   // Write file header
   if(fwrite_packet(&xFileHeader,sizeof(xFileHeader),pALP_DownloadFile) == 0)
   {
	   vDBG_LogError(StackFM_AppLayer, ErrUnableToWrite, "Unable to write Event Data Header.\n");
	   bALP_ErrorOcurredFlag = eTRUE;
      return;
   }
   // Close file
   if (fd.close() != PL_FD_STATUS_OK)//if (fclose(pALP_DownloadFile) != 0)
   {
      //pALP_DownloadFile = NULL;
      vDBG_LogError(StackFM_AppLayer, ErrUnableToWrite, "Unable to close Tacho file.\n");
	  bALP_ErrorOcurredFlag = eTRUE;
      return;
   }

   // Indicate success
   //pALP_DownloadFile = 255;
   //cp020327 eALP_State = ALP_STATE_DONE;
   //ALP_vSafeStateChange(ALP_STATE_DONE);                 //Do not overwrite any error states
}

void vALP_ProcessEventFile()
{
   U8 U8EndPage;
   // File header
   tALP_DownloadFileHeader xFileHeader = ALP_FILE_HEADER_PREDEFINED_VALUES;
   tBOOL  bFileTooSmall = eFALSE;

   // Make sure file is still open
   fd.filenum = pALP_DownloadFile;
   if(fd.fileopened == NO)
   {
      vDBG_LogError(StackFM_AppLayer, ErrUnableToRead, "Could not fix Event file header, because file is already closed.");
	  bALP_ErrorOcurredFlag = eTRUE;
      return;
   }

   // Fill in values for file header
   U8EndPage                               = 0;//(U8)((U32ALP_ConfigSize+U32ALP_EventsSize) / (U32)1024) + 1;
   xFileHeader.U8SoftwareVersion           = 0;//aALP_DdrVersionBlock[ALP_EVENT_DEVICE_DRIVER];
   xFileHeader.U8VehicleNumberLSB          = (U8)(U16ALP_VehicleID&0x00FF);
   xFileHeader.U8VehicleNumberMSB          = (U8)((U16ALP_VehicleID&0xFF00)>>8);
   xFileHeader.U8EndPageEventData          = U8EndPage;
   xFileHeader.U8Checksum_EventData        = 0x01+2+U8EndPage+2;
   xFileHeader.U8EndPageEventData_Backup   = U8EndPage;
   xFileHeader.U8Checksum_EventData_Backup = 0x01+2+U8EndPage+2;
   xFileHeader.U8EndPageEvents             = U8EndPage+1;
   xFileHeader.U8Checksum_EndPageEvents    = U8EndPage+1+2;

   if(fd.filesize < 3*1024)//if(ftell(pALP_DownloadFile) < 3*1024)
      bFileTooSmall = eTRUE;

   // Go back to start of file
   if(fd.setpointer(1) != PL_FD_STATUS_OK)//if(fseek(pALP_DownloadFile, 0, SEEK_SET) != 0)
   {
      //mDBG_PRINT((DBG_ERR, DBG_FL, "Unable to go back to start of file.\n"));
      vDBG_LogError(StackFM_AppLayer, ErrUnableToRead, "Unable to go back to start of file.");
	  bALP_ErrorOcurredFlag = eTRUE;
      return;
   }

   // Write file header
   if(fwrite_packet(&xFileHeader,sizeof(xFileHeader),pALP_DownloadFile) == 0)
   {

	   vDBG_LogError(StackFM_AppLayer, ErrUnableToWrite, "Unable to write Event Data Header.");
	   bALP_ErrorOcurredFlag = eTRUE;
      return;
   }
   // Close file
   if(fd.close() != PL_FD_STATUS_OK)//if (fclose(pALP_DownloadFile) != 0)
   {
      pALP_DownloadFile = NULL;
	  bALP_ErrorOcurredFlag = eTRUE;
      vDBG_LogError(StackFM_AppLayer, ErrUnableToWrite, "Unable to close Event file.");
      return;
   }

   if(bFileTooSmall)
   {
      vDBG_LogError(StackFM_AppLayer,ErrDataWrong,"Downloaded data is corrupted, Size too small");
	  bALP_ErrorOcurredFlag = eTRUE;
      return;
   }

   // Indicate success
   //pALP_DownloadFile = NULL;
   //ALP_vSafeStateChange(ALP_STATE_DONE);                 //Do not overwrite any error states
}

tBOOL bALP_StartDownload(string szFileName, tALP_DownloadType eDownloadType)
{

   if(bALP_CommsEstablishedFlag == eFALSE)
      return eFALSE;
	  
	bALP_ValidInfoFlag = eFALSE;
	bALP_FileDownloadedFlag = eFALSE;
    bALP_ErrorOcurredFlag = eFALSE;
   // Open the file for binary writing
   pALP_DownloadFile = NUM_FILE_DEFAULT;
   fd.filenum = pALP_DownloadFile;

   if(fd.open(szFileName) != PL_FD_STATUS_OK)
   {
      //mDBG_PRINT((DBG_ERR, DBG_FL, "Unable to open file for writing. Error = %d\n",GetLastError()));
      vDBG_LogError(StackFM_AppLayer,ErrUnableToOpenConnection,"Unable to open file for writing.");
	   return eFALSE;
   }
   
   if(fd.setfilesize(0) != PL_FD_STATUS_OK){
		vDBG_LogError(StackFM_AppLayer,ErrUnableToWrite,"Unable to resize file.");
	   return eFALSE;
   }
   

   fd.setpointer(fd.filesize+1);
   // Reserve 1k space for header at the start of the file
   
   vDBG_LogState(StackFM_AppLayer, Info,"writing file header in Flash Disk...");
   if( fwrite_blank(1024,pALP_DownloadFile) != 1024)
   {
	  vDBG_LogError(StackFM_AppLayer,ErrUnableToWrite, "Unable to write in file\n");
      return eFALSE;
   }
   
   
   // Reset tracking variables
   U32ALP_BytesReceived = 0;
   U32ALP_BlockSize     = 0;
   U32ALP_EventsSize    = 0;
   U32ALP_TachoSize     = 0;

   //Request the specified download from the Transport Layer
   U8 U8Command;
   switch(eDownloadType)
   {
   case ALP_QUICK_EVENT_DOWNLOAD:
      U8Command         = ALP_RQ_EVENTS;
      xALP_DownloadParams.U8DownloadType = ALP_QUICK;
      xALP_DownloadParams.U8ParamSize = 1;
      break;
   case ALP_FULL_EVENT_DOWNLOAD:
      U8Command         = ALP_RQ_EVENTS;
      xALP_DownloadParams.U8DownloadType = ALP_FULL;
      xALP_DownloadParams.U8ParamSize = 1;
      break;

   case ALP_QUICK_TACHO_DOWNLOAD:
      U8Command         = ALP_RQ_TACHO;
      xALP_DownloadParams.U8DownloadType = ALP_QUICK;
      xALP_DownloadParams.U8ParamSize = 1;
      break;
   case ALP_FULL_TACHO_DOWNLOAD:
      U8Command         = ALP_RQ_TACHO;
      xALP_DownloadParams.U8DownloadType = ALP_FULL;
      xALP_DownloadParams.U8ParamSize = 1;
      break;

   default:
	   vDBG_LogState(StackFM_AppLayer, Info, "Invalid download type specified. Defaulting to 'Quick Event Download'");
	   U8Command = ALP_RQ_CONFIG;
      xALP_DownloadParams.U8DownloadType = ALP_QUICK;
      break;
   }

   vDBG_LogState(StackFM_AppLayer, Info, "Starting download from FM...");
   vTSP_WriteCommand(U8Command,(U8*)&xALP_DownloadParams,xALP_DownloadParams.U8ParamSize);
   //eALP_State = ALP_STATE_DOWNLOAD_INITIATED;            //Download started
   return eTRUE;
}

tBOOL bALP_RequestAcronymValue(U32 U32Acro)
{
  if(bALP_CommsEstablishedFlag == eFALSE)
      return eFALSE;    
	  
   bALP_ValidInfoFlag = eFALSE;
   bALP_ErrorOcurredFlag = eFALSE;
	
   ALP_U32AcronymToRequest = U32Acro;
   //vDBG_LogState(StackFM_AppLayer, Info, "Requesting value of Acronym["+lstr(ALP_U32AcronymToRequest)+"] from the FM");
   
   //eALP_State = ALP_STATE_REQUEST_ACRONYM_VALUE_INITIATED;//A request acronym value action was initiated

   vTSP_WriteCommand(ALP_RQ_ACRONYM_VALUE,(U8*)(&ALP_U32AcronymToRequest),4);//Request the acronym value
   xALP_AcronymData.U32Acronym = 0xFFFFFFFF;
   return eTRUE;
}

tBOOL bALP_GetAcronymValue(U32 *pU32Acronym, U32 *pU32Value)
{
   if( (pU32Acronym == (U32 *)0) || (pU32Value == (U32 *)0) || (bALP_ValidInfoFlag == eFALSE))
   {
      return eFALSE;                                      //Invalid pointers
   }
   if(xALP_AcronymData.U32Acronym == 0xFFFFFFFF)
      return eFALSE;                                      //Not valid yet
   *pU32Acronym = xALP_AcronymData.U32Acronym;           //Return the actual values
   *pU32Value = xALP_AcronymData.U32Value;
   return eTRUE;
}

tBOOL bALP_SetDateTime(U32* pU32Date, U32* pU32Time)
{
	vDBG_LogState(StackFM_AppLayer, Info, "Sending command to set FM Time");	
   return bALP_CommandToFm(ALP_CQ_SET_DATE_TIME, *pU32Date, *pU32Time,0);;
}

tBOOL bALP_IdentifyDriver(U16 U16NewDriverId)
{
	tBOOL result;
	vDBG_LogState(StackFM_AppLayer, Info, "Identifying driver in FM");
	bALP_DriverLoggedFlag	 	 = eFALSE;
	result = bALP_CommandToFm(ALP_CQ_DISARM_VEHICLE_ID,U16NewDriverId,0,0);
	if(!result) vDBG_LogError(StackFM_AppLayer, ErrUnableToAuthenticate, "can't identify driver");
   return result;
}


tBOOL bALP_CommandToFm(U32 U32CommandId, U32 U32Parameter1, U32 U32Parameter2, U32 U32Parameter3){
	if(bALP_CommsEstablishedFlag == eFALSE)
      return eFALSE;                                      //Not ready to do comms
	  
	xALP_CommandResult.U8CommandID = 0;
   
   xALP_CommandToConfig.U8CommandId = (U8)(0x000000FF & U32CommandId);
   xALP_CommandToConfig.U32Parameter1 = U32Parameter1;
   xALP_CommandToConfig.U32Parameter2 = U32Parameter2;
   xALP_CommandToConfig.U32Parameter3 = U32Parameter3;
   vTSP_WriteCommand(ALP_RQ_NEW_COMMAND_TO_CONFIG,(U8*)(&xALP_CommandToConfig),sizeof(xALP_CommandToConfig));
   
   return eTRUE;
}


U16 fwrite_packet(U8 *pBuffer, U8 U8Length, U8 U8fileNum)
{
	string sBuffer = "";
	U8 U8Data;
	U8 bytes_commited = 0;
	
	fd.filenum = U8fileNum;
	if (fd.fileopened != YES) return 0;

   while(len(sBuffer) < U8Length){
      U8Data = *(pBuffer++);
      sBuffer += chr(U8Data);
   }

   fd.transactionstart();
   fd.setdata(sBuffer);

   if(fd.laststatus != PL_FD_STATUS_OK) {
      return 0;
   }
   fd.transactioncommit();
   return U8Length;
}

U16 fwrite_blank(U16 U16Length, U8 U8fileNum)
{
   string sBuffer;
   U16 bytes_commited = 0;
   U16 rest = U16Length%(ALP_DOWNLOAD_BUFFER_SIZE);
   U16 last_packet = U16Length - rest;
   U16 packet = ALP_DOWNLOAD_BUFFER_SIZE;
   
   U8 i;

   fd.filenum = U8fileNum;
   if (fd.fileopened != YES) return 0;
   
   for(i = 0; i < ALP_DOWNLOAD_BUFFER_SIZE; i++){
	sBuffer += chr(0xFF);
   } 

   for(;;) {
        
      //envia la informacion en buffer tipo string al archivo.
      fd.transactionstart();
      fd.setdata(left(sBuffer,packet));

      if(fd.laststatus != PL_FD_STATUS_OK)  break;
      fd.transactioncommit();

      bytes_commited += packet;

      if     (bytes_commited < last_packet)  packet = ALP_DOWNLOAD_BUFFER_SIZE;
      else if(bytes_commited < U16Length)    packet = rest;
      else break;
   }

   return bytes_commited;
}

void setALP_VehicleIDValue(U16 U16VehiID){
	U16ALP_VehicleID = U16VehiID;
}

PUBLIC U16  getALP_VehicleIDValue(){
	return U16ALP_VehicleID;
}