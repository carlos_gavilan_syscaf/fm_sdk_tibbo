/* ==========================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        Simplified HDLC Protocol Layer
AUTHOR:           P.J. Conradie
DESCRIPTION:      This layer provides HDLC encapsulation for data packets

         e.g. [7E] .. (all 7E data escaped with 7D) .. [CRC lo] [CRC hi] [7E]

VERSION:          $Revision: 9 $
DATED:            $Date: 02/03/12 15:31 $
LAST MODIFIED BY: $Author: Carlop $
HISTORY:          $Log: /products/COMMS/Software/WinSDK/hdlc.c $
 *
 * 9     02/03/12 15:31 Carlop
 *
 * 8     01/07/04 10:42 Carlop
 * changed back to transport layer v2, not escape XON, XOFF
 *
 * 7     01/07/02 13:19 Carlop
 * Added escape sequences for XON,XOFF flow control
 *
 * 6     01/02/27 14:54 Carlop
 * Fixed on receive decoder posible memory overrun
 *
 * 5     01/02/21 10:59 Carlop
 * Added escaping for char 10 <LF> to cater for modem
 * <CR><LF>DISCONNECT<CR><LF>
 *
 * 4     12/01/01 9:13 Pieterc

========================================================================== */

/* ___PROJECT INCLUDES____________________________________________________ */
#include "hdlc.h"
#include "transprt.h"
#include "serial.h"
#include "..\System\debug.h"

/* ___LOCAL DEFINITIONS___________________________________________________ */
// Constants used during CRC calculation
#define HDLC_INITIAL_CRC 0xffff
#define HDLC_GOOD_CRC    0xf0b8

// Buffer sizes
#define HDLC_BUFFER_SIZE 256

/* ___LOCAL VARIABLES_____________________________________________________ */
// Function pointer
//tHDLC_TxBuffer pfHDLC_TxBuffer;

// Packet storage
U8          aU8HDLC_RxBuffer[HDLC_BUFFER_SIZE];
string      aU8HDLC_TxBuffer;

U16   U16HDLC_RxIndex    = 0;
tBOOL bHDLC_RxEscapeFlag = eFALSE;

// CRC storage
U16 U16HDLC_RxCrc        = HDLC_INITIAL_CRC;
U16 U16HDLC_TxCrc        = HDLC_INITIAL_CRC;

// Table used during CRC calculation
PRIVATE U16 aU16HDLC_CrcTable[256] =
{
   0x0000,0x1189,0x2312,0x329B,0x4624,0x57AD,0x6536,0x74BF,
   0x8C48,0x9DC1,0xAF5A,0xBED3,0xCA6C,0xDBE5,0xE97E,0xF8F7,
   0x1081,0x0108,0x3393,0x221A,0x56A5,0x472C,0x75B7,0x643E,
   0x9CC9,0x8D40,0xBFDB,0xAE52,0xDAED,0xCB64,0xF9FF,0xE876,
   0x2102,0x308B,0x0210,0x1399,0x6726,0x76AF,0x4434,0x55BD,
   0xAD4A,0xBCC3,0x8E58,0x9FD1,0xEB6E,0xFAE7,0xC87C,0xD9F5,
   0x3183,0x200A,0x1291,0x0318,0x77A7,0x662E,0x54B5,0x453C,
   0xBDCB,0xAC42,0x9ED9,0x8F50,0xFBEF,0xEA66,0xD8FD,0xC974,
   0x4204,0x538D,0x6116,0x709F,0x0420,0x15A9,0x2732,0x36BB,
   0xCE4C,0xDFC5,0xED5E,0xFCD7,0x8868,0x99E1,0xAB7A,0xBAF3,
   0x5285,0x430C,0x7197,0x601E,0x14A1,0x0528,0x37B3,0x263A,
   0xDECD,0xCF44,0xFDDF,0xEC56,0x98E9,0x8960,0xBBFB,0xAA72,
   0x6306,0x728F,0x4014,0x519D,0x2522,0x34AB,0x0630,0x17B9,
   0xEF4E,0xFEC7,0xCC5C,0xDDD5,0xA96A,0xB8E3,0x8A78,0x9BF1,
   0x7387,0x620E,0x5095,0x411C,0x35A3,0x242A,0x16B1,0x0738,
   0xFFCF,0xEE46,0xDCDD,0xCD54,0xB9EB,0xA862,0x9AF9,0x8B70,
   0x8408,0x9581,0xA71A,0xB693,0xC22C,0xD3A5,0xE13E,0xF0B7,
   0x0840,0x19C9,0x2B52,0x3ADB,0x4E64,0x5FED,0x6D76,0x7CFF,
   0x9489,0x8500,0xB79B,0xA612,0xD2AD,0xC324,0xF1BF,0xE036,
   0x18C1,0x0948,0x3BD3,0x2A5A,0x5EE5,0x4F6C,0x7DF7,0x6C7E,
   0xA50A,0xB483,0x8618,0x9791,0xE32E,0xF2A7,0xC03C,0xD1B5,
   0x2942,0x38CB,0x0A50,0x1BD9,0x6F66,0x7EEF,0x4C74,0x5DFD,
   0xB58B,0xA402,0x9699,0x8710,0xF3AF,0xE226,0xD0BD,0xC134,
   0x39C3,0x284A,0x1AD1,0x0B58,0x7FE7,0x6E6E,0x5CF5,0x4D7C,
   0xC60C,0xD785,0xE51E,0xF497,0x8028,0x91A1,0xA33A,0xB2B3,
   0x4A44,0x5BCD,0x6956,0x78DF,0x0C60,0x1DE9,0x2F72,0x3EFB,
   0xD68D,0xC704,0xF59F,0xE416,0x90A9,0x8120,0xB3BB,0xA232,
   0x5AC5,0x4B4C,0x79D7,0x685E,0x1CE1,0x0D68,0x3FF3,0x2E7A,
   0xE70E,0xF687,0xC41C,0xD595,0xA12A,0xB0A3,0x8238,0x93B1,
   0x6B46,0x7ACF,0x4854,0x59DD,0x2D62,0x3CEB,0x0E70,0x1FF9,
   0xF78F,0xE606,0xD49D,0xC514,0xB1AB,0xA022,0x92B9,0x8330,
   0x7BC7,0x6A4E,0x58D5,0x495C,0x3DE3,0x2C6A,0x1EF1,0x0F78
};

/* ___MACROS______________________________________________________________ */
// Macro used to calculate CRC using ROM table method
#define mHDLC_CALCULATE_CRC(U16Crc, U8Data) \
        U16Crc = (((U16Crc) >> 8) ^ aU16HDLC_CrcTable[((U16Crc) ^ (U8Data)) & 0xff])

/* ___PRIVATE FUNCTIONS___________________________________________________ */
/*PRIVATE void vHDLC_DummyTxBufferHandler(U8* pBuffer, U16 U16Count)
{
   // Do nothing
}*/


//Check if input character has to be escaped
//Returns 1 if the input has to be escaped, else 0
PRIVATE U8 U8HDLC_CheckToEscape(U8 U8Data)
{
   if(U8Data == 0x7D)                              //Escaping character
      return 1;
   if(U8Data == '~')                               //HDLC framing character
      return 1;
   if(U8Data == 0x0A)                              //LineFeed character
      return 1;
/*
   if(U8Data == 0x11)                              //XON character
      return 1;
   if(U8Data == 0x13)                              //XOFF character
      return 1;
*/
   return 0;
}

/* ___PUBLIC FUNCTIONS____________________________________________________ */
PUBLIC void vHDLC_Init()
{
   U16HDLC_RxCrc      = HDLC_INITIAL_CRC;
   U16HDLC_TxCrc      = HDLC_INITIAL_CRC;
   U16HDLC_RxIndex    = 0;
   bHDLC_RxEscapeFlag = eFALSE;

   aU8HDLC_RxBuffer = "";
   aU8HDLC_TxBuffer = "";


   //vDBG_LogState(StackFM_HDLC, Info, "Init Ok");
}

/*PUBLIC void vHDLC_SetTxHandler(tHDLC_TxBuffer pfTxBuffer)
{
   pfHDLC_TxBuffer = pfTxBuffer;
}*/

PUBLIC void vHDLC_TxBuffer(U8* pU8Data, U16 U16Count)
{

   U8  U8Data;
   U16 U16TxCount;
   aU8HDLC_TxBuffer = "";

   // Reset CRC
   U16HDLC_TxCrc = HDLC_INITIAL_CRC;

   // Start with 0x7E
   aU8HDLC_TxBuffer+="~";//pU8Buffer++;
   
   // Copy data into buffer and calculate CRC
   while(U16Count-- != 0)
   {
      // Retrieve data
      U8Data = *pU8Data++;
      // Calculate CRC
      mHDLC_CALCULATE_CRC(U16HDLC_TxCrc,U8Data);


      // if data is 0x7D or 0x7E then it should be escaped
      //if((U8Data == 0x7D)||(U8Data == '~')||(U8Data == 0x10)) //Add checking for LF -> <CR><LF>DISCONNET<CR><LF>
      if(U8HDLC_CheckToEscape(U8Data))
      {
         // Add escape character
         
          aU8HDLC_TxBuffer+=chr(0x7D);//pU8Buffer++;

         if(len(aU8HDLC_TxBuffer) > (HDLC_BUFFER_SIZE-1)) return;
         // Escape data
         U8Data = U8Data ^ 0x20;
      }
      // Add escaped data
      aU8HDLC_TxBuffer+=chr(U8Data);
      //*pU8Buffer++ = U8Data;
      if(len(aU8HDLC_TxBuffer) > (HDLC_BUFFER_SIZE-1)) return;
   }

   // Invert CRC
   U16HDLC_TxCrc = U16HDLC_TxCrc ^ 0xffff;

   // Add low part of CRC
   U8Data = (U8)(U16HDLC_TxCrc&0xff);
   if((U8Data == 0x7D)||(U8Data == '~')||(U8Data == 0x10)) //Add checking for LF -> <CR><LF>DISCONNET<CR><LF>
   {
      aU8HDLC_TxBuffer+=chr(0x7D);
      //*pU8Buffer++ = 0x7D;
      if(len(aU8HDLC_TxBuffer) > (HDLC_BUFFER_SIZE-1)) return;
      U8Data       = U8Data ^ 0x20;
   }
   aU8HDLC_TxBuffer+=chr(U8Data);
   //*pU8Buffer++ = U8Data;
   if(len(aU8HDLC_TxBuffer) > (HDLC_BUFFER_SIZE-1)) return;

   // Add high part of CRC
   U8Data = (U8)((U16HDLC_TxCrc&0xff00)>>8);
   //if((U8Data == 0x7D)||(U8Data == '~')||(U8Data == 0x10)) //Add checking for LF -> <CR><LF>DISCONNET<CR><LF>
   if(U8HDLC_CheckToEscape(U8Data))
   {
      aU8HDLC_TxBuffer+=chr(0x7D);
      if(len(aU8HDLC_TxBuffer) > (HDLC_BUFFER_SIZE-1)) return;
      U8Data       = U8Data ^ 0x20;
   }

   aU8HDLC_TxBuffer+=chr(U8Data);
   if(len(aU8HDLC_TxBuffer) > (HDLC_BUFFER_SIZE-1)) return;

   // End with 0x7E
   aU8HDLC_TxBuffer+="~";
   
   if(len(aU8HDLC_TxBuffer) > (HDLC_BUFFER_SIZE-1))  return;

   // Send buffer
   ser.setdata(aU8HDLC_TxBuffer);
   ser.send();
}



PUBLIC void vHDLC_OnRxData(string<1> Data)
{
   // See if start/end marker 0x7E has been received
   U8* pU8Data = Data;
   U8  U8Data = *pU8Data;
   if(U8Data == '~')
   {
      // Reset escape flag
      bHDLC_RxEscapeFlag = eFALSE;

      // If this is the starting marker
      // or the packet has nothing inside ('~~')
      // then do nothing
      if (U16HDLC_RxIndex < 1)
      {
         // Reset receiver
         U16HDLC_RxIndex = 0;
         aU8HDLC_RxBuffer = "";
         U16HDLC_RxCrc   = HDLC_INITIAL_CRC;
         return;
      }

      // See if CRC is correct
      if(U16HDLC_RxCrc != HDLC_GOOD_CRC)
      {
         // Reset receiver
         U16HDLC_RxIndex = 0;
         aU8HDLC_RxBuffer = "";
         U16HDLC_RxCrc   = HDLC_INITIAL_CRC;
         // Indicate to upper layer that a checksum error has occurred
         vTSP_OnCRCError();
         return;
      }

      // Remove CRC
      U16HDLC_RxIndex -= 2;
      // Pass packet up to upper layer
      vTSP_OnNewPacket(aU8HDLC_RxBuffer, U16HDLC_RxIndex);

      // Reset receiver
      U16HDLC_RxIndex = 0;
      aU8HDLC_RxBuffer = "";
      U16HDLC_RxCrc   = HDLC_INITIAL_CRC;
      return;
   }

   // See if data is escape character
   if ((U8Data == 0x7D)&&(bHDLC_RxEscapeFlag == eFALSE))
   {
      // Next byte of data will be escaped
      bHDLC_RxEscapeFlag = eTRUE;
      return;
   }
   // See if data should be escaped
   if (bHDLC_RxEscapeFlag == eTRUE)
   {
      // Escape data
      U8Data             = U8Data ^ 0x20;
      bHDLC_RxEscapeFlag = eFALSE;
   }

   // Calculate CRC
   mHDLC_CALCULATE_CRC(U16HDLC_RxCrc,U8Data);

   // Copy data into receive buffer
   aU8HDLC_RxBuffer[U16HDLC_RxIndex] = U8Data;

   // Increment and see if buffer overflow has occured
   if(++U16HDLC_RxIndex == HDLC_BUFFER_SIZE)
   {
      // Invalidate CRC so that packet will be ignored
      U16HDLC_RxIndex = 0;
      U16HDLC_RxCrc ^= 0xffff;
   }
}

