#ifndef __FM_H__
#define __FM_H__

#include "..\System\cifms.h"

/* ___PROJECT INCLUDES____________________________________________________ */


#include "serial.h"
#include "timer.h"
#include "hdlc.h"
#include "transprt.h"
#include "AppLayer.h"

#define FM_TIMEOUT 			400

EXTERN PUBLIC void vFM_Initialize();
EXTERN PUBLIC void vFM_Reset();
EXTERN PUBLIC void vFM_ProcessRxTx();
EXTERN PUBLIC void vFM_SetDateTime(U32* bcd_date, U32* bcd_time);
EXTERN PUBLIC void vFM_IdentifyDriver(U16 U16NewDriverId);

EXTERN PUBLIC tBOOL bFM_HasTimeSetOnFM();
EXTERN PUBLIC tBOOL bFM_HasDriverLoggedInFM();

EXTERN PUBLIC void vFM_WakingListeners();
EXTERN PUBLIC void vFM_SleepListeners();

#endif