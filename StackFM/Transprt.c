/* ==========================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        GO_BACK_N_SOURCE
AUTHOR:           Carlo Putter
DESCRIPTION:      This component implements Go-Back-N protocol

VERSION:          $Revision: 14 $
DATED:            $Date: 02/03/13 11:04 $
LAST MODIFIED BY: $Author: Carlop $
HISTORY:          $Log: /products/COMMS/Software/WinSDK/Transprt.c $
 *
 * 14    02/03/13 11:04 Carlop
 * Updated Transport layer to version 3
 *
 * 13    02/03/11 10:27 Carlop
 * Updated Transport layer to version 3
 *
 * 12    01/10/24 11:32 Carlop
 * Upped Application laver version to 3
 *
 * 11    01/08/20 15:39 Carlop
 * Bug fix
 *
 * 10    01/07/04 10:42 Carlop
 * changed back to transport layer v2
 *
 * 9     01/07/02 13:20 Carlop
 * Upped Transport version, HDLC.c changed
 *
 * 8     01/04/10 15:13 Carlop
 * Added comment to update protocol versions
 * //Update values in AppLayer.cpp !!!! if changed
 *
 * 7     16/02/01 12:14 Pieterc
 *
 * 6     01/02/16 10:51 Carlop
 *
 * 5     01/02/14 14:31 Carlop
 *
 * 4     01/02/14 14:03 Carlop
 *
 * 2     01/02/14 13:05 Carlop
 *
 * 1     01/02/13 17:08 Carlop
 *
 * 4     01/02/13 13:32 Carlop
 *
 * 3     01/02/12 13:51 Carlop
 *
 * 2     01/02/12 12:50 Carlop
 *
 * 1     01/02/12 12:41 Carlop
 *
 * 6     01/02/12 11:56 Carlop
 *
 * 5     01/02/09 16:43 Carlop
 *
 * 4     01/02/09 13:31 Carlop
 *
 * 3     01/02/05 16:20 Carlop
 *
 * 2     00/12/22 11:30 Carlop
 * New project to test the Go Back N transport layer protocol

========================================================================== */

/* ___STANDARD INCLUDES___________________________________________________ */
/*#if defined(TSP_DBG_HDLC) && defined(__DBG__)
#include "time.th"
#endif*/


/* ___PROJECT INCLUDES____________________________________________________ */
#include "Transprt.h"
#include "AppLayer.h"

/* ___LOCAL DEFINITIONS___________________________________________________ */
#define ALP_VERSION_ID        0                   //Version Info Packet ID
#define TSP_VERSION           3                   //Transport Layer vesrion
#define ALP_VERSION           2                   //Application layer version
#ifndef COMMS_DEVICE_TYPE
   #define COMMS_DEVICE_TYPE     0xFF             //Comms device type is a PC,capablity list is all
#endif
//Update values in AppLayer.cpp !!!! if changed


#define TSP_GBN_SEQ_WRAP      TSP_HEADER_SEQ_NUM_MASK-1 //0x07                //0bxxxxxAAA -> 0..7
//#define TSP_GBN_SEQ_NUMBERS   (TSP_GBN_SEQ_WRAP+1)//Seq : 0..7

// File transmission states
#define TSP_FILE_IDLE         0                   //No file transmission in progress
#define TSP_FILE_SIZE         1                   //File saze header to be transmitted
#define TSP_FILE_DATA         2                   //File contents to be transmitted
#define TSP_FILE_EOF          3                   //End of file to be transmitted
#define TSP_SEGMENT_SIZE_LIMIT 128//Maximum size of a transmission packet(-Transport Header)

#define TSP_CMND_IDLE         0                   //No Commands in progress
#define TSP_CMND_NEW          1                   //New command to be transmitted

#define TSP_SEND_CLEAR        0x00                //Clear all
#define TSP_SEND_RESET        0x03                //Send reset request packet
#define TSP_SEND_VERSION      0x0C                //Send version packet

struct tTSP_SentItem
{
  U8        U8SequenceNumber;                     //This packets SQ number (== TSP_GBN_SEQ_NUMBERS if free)
  U8*   	pfU8PacketStart;                      //Pointer to data
  U8        U8PacketSize;                         //Number of data bytes
  U8        U8PacketID;                           //ID of this item
  U8        U8TimerTicks;                         //Ticks left before resend
  U8        U8RetriesLeft;                        //Retries left before error
};

/* ___GLOBAL VARIABLES____________________________________________________ */

//--------------------------------------------------------------------------
tTSP_SentItem   axTSP_TxQueue[TSP_GBN_PIPE_DEPTH]; //Array of Usable Items
U8              U8TSP_TxQFirstItem;
U8              U8TSP_TxQLastItem;
U8              U8TSP_TxQResendItem;

//--------------------------------------------------------------------------
U8              U8TSP_NextOutSeq;                 //Next Seq# to be transmited
U8              U8TSP_NextInSeq;                  //Next Seq# to be received

//--------------------------------------------------------------------------
U8              aU8TSP_TxPacket[TSP_DATA_BUFFER_SIZE];//Data packet
U8              aU8TSP_TxAsync[4];
U8              aU8TSP_TxAckNack[1];
U8              aU8TSP_TxReset[1];

//--------------------------------------------------------------------------
U8              U8TSP_FileState;                  //File transmission statemachine
U8              U8TSP_FileSizeID;                 //ID to mark the file size data
U8              U8TSP_FileDataID;                 //ID to mark the file contents data
U8*             pfU8TSP_FileIndex;                //Current file location pointer
U32             U32TSP_FileSize;                  //Number of bytes to be transmitted
U8*             pfU8TSP_DriveStart;               //Start of the Ringbuffer (Drive) containing the file
U8*             pfU8TSP_DriveEnd;                 //End of the Ringbuffer (Drive) containing the file
U8              aU8TSP_FileSizeBuffer[5];         //ram buffer to hold the file size

U8              U8TSP_CmndState;                  //Command transmission state
U8              U8TSP_CmndDataID;                 //ID to mark the command contents data
U8*             pfU8TSP_CommandIndex;             //Pointer to the commsnd data
U8              U8TSP_CmndSize;                   //Number of bytes to be transmitted

//--------------------------------------------------------------------------
U8              U8TSP_ResetRequestTimer;          //Timer to continually request resets

//--------------------------------------------------------------------------
U8              U8TSP_ToBeACKed;                  //if != TSP_INVALID_SEQ_NUM -> new ack to send
U8              U8TSP_ToBeNACKed;                 //if != TSP_INVALID_SEQ_NUM -> new nack to send
U8              U8TSP_ToSend;                     //dual-bitfield: ResetRQ, VersionInfoRQ
U8              U8TSP_ReReceive;                  //Missed an item, wait for re-receive

/* ___LOCAL VARIABLES_____________________________________________________ */
/* ___PRIVATE FUNCTION PROTOTYPES_________________________________________ */
/* ___MACROS______________________________________________________________ */
/* ___PRIVATE FUNCTIONS___________________________________________________ */


//Get NextItemIndex
U8 U8TSP_NextSeqNumber(U8 U8FromIndex)
{
  U8FromIndex++;
  //U8FromIndex &= TSP_GBN_SEQ_WRAP;
  if(U8FromIndex > TSP_GBN_SEQ_WRAP)
    U8FromIndex = 0;
  return(U8FromIndex);
}

//Get PreviousItemIndex
U8 U8TSP_PreviousSeqNumber(U8 U8FromIndex)
{
   U8FromIndex--;
   if(U8FromIndex > TSP_GBN_SEQ_WRAP)
      U8FromIndex = TSP_GBN_SEQ_WRAP;
   return(U8FromIndex);
}


//Get NextItemIndex
U8 U8TSP_TxQNextIndex(U8 U8FromIndex)
{
  U8FromIndex++;
  if(U8FromIndex >= TSP_GBN_PIPE_DEPTH) U8FromIndex = 0;
  return(U8FromIndex);
}

//Get PreviousItemIndex
U8 U8TSP_TxQPreviousIndex(U8 U8FromIndex)
{
   U8FromIndex--;
   if(U8FromIndex >= TSP_GBN_PIPE_DEPTH)
      U8FromIndex = TSP_GBN_PIPE_DEPTH-1;
   return(U8FromIndex);
}

//Get the index of a free location to add a packet
// OK -> Item Index
// No -> "TSP_GBN_PIPE_DEPTH" (out of queue pointer)
U8 U8TSP_TxQGetAddIndex()
{
U8  Retvalue;

  if(U8TSP_TxQLastItem == TSP_GBN_PIPE_DEPTH)
    return(U8TSP_TxQLastItem);

  Retvalue = U8TSP_TxQLastItem;
  axTSP_TxQueue[Retvalue].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  U8TSP_TxQLastItem = U8TSP_TxQNextIndex(U8TSP_TxQLastItem);
  if(U8TSP_TxQLastItem == U8TSP_TxQFirstItem)
    U8TSP_TxQLastItem = TSP_GBN_PIPE_DEPTH;
  return(Retvalue);
}

//Check for free queue sapce
// OK -> 1
// No -> 0
U8 U8TSP_TxQCheckFreeSpace()
{
  if(U8TSP_TxQLastItem == TSP_GBN_PIPE_DEPTH)
    return(0);
  return(1);
}

//Delete an item with a specific sequence number,
//if Seq# Last+1 specified (delete all, -> no acks received for the items)
//as well as older items
// OK -> 1
// Err -> 0 (did not exist)
U8 U8TSP_TxQDeleteSeqNum(U8 U8SeqNumber)
{
U8 U8StartLoc;
U8 U8ItemCount;
U8 U8ItemSeqNum;

U8 aU8QueueSequences[TSP_GBN_PIPE_DEPTH];
U8 U8SequenceCatch;
U8 U8SequenceDelete;


  if(U8TSP_TxQLastItem == U8TSP_TxQFirstItem) return(0);//nothing in the queue

  U8StartLoc = U8TSP_TxQLastItem;                 //use newest packet as the start index
  if(U8TSP_TxQLastItem == TSP_GBN_PIPE_DEPTH)     //pipe was full, use oldest item as start index
    U8StartLoc = U8TSP_TxQFirstItem;              //

  U8ItemCount = 0;
  while(U8ItemCount++ != TSP_GBN_PIPE_DEPTH)      //run through all the list items
  {
    U8StartLoc = U8TSP_TxQPreviousIndex(U8StartLoc);//previous item
    U8ItemSeqNum = axTSP_TxQueue[U8StartLoc].U8SequenceNumber;
    if((U8ItemSeqNum & TSP_HEADER_SEQ_NUM_MASK) == U8SeqNumber)//Seq num found
    {
      axTSP_TxQueue[U8StartLoc].U8SequenceNumber = TSP_INVALID_SEQ_NUM;//Clear the item found
      if(U8TSP_TxQLastItem == TSP_GBN_PIPE_DEPTH) //Buffer was full, reset it
        U8TSP_TxQLastItem = U8TSP_TxQFirstItem;   //FI = LI -> clear

      U8TSP_TxQFirstItem = U8TSP_TxQNextIndex(U8StartLoc);//update used space start "pointer"


      //Remove earlyer sequence packets
      U8SequenceCatch = 0;
      while(U8SequenceCatch < TSP_GBN_PIPE_DEPTH)        //Build a list of old sequence numbers
      {
         aU8QueueSequences[U8SequenceCatch] = U8SeqNumber;
         U8SeqNumber = U8TSP_PreviousSeqNumber(U8SeqNumber); //Previous sequence number
         U8SequenceCatch++;
      }
      //Delete the older items from the list
      U8SequenceDelete = 0;                              //Send buffer sequence traverser
      while(U8SequenceDelete < TSP_GBN_PIPE_DEPTH)
      {
         U8SequenceCatch = 0;
         while(U8SequenceCatch < TSP_GBN_PIPE_DEPTH)        //Build a list of old sequence numbers
         {
            if((axTSP_TxQueue[U8SequenceDelete].U8SequenceNumber & TSP_HEADER_SEQ_NUM_MASK) == (aU8QueueSequences[U8SequenceCatch]))
               axTSP_TxQueue[U8SequenceDelete].U8SequenceNumber = TSP_INVALID_SEQ_NUM;//Delete the item

            U8SequenceCatch++;
         }
         U8SequenceDelete++;
      }


      return(1);                                  //item was found and deleted
    }
  }
  return(0);                                      //not found
}

//Resets the resend pointer to point to the earlyest item in the list
void vTSP_TxQClearResendPointer()
{
  if(U8TSP_TxQFirstItem == U8TSP_TxQLastItem)
  {
    U8TSP_TxQResendItem = TSP_GBN_PIPE_DEPTH;
    return;
  }
  U8TSP_TxQResendItem = U8TSP_TxQFirstItem;
}

U8 U8TSP_TxQCheckResendActive()
{
  if(U8TSP_TxQResendItem == TSP_GBN_PIPE_DEPTH) return(0);
  return(1);
}

//retruns the list item to be resent
// OK -> Listindex
// No -> "TSP_GBN_PIPE_DEPTH" (out of queue pointer)
U8 U8TSP_TxQGetNextResendIndex()
{
U8  U8ReturnValue = U8TSP_TxQResendItem;

  if(U8TSP_TxQResendItem == TSP_GBN_PIPE_DEPTH)
    return(TSP_GBN_PIPE_DEPTH);                   //Nothing to resend

  U8TSP_TxQResendItem = U8TSP_TxQNextIndex(U8TSP_TxQResendItem);

  if(U8TSP_TxQLastItem == TSP_GBN_PIPE_DEPTH)     //pipe is full
  {
    if(U8TSP_TxQResendItem == U8TSP_TxQFirstItem)
      U8TSP_TxQResendItem = TSP_GBN_PIPE_DEPTH;   //no more to resend
  }else{
    if(U8TSP_TxQResendItem == U8TSP_TxQLastItem)
      U8TSP_TxQResendItem = TSP_GBN_PIPE_DEPTH;   //no more to resend
  }

  return(U8ReturnValue);                          //return
}

//retruns the item index of the searched sequence number
// OK -> Listindex
// No -> "TSP_GBN_PIPE_DEPTH" (out of queue pointer)
U8 U8TSP_TxQFindSequenceIndex(U8 U8SeqNumber)
{
U8 U8ItemCount;

  if(U8TSP_TxQLastItem == U8TSP_TxQFirstItem) return(TSP_GBN_PIPE_DEPTH);//nothing in the queue

  U8ItemCount = 0;
  do{
    if((axTSP_TxQueue[U8ItemCount].U8SequenceNumber & TSP_HEADER_SEQ_NUM_MASK)== U8SeqNumber)
      return(U8ItemCount);                        //Item found
    U8ItemCount++;
  }while(U8ItemCount != TSP_GBN_PIPE_DEPTH);      //run through all the list items

  return(TSP_GBN_PIPE_DEPTH);                     //none found
}

void vTSP_TxQClear()
{
  //stop interrupts
  axTSP_TxQueue[0].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  axTSP_TxQueue[1].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  axTSP_TxQueue[2].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  axTSP_TxQueue[3].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  U8TSP_TxQLastItem = 0;
  U8TSP_TxQFirstItem = 0;
  U8TSP_TxQResendItem = TSP_GBN_PIPE_DEPTH;


  vDBG_LogState(StackFM_Transport, Info, "~~ Queue cleared");

  //continue interrupts
}


/* --------------------- Go-Back-N functionality ------------------------- */


U8 U8TSP_TransmitQueueItem(U8* U8DataDest, U8 U8ItemIndex)
{
	U8  U8PacketSize;
	U8  U8DataSize;
	U8* pfU8Data;
	
	U8* pU8aux;

  if(axTSP_TxQueue[U8ItemIndex].U8SequenceNumber == TSP_INVALID_SEQ_NUM)
    return(0);                                    //nothing to transmit

  axTSP_TxQueue[U8ItemIndex].U8TimerTicks = TSP_TX_TIMEOUT;

  pU8aux = &(axTSP_TxQueue[U8ItemIndex].U8RetriesLeft);
  *pU8aux--;
  
  *U8DataDest++ = axTSP_TxQueue[U8ItemIndex].U8SequenceNumber;
  *U8DataDest++ = axTSP_TxQueue[U8ItemIndex].U8PacketID;

  U8DataSize = axTSP_TxQueue[U8ItemIndex].U8PacketSize;
  U8PacketSize = U8DataSize + 2;
  pfU8Data = axTSP_TxQueue[U8ItemIndex].pfU8PacketStart;
  while(U8DataSize-- != 0)
    *U8DataDest++ = *pfU8Data++;

  return(U8PacketSize);                               //Data + Header ID
}


void vTSP_ResetGBNMachine()
{
  //Reset GoBackN sequence numbers
  U8TSP_NextOutSeq = 0;                           //Next Seq# to be transmited
  U8TSP_NextInSeq = 0;                            //Next Seq# to be received

  //Transmit que empty
  axTSP_TxQueue[0].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  axTSP_TxQueue[1].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  axTSP_TxQueue[2].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  axTSP_TxQueue[3].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
  U8TSP_TxQLastItem = 0;                          //init transmit queue params
  U8TSP_TxQFirstItem = 0;
  U8TSP_TxQResendItem = TSP_GBN_PIPE_DEPTH;       //nothing to resend


  //File statemachine init
  U8TSP_FileState = TSP_FILE_IDLE;                //No file transmission in progress
  //Command state init
  U8TSP_CmndState = TSP_CMND_IDLE;                //No commands in progress

  U8TSP_ReReceive = 0;

}

/* --------------------- Transport functionality ------------------------- */

//To transmit a data file:
// File Size
// File data
// File end
//
// This function implements a statemachine to split the file into its components


void vTSP_GetNextSegment(tTSP_SentItem* xAddItem)
//returns the number of bytes added in the transmission buffer
{
U8 U8SegmentSize = 0;
  switch(U8TSP_FileState)
  {
  case TSP_FILE_IDLE:                             //No file transmission in progress
    break;
  case TSP_FILE_SIZE:                             //File saze header to be transmitted
    (*xAddItem).U8PacketID = U8TSP_FileSizeID;    //Segment Identifier for file size
    //Byte packing == little-indian format
    aU8TSP_FileSizeBuffer[0] = (U8)((U32TSP_FileSize >> 0 ) & 0x000000FF);
    aU8TSP_FileSizeBuffer[1] = (U8)((U32TSP_FileSize >> 8 ) & 0x000000FF);
    aU8TSP_FileSizeBuffer[2] = (U8)((U32TSP_FileSize >> 16) & 0x000000FF);
    aU8TSP_FileSizeBuffer[3] = (U8)((U32TSP_FileSize >> 24) & 0x000000FF);
    (*xAddItem).U8PacketSize = 4;                 //4 bytes in this segment
    (*xAddItem).pfU8PacketStart = aU8TSP_FileSizeBuffer;//Where the data are
    U8TSP_FileState = TSP_FILE_DATA;
    break;
  case TSP_FILE_DATA:                             //File contents to be transmitted
    (*xAddItem).U8PacketID = U8TSP_FileDataID;       //Segment Identifier for file data
    U8SegmentSize = 0;                            //1 byte
    (*xAddItem).pfU8PacketStart = pfU8TSP_FileIndex;//Where data start

    while((U8SegmentSize < TSP_SEGMENT_SIZE_LIMIT) && (U32TSP_FileSize > 0))
    {
      *pfU8TSP_FileIndex++;                       //next byte of the file
      U32TSP_FileSize--;                          //one less to transmit
      U8SegmentSize++;                            //one more byte added
      if(pfU8TSP_FileIndex > pfU8TSP_DriveEnd)    //Disk wrapping (Ring buffers)
      {
        pfU8TSP_FileIndex = pfU8TSP_DriveStart;
        break;
      }
    }
    (*xAddItem).U8PacketSize = U8SegmentSize;     //Number of bytes in the segment (can be smaller than full size)
    if(U32TSP_FileSize == 0)
      U8TSP_FileState = TSP_FILE_EOF;
    break;
  case TSP_FILE_EOF:                              //End of file to be transmitted
    (*xAddItem).U8PacketID = U8TSP_FileDataID;    //Segment Identifier for file data
    (*xAddItem).pfU8PacketStart = pfU8TSP_FileIndex;//Where the file ended
    (*xAddItem).U8PacketSize = 0;                 //1 byte in this segment => empty packet == EOF
    U8TSP_FileState = TSP_FILE_IDLE;
    break;
  }

}

/* --------------------- Test functions ---------------------------------- */


/* ___PUBLIC FUNCTIONS____________________________________________________ */
void vTSP_Init()
{
  vTSP_ResetGBNMachine();

  //reset requests
  U8TSP_ResetRequestTimer = 0;                    //Do not request resets
  //Async transmissions
  U8TSP_ToBeACKed = TSP_INVALID_SEQ_NUM;          //Nothing to ACK
  U8TSP_ToBeNACKed = TSP_INVALID_SEQ_NUM;         //Nothing to NACK
  U8TSP_ToSend = TSP_SEND_CLEAR;                  //Noothing to Send

  //vDBG_LogState(StackFM_Transport, Info, "Init Ok");
	

}



void vTSP_OnNewPacket(U8* pU8Data, U16 U16Count)
{

  U8  U8TransportHeaderType;
  U8  U8TransportSequence;
  U8  U8PacketFound;
  
  U8TransportHeaderType = (*pU8Data) & TSP_HEADER_TYPE_MASK;
  U8TransportSequence = (*pU8Data) & TSP_HEADER_SEQ_NUM_MASK;
  pU8Data++;
  U16Count--;

  switch(U8TransportHeaderType)
  {
  case TSP_HEADER_TYPE_RESET:
    vTSP_ResetGBNMachine();                       //reset all statemachines
    U8TSP_ToSend |= TSP_SEND_VERSION;             //send a version info packet
    break;
  case TSP_HEADER_TYPE_ACK:
    U8PacketFound = U8TSP_TxQFindSequenceIndex(U8TransportSequence);
    if(U8PacketFound != TSP_GBN_PIPE_DEPTH)       //valid ACK
    {
/*#ifndef __DBG_GBN__
      if(axTSP_TxQueue[U8PacketFound].U8PacketSize == 0){}
#endif*/
    }
    U8TSP_TxQDeleteSeqNum(U8TransportSequence);   //Delete the Acked and older item(s) from the list
    break;
    
  case TSP_HEADER_TYPE_NACK:

    if(U8TSP_TxQFindSequenceIndex(U8TransportSequence) != TSP_GBN_PIPE_DEPTH)
    {                                             //Item found in the queue
      //U8TSP_TxQDeleteSeqNum(U8TSP_PreviousSeqNumber(U8TransportSequence));
      vTSP_TxQClearResendPointer();               //reset pointer for re-sending
      //go into resend mode
    }else{                                        //Item not in queue
      if(U8TSP_TxQFindSequenceIndex(U8TSP_PreviousSeqNumber(U8TransportSequence)) != TSP_GBN_PIPE_DEPTH)
      {
        vTSP_TxQClear();                          //All items received successfull, continue normal transmission
      }else{
        //error (invald sequencing)
      }
    }

    break;
  case TSP_HEADER_TYPE_DATA:
    if(U8TransportSequence == U8TSP_NextInSeq)
    {

      vALP_OnNewPacket(pU8Data,U16Count);         //Processs the packet
      U8TSP_ToBeACKed = TSP_HEADER_TYPE_ACK;      //ACK the packet
      U8TSP_ToBeACKed |= U8TransportSequence;
      U8TSP_NextInSeq = U8TSP_NextSeqNumber(U8TSP_NextInSeq);//update sequence numbers
      U8TSP_ReReceive = 0;
    }else{
      if(U8TSP_ReReceive != 1)
      {
        U8TSP_ToBeNACKed = TSP_HEADER_TYPE_NACK;    //Nack the waiting for packet
        U8TSP_ToBeNACKed |= U8TSP_NextInSeq;
        U8TSP_ReReceive = 1;
      }
    }
    if(U8TSP_ResetRequestTimer != 0)
    {
      U8TSP_ResetRequestTimer = 0;                  //No more auto reset requests
    }
    break;
  }
}

//Called when CRC fails
PUBLIC void vTSP_OnCRCError(){
	//vDBG_LogState(StackFM_Transport, Info, "vTSP_OnCRCError()\n");
}  //Not used in Go back N protocol

//Called every 1/16th of a second
PUBLIC void vTSP_OnTimerTick()
{
  U8 U8QueueIndex;
  U8* pU8aux;
  //reset request repetative timer
  if(U8TSP_ResetRequestTimer != 0)
  {
    if(U8TSP_ResetRequestTimer == TSP_RESET_REQUEST_TIMEOUT)
    {
      U8TSP_ResetRequestTimer = 1;                //Clear timer tick counter
      U8TSP_ToSend |= TSP_SEND_RESET;             //Add Reset request to async send queue
    }else{
      U8TSP_ResetRequestTimer++;                  //increment request timer tick counter
    }
  }

  //Check all packets for resending time
  U8QueueIndex = 0;
  while(U8QueueIndex != TSP_GBN_PIPE_DEPTH)
  {
    if(axTSP_TxQueue[U8QueueIndex].U8SequenceNumber != TSP_INVALID_SEQ_NUM)
    {
	  pU8aux = &(axTSP_TxQueue[U8QueueIndex].U8TimerTicks);
      if(*pU8aux > 0){//item did not timeout
        *pU8aux--;
		}

      if(axTSP_TxQueue[U8QueueIndex].U8RetriesLeft == 0)
      {
//cp        axTSP_TxQueue[U8QueueIndex].U8SequenceNumber = TSP_INVALID_SEQ_NUM;
//cp        vTSP_TxQClear();

        vALP_OnSendTimeout();                     //Timeed out sending a packet

        return;
      }
      if(axTSP_TxQueue[U8QueueIndex].U8TimerTicks == 0)//Item timed out
      {
        if(U8TSP_TxQCheckResendActive() == 0)
          vTSP_TxQClearResendPointer();           //Resend items queue
      }
    }
    U8QueueIndex++;
  }
}

void vTSP_GetNextPacket(U32* ppU8Data, U8* pU8Count)
// Set (*ppU8Data) = pointer to the data
// Set (*pU8Count) = data length
{
U8  U8ResendIndex;
U8  U8QueueAddPosistion;

  //Check for async send requests
  if(U8TSP_ToSend != TSP_SEND_CLEAR)              //Async request in the pipe
  {
    if((U8TSP_ToSend & TSP_SEND_RESET) == TSP_SEND_RESET)//Send reset request packet
    {
      //Reset GoBackN sequence numbers
      U8TSP_NextOutSeq = 0;                       //Next Seq# to be transmited
      U8TSP_NextInSeq = 0;                        //Next Seq# to be received

      //reset self
      aU8TSP_TxReset[0] = TSP_HEADER_TYPE_RESET;  //send a reset packet
      *ppU8Data = (U32)(&aU8TSP_TxReset );          //Set data source address pointer
      *pU8Count = 1;                              //1 byte to transmit
      U8TSP_ToSend ^= TSP_SEND_RESET;             //Clear the flag
      return;                                     //return the number of bytes...
    }
    if((U8TSP_ToSend & TSP_SEND_VERSION) == TSP_SEND_VERSION)//Send version packet
    {

      aU8TSP_TxAsync[0] = TSP_HEADER_TYPE_DATA;   //Data packet
      aU8TSP_TxAsync[1] = ALP_VERSION_ID;         //Version Info Packet ID
      aU8TSP_TxAsync[2] = TSP_VERSION;            //Transport Layer vesrion
      aU8TSP_TxAsync[3] = ALP_VERSION;            //Application layer version
      aU8TSP_TxAsync[4] = COMMS_DEVICE_TYPE;       //Communications device type
      *ppU8Data = (U32)(&aU8TSP_TxAsync);          //Set data source address pointer
      *pU8Count = 5;                              //5 bytes to transmit
      U8TSP_NextOutSeq = U8TSP_NextSeqNumber(U8TSP_NextOutSeq);//update sequence numbers
      U8TSP_ToSend ^= TSP_SEND_VERSION;           //Clear the flag

      return;                                     //return the number of bytes...
    }
  }

  if(U8TSP_ToBeACKed != TSP_INVALID_SEQ_NUM)      //New ACK to be send
  {
    aU8TSP_TxAckNack[0] = TSP_HEADER_TYPE_ACK;    //ACK to be sent
    aU8TSP_TxAckNack[0] |= U8TSP_ToBeACKed;       //Sequence to be acked
    U8TSP_ToBeACKed = TSP_INVALID_SEQ_NUM;        //Sent
    *ppU8Data = (U32)(&aU8TSP_TxAckNack); //original (U32)(aU8TSP_TxAckNack)         //source of data to transmit
    *pU8Count = 1;                                //1 byte to transmit

    return;
  }
  if(U8TSP_ToBeNACKed != TSP_INVALID_SEQ_NUM)     //New NACK to be send
  {
    aU8TSP_TxAckNack[0] = TSP_HEADER_TYPE_NACK;   //ACK to be sent
    aU8TSP_TxAckNack[0] |= U8TSP_ToBeNACKed;      //Sequence to be acked
    U8TSP_ToBeNACKed = TSP_INVALID_SEQ_NUM;       //Sent
    *ppU8Data = (U32)(&aU8TSP_TxAckNack);          //source of data to transmit
    *pU8Count = 1;                                //1 byte to transmit

    return;
  }


  //Check for resend items
  U8ResendIndex = U8TSP_TxQGetNextResendIndex();  //get next item to be resend, adjust to next resent item
  if(U8ResendIndex < TSP_GBN_PIPE_DEPTH)          //item found for retransmission
  {
    *ppU8Data = (U32)&aU8TSP_TxPacket;
    *pU8Count = U8TSP_TransmitQueueItem(aU8TSP_TxPacket,U8ResendIndex);


    return;                                       //return with new data
  }

  //check for free space
  if(U8TSP_TxQCheckFreeSpace() == 0)              //no free space to add an item
  {
    *pU8Count = 0;
    return;                                       //return if no free space
  }



  //check for new commands
  if(U8TSP_CmndState == TSP_CMND_NEW)             //New commands in the queue?
  {
    U8QueueAddPosistion = U8TSP_TxQGetAddIndex(); //Want to add an item, reserve a location
    axTSP_TxQueue[U8QueueAddPosistion].pfU8PacketStart = pfU8TSP_CommandIndex;
    axTSP_TxQueue[U8QueueAddPosistion].U8PacketID = U8TSP_CmndDataID;
    axTSP_TxQueue[U8QueueAddPosistion].U8PacketSize = U8TSP_CmndSize;
    axTSP_TxQueue[U8QueueAddPosistion].U8RetriesLeft = TSP_TX_MAX_RETRIES+1;
    axTSP_TxQueue[U8QueueAddPosistion].U8SequenceNumber = TSP_HEADER_TYPE_DATA;
    axTSP_TxQueue[U8QueueAddPosistion].U8SequenceNumber |= U8TSP_NextOutSeq;
    U8TSP_NextOutSeq = U8TSP_NextSeqNumber(U8TSP_NextOutSeq);//update sequence numbers

    U8TSP_CmndState = TSP_CMND_IDLE;              //A new commsnd is ready to be transmitted

    *ppU8Data = (U32)&aU8TSP_TxPacket;             //Transmit packet location
    *pU8Count = U8TSP_TransmitQueueItem(aU8TSP_TxPacket,U8QueueAddPosistion);

    return;                                       //Return with new data
  }

  //check for new data
  if(U8TSP_FileState != TSP_FILE_IDLE)            //there are file data to be transmitted
  {
    U8QueueAddPosistion = U8TSP_TxQGetAddIndex(); //Want to add an item, reserve a location
    vTSP_GetNextSegment(&axTSP_TxQueue[U8QueueAddPosistion]);
    axTSP_TxQueue[U8QueueAddPosistion].U8RetriesLeft = TSP_TX_MAX_RETRIES+1;
    axTSP_TxQueue[U8QueueAddPosistion].U8SequenceNumber = TSP_HEADER_TYPE_DATA;
    axTSP_TxQueue[U8QueueAddPosistion].U8SequenceNumber |= U8TSP_NextOutSeq;
    U8TSP_NextOutSeq = U8TSP_NextSeqNumber(U8TSP_NextOutSeq);//update sequence numbers

    *ppU8Data = (U32)&aU8TSP_TxPacket;             //Transmit packet location
    *pU8Count = U8TSP_TransmitQueueItem(aU8TSP_TxPacket,U8QueueAddPosistion);

    return;                                       //Return with new data
  }

  *pU8Count = 0;                                  //nothing to send
}

void vTSP_WriteData(                              //data block from upper layer
                U8 U8SizeDescryptor,              //Packet ID for Size descryptor
                U8 U8Descryptor,                  //PAcket ID for Data descryptor
                U8* pU8Data,                  //Pointer to the first byte to be sent
                U32 U32Size                       //number of bytes to be sent
            )
{
  U8TSP_FileSizeID = U8SizeDescryptor;            //ID to mark the file size data
  U8TSP_FileDataID = U8Descryptor;                //ID to mark the file contents data
  pfU8TSP_FileIndex = pU8Data;                    //Current file location pointer
  U32TSP_FileSize = U32Size;                      //Number of bytes to be transmitted
  pfU8TSP_DriveStart = pU8Data;                   //Start of the Ringbuffer (Drive) containing the file
  pfU8TSP_DriveEnd = pU8Data + U32Size-1;         //End of the Ringbuffer (Drive) containing the file
  U8TSP_FileState = TSP_FILE_SIZE;                //File transmission statemachine = transmit file size
}

void vTSP_WriteDataBuffer(                        //data block from upper layer
                U8 U8SizeDescryptor,              //Packet ID for Size descryptor
                U8 U8Descryptor,                  //PAcket ID for Data descryptor
                U8* pU8Data,                  //Pointer to the first byte to be sent
                U32 U32Size,                      //number of bytes to be sent
                U8* pU8BlockStart,            //Address of the first byte of the "ring buffer"
                U8* pU8BloackEnd              //Address of the last byte of the "ring buffer"
            )
{
  U8TSP_FileSizeID = U8SizeDescryptor;            //ID to mark the file size data
  U8TSP_FileDataID = U8Descryptor;                //ID to mark the file contents data
  pfU8TSP_FileIndex = pU8Data;                    //Current file location pointer
  U32TSP_FileSize = U32Size;                      //Number of bytes to be transmitted
  pfU8TSP_DriveStart = pU8BlockStart;             //Start of the Ringbuffer (Drive) containing the file
  pfU8TSP_DriveEnd = pU8BloackEnd;                //End of the Ringbuffer (Drive) containing the file
  U8TSP_FileState = TSP_FILE_SIZE;                //File transmission statemachine = transmit file size
}

void vTSP_WriteCommand(U8 Descryptor, U8* pU8Parameters, U8 U8ParamSize)
{
  U8TSP_CmndDataID = Descryptor;                  //ID to mark the command contents data
  pfU8TSP_CommandIndex = pU8Parameters;           //Pointer to the commsnd data
  U8TSP_CmndSize = U8ParamSize;                   //Number of bytes to be transmitted
  U8TSP_CmndState = TSP_CMND_NEW;                 //A new commsnd is ready to be transmitted
}

void vTSP_RequestReset()
{
  U8TSP_ResetRequestTimer = 1;                    //Do not request resets
  U8TSP_ToSend |= TSP_SEND_RESET;                 //Add a reset request to the queue
}

