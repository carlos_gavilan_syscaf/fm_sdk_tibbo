#ifndef __HDLC_H__
#define __HDLC_H__
/* ==========================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        Simplified HDLC Protocol Layer
AUTHOR:           P.J. Conradie
DESCRIPTION:      This component provides HDLC encapsulation for data packets

         e.g. [7E] .. (all 7E data escaped with 7D) .. [CRC lo] [CRC hi] [7E]

VERSION:          $Revision: 2 $
DATED:            $Date: 15/12/00 9:40 $
LAST MODIFIED BY: $Author: Pieterc $

========================================================================== */

/* ___PROJECT INCLUDES____________________________________________________ */
#include "..\System\cifms.h"

/* ___TYPE DEFINITIONS____________________________________________________ */

// Type definition of the function pointer that will be called
// on to transmit the data over the serial interface,
//typedef void (*tHDLC_TxBuffer)(U8* pBuffer, U16 U16Count);

// i.e. a user function such as this must be supplied
// that will send data over RS232:
// -------------------------------------------------
// void vSERIAL_TxBuffer(U8* pBuffer, U16 U16Count);
// -------------------------------------------------

// To install the function, do the following in the user code:
// ...
// vHDLC_SetTxHandler(vSERIAL_TxBuffer);

/* ___PUBLIC FUNCTION PROTOTYPES__________________________________________ */
EXTERN PUBLIC void vHDLC_Init        ();
EXTERN PUBLIC void vHDLC_TxBuffer    (U8* pU8Data, U16 U16Count);
EXTERN PUBLIC void vHDLC_OnRxData    (string<1> U8Data);

/* ___MACROS______________________________________________________________ */

#endif

