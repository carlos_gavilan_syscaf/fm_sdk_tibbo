#ifndef __TRANSPORT_H__
#define __TRANSPORT_H__
/*
=============================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        Transport Layer
AUTHOR:           Carlo Putter
DESCRIPTION:      This component provides comms transport layer functionality

VERSION:          $Revision: 19 $
DATED:            $Date: 01/08/20 15:39 $
LAST MODIFIED BY: $Author: Carlop $

=============================================================================
*/

/* ___PROJECT INCLUDES____________________________________________________ */

#include "..\System\cifms.h"
#include "hdlc.h"


/* ___TYPE DEFINITIONS____________________________________________________ */
#define TSP_GBN_PIPE_DEPTH    4                   //Number of packet in the transmission pipe

#define TSP_DATA_BUFFER_SIZE    140   // Number of byte in the transmission packet
#define TSP_HEADER_TYPE_MASK    0xC0  // 1100 0000
#define TSP_HEADER_TYPE_RESET   0x00  // 0000 0000
#define TSP_HEADER_TYPE_ACK     0x40  // 0100 0000
#define TSP_HEADER_TYPE_NACK    0x80  // 1000 0000
#define TSP_HEADER_TYPE_DATA    0xC0  // 1100 0000

#define TSP_HEADER_SEQ_NUM_MASK 0x3F  // 0011 1111
#define TSP_INVALID_SEQ_NUM     0xFF  // Not a valid sequence number
#define TSP_MAX_SEQUENCES       0x03  // Maximum of 2 sequences in the pipe

#define TSP_TX_TIMEOUT          8*16  // retransmit (within 8 seconds  == [8*16] * 1/16[s] ) {<=15sec}
//#define TSP_TX_MAX_RETRIES      3     // retry to transmit only 3 times
#define TSP_TX_MAX_RETRIES      TSP_GBN_PIPE_DEPTH + 1// retry one more time than the pipe depth

#define TSP_RESET_REQUEST_TIMEOUT 80  //5 seconds retransmission interval

/* ___PUBLIC FUNCTION PROTOTYPES__________________________________________ */
//To be implemented in transport.cpp
EXTERN PUBLIC void vTSP_Init           ();
EXTERN PUBLIC void vTSP_OnNewPacket    (U8* pU8Data, U16 U16Count);
EXTERN PUBLIC void vTSP_OnCRCError     ();
EXTERN PUBLIC void vTSP_OnTimerTick    ();
EXTERN PUBLIC void vTSP_GetNextPacket  (U32* ppU8Data, U8* pU8Count);

EXTERN PUBLIC void vTSP_WriteData      (                         //data block from upper layer
                                        U8      U8SizeDescriptor,//Packet ID for Size descriptor
                                        U8      U8Descryptor,    //Packet ID for Data descriptor
                                        U8*     pU8Data,         //Pointer to the first byte to be sent
                                        U32     U32Size);        //number of bytes to be sent

EXTERN PUBLIC void vTSP_WriteDataBuffer(                         //data block from upper layer
                                        U8      U8SizeDescriptor,//Packet ID for Size descriptor
                                        U8      U8Descriptor,    //Packet ID for Data descriptor
                                        U8*     pU8Data,         //Pointer to the first byte to be sent
                                        U32     U32Size,         //number of bytes to be sent
                                        U8* pU8BlockStart,   //Address of the first byte of the "ring buffer"
                                        U8* pU8BlockEnd);    //Address of the last byte of the "ring buffer"

EXTERN PUBLIC void vTSP_WriteCommand   (U8 U8Descriptor, U8* pU8Parameters, U8 U8ParamSize);
EXTERN PUBLIC void vTSP_RequestReset   ();

#endif
