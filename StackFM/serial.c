/*
 * serial.cpp
 *
 *  Created on: 5/08/2015
 *      Author: Alejandro.Morales
 */

/* ___PROJECT INCLUDES____________________________________________________ */
#include "serial.h"

int ser_port_sdk = -1;
//string sTxBuffer;

/* ___PUBLIC FUNCTIONS____________________________________________________ */
PUBLIC tBOOL bSERIAL_Open(int iCommPort, int iBaudRate)
{
   //8,COMM_NO_PARITY,COMM_1_STOP_BIT,COMM_HARDWARE_FLOW_CONTROL,FALSE))

    ser_port_sdk = iCommPort;
	ser.num = ser_port_sdk;

	
	switch(iBaudRate){
		 case 0: 
			ser.baudrate=ser.div9600*8; //9600/1200=8
			break;

		 case 1: 
			ser.baudrate=ser.div9600*4; //9600/2400=4
			break;

		 case 2: 
			ser.baudrate=ser.div9600*2; //9600/4800=2
			break;

		 case 3: 
			ser.baudrate=ser.div9600;   //9600/9600=1
			break;

		 case 4: 
			ser.baudrate=ser.div9600/2; //19200/9600=2
			break;

		default: 
			ser.baudrate=ser.div9600/4; //38400/9600=4
			break;
		}

	
	ser.mode = PL_SER_MODE_UART;
	ser.parity = PL_SER_PR_NONE; //PL_SER_PR_MARK: parity bit always at "1". Second stop bit
	ser.bits = PL_SER_BB_8;
	ser.flowcontrol = DISABLED;//ENABLED; //en el codigo original estaba disabled
	ser.interchardelay	= 0;						// retardo en recivir el siguiente caracter
	ser.esctype			= PL_SER_ET_DISABLED;		// identifica caracteres especiales
	
	ser.interface = PL_SER_SI_FULLDUPLEX;
	
	io.num				= RX_FM; // Se identifica el pin a configurar del puerto serial utilizado
	io.enabled			= NO;					// Se configura como IN
	
	io.num				= TX_FM;					// Se identifica el pin a configurar del puerto serial utilizado
	io.enabled			= YES;					// Se configura como OUT
	
	//config cts
	io.num = CTS_FM; //PL_IO_NUM_15
	io.enabled = NO; // Se configura como IN
	
	ser.ctsmap = CTS_FM;
	
	//config rts
	io.num = RTS_FM; //PL_IO_NUM_14
	io.enabled = YES;
	io.state   = LOW;
	ser.rtsmap = RTS_FM;

	ser.rxbuffrq (6);							// Numero de paginas por buffer de recepcion (tama�o de pagina - 256 bytes) en cada resepcion el tema�o de los datos es 128 bytes mas la cabecera y mas cosas por eso se ponen 4 hojas
	ser.txbuffrq (2);
	sys.buffalloc();
	
	ser.enabled			= YES;					// Habilito el puerto con la configuracion
	
	return eTRUE;
}

PUBLIC tBOOL bSERIAL_Close()
{
	if(ser_port_sdk != -1){
		ser.num = ser_port_sdk;
		ser.enabled = 0;
		return eTRUE;
	} else return eFALSE;
}

/*PUBLIC U16 U16SERIAL_RxData(U8* pU8Buffer, U16 U16Count)
{
	U16 U16BufferLen; //size rx buffer
	U8* pU8Buffer_aux;
	string sBuffer;
	
	sBuffer = ser.getdata(U16Count); 
	U16BufferLen= U16Count;//ser.rxlen;
	pU8Buffer_aux = sBuffer;
	while(U16Count-- > 0) *(pU8Buffer++) = *(pU8Buffer_aux++);

	return U16BufferLen;
}*/

/*
PUBLIC void vSERIAL_TxData(U8* pU8Buffer, U16 U16Count)
{
	
	U8* psBuffer;
	
	tBOOL cont = eTRUE;
	sTxBuffer = "";
	
	psBuffer = sTxBuffer;
	if(U16Count >=255) vDBG_LogState(StackFM_PHY, Info, "vSERIAL_TxData: U16Count >=255\n");

	while(U16Count-- > 0) {		
		sTxBuffer += "0";
		*(psBuffer++) = *(pU8Buffer++);
	}
	
	ser.num = ser_port_sdk;
	//ser.notifysent(ser.txbuffsize-len(sTxBuffer)) ;
	ser.setdata(sTxBuffer);
	ser.send();
	
}*/




		