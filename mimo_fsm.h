#ifndef __MIMO_FSM_H__
#define __MIMO_FSM_H__

/**
 * @file   mimo_fsm.h
 * @Author Me (me@example.com)
 * @date   September, 2008
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 */
 
#define PERIOD_FOR_FILE_TRANSFER 	1000
#define MIMO_REQUEST_PERIOD		 	50

#include "StackFM\fm.h"
#include "StackFM\AppLayer.h"
#include "System\system_config.h"
#include "System\date.h"
#include "dfb_fsm.h"


enum MIMO_STATES
{
	// Application's state machine's initial state. 
	MIMO_STATE_INIT=0,
	
	MIMO_STATE_IDLE,
	MIMO_SEARCHING_FM,
	MIMO_REQUEST_VEHICLE_ID_VALUE,
	MIMO_SAVE_VEHICLE_ID,
	MIMO_REQUEST_FM_TIME,
	MIMO_SET_TIBBO_TIME,
	
	MIMO_LOG_IN_FTP_SERVER,

	MIMO_REQUEST_IGNITION_VALUE,
	MIMO_EVAL_TRIP_STATE,
	MIMO_UPDATE_VELOCITY,

	MIMO_WAITING_FOR_START_FILE_TRANSFER,
	MIMO_TRANSFERING_FILES,

	MIMO_WAITING_FILE_DOWNLOAD,
	MIMO_WAITING_FILE_UPLOAD,
	MIMO_WAITING_ACRONYM_VALUE,


	MIMO_STATE_PAUSED,
	MIMO_STATE_RESUMING,
	MIMO_STATE_SLEEPING,
	MIMO_STATE_EXIT,

	MIMO_ERROR

} ;

enum TRIP_STATES
{
   UNDEFINED = 0,
   TRIP_INPROGRESS,
   TRIP_ENDED
} ;

enum FILE_TRANSFER_STATES
{
	ANY_FILES_TRANSFERRED = 0,
	START_DOWNLOAD_TACHO,
	START_UPLOAD_TACHO,
	START_DOWNLOAD_EVENT,
	START_UPLOAD_EVENT,
	ALL_FILES_TRANSFERED
};
// *****************************************************************************
/* Mimo Data

  Resumen:
    Almacena los datos que utiliza la maquina de estados.

  Descripcion:
    Esta estructura almacena los datos que utiliza la maquina de estados.

  Observaciones:
    strings y buffers son definidos fuera de esta estructura.
 */

struct MIMO_DATA
{
    /* The application's current state */
    MIMO_STATES state;
	MIMO_STATES nextState;
	
	FILE_TRANSFER_STATES fileTransferState;
    TRIP_STATES tripState;
	
	U32 acronymValue;
	U32 acronymCode;
	
	U8 Velocity;
	U8 errorCount;
	
	tBOOL FM_Offline;

};


EXTERN PUBLIC void  vMimo_Initialize();
EXTERN PUBLIC void  vMimo_Tasks();
EXTERN PUBLIC tBOOL bMimo_Pause();
EXTERN PUBLIC tBOOL bMimo_Sleep(tTMR_Handle sleep_period);
EXTERN PUBLIC void  vMimo_Continue();

EXTERN PUBLIC U8    U8Mimo_getVelocity();
EXTERN PUBLIC tBOOL bMimo_HasTravelInProgress();

void vMimo_RequestAcronymValue(U32 Acronym, MIMO_STATES xNextState);
void vMimo_DownloadFileFromFM(U8 eDownloadType);
void vMimo_UploadFileToFTP(string filename);

void vFileTransferTasks();

#endif 


