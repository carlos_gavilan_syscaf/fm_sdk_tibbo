/* ___PROJECT INCLUDES____________________________________________________ */

#include "mimo_fsm.h"
#include "Net\ftp_client.h"
/* ___LOCAL VARIABLES_____________________________________________________ */
// Global timer tracking variable


tTMR_Handle	hMimo_StartFileTransferTimer;
tTMR_Handle	hFM_TimeoutTimer;

MIMO_DATA mimoData;

void vMimo_Initialize(){

	if (bSERIAL_Open(SER_NUM_FM,br19200) == eFALSE)
		vDBG_LogError(StackFM_PHY, ErrUnableToOpenConnection, "Could not open Serial Port for SDK");
	
	vFM_Initialize();
	
	mimoData.errorCount = 0;
	
	hFM_TimeoutTimer = hTMR_StartTimer(FM_TIMEOUT);
	mimoData.state = MIMO_STATE_INIT;//MIMO_REQUEST_VEHICLE_ID_VALUE;
}

void vMimo_Tasks(){

  
    vFM_ProcessRxTx();
	
	
    /**
     * State machine implementation:
     * @statemachine
     */
	 
	switch ( mimoData.state )
    {
		/** Label "Initial" */
		/* For simple states like this one,
		 * all you have to do is give the state a label.
		 */
        case MIMO_STATE_INIT:
        {     
			mimoData.fileTransferState = ANY_FILES_TRANSFERRED;
			mimoData.tripState = UNDEFINED;
			mimoData.Velocity = 0;
			mimoData.acronymValue = 0;
			mimoData.acronymCode = 0;
	
			mimoData.state = MIMO_SEARCHING_FM;
			
			vDBG_LogState(MimoFsm, Info, "searching fm device...");
            break;
        }
		case MIMO_SEARCHING_FM:
        {     
			if(bALP_HasCommsBeenEstablished()){
				mimoData.errorCount = 0;
				mimoData.state = MIMO_REQUEST_VEHICLE_ID_VALUE;
			} /*else {
				mimoData.state = MIMO_STATE_INIT;
			}*/
			vTSP_RequestReset();
	
            break;
        }
		
		case MIMO_REQUEST_VEHICLE_ID_VALUE:
		{
			vMimo_RequestAcronymValue(VEHID, MIMO_SAVE_VEHICLE_ID);
			break;
		}
		
		case MIMO_SAVE_VEHICLE_ID:
		{
			setALP_VehicleIDValue((U16)mimoData.acronymValue);
			vDBG_LogState(MimoFsm, Info, "Vehicle ID: "+str(mimoData.acronymValue));
			mimoData.state = MIMO_REQUEST_FM_TIME;
			break;
		}
		
		case MIMO_REQUEST_FM_TIME:
		{
			vMimo_RequestAcronymValue(TIMES, MIMO_SET_TIBBO_TIME);
			break;
		}
		
		case MIMO_SET_TIBBO_TIME:
		{
			setTibboTimeFromEpoch(mimoData.acronymValue, 0);  //GMT_BOG
			vDBG_LogState(MimoFsm, Info, "Tibbo date set: "+getDateFromTibbo());
			mimoData.state = MIMO_STATE_RESUMING;
			break;
		}
		
		case MIMO_STATE_PAUSED:
		{
			break;
		}
		
		case MIMO_STATE_SLEEPING:
		{
			if(bTMR_HasTimerExpired(hFM_TimeoutTimer) == eTRUE ){
				vDBG_LogState(MimoFsm, Info, "sleep time ended, I woke up!");
				mimoData.state = MIMO_STATE_EXIT;
			}
			break;
		}
		
		case MIMO_STATE_RESUMING:
		{
			vTSP_RequestReset();
			
			//vFM_Reset();
			
			bDataFrameBuilder_Pause();
			
			hFM_TimeoutTimer = hTMR_StartTimer(MIMO_REQUEST_PERIOD);
			
			mimoData.state = MIMO_STATE_IDLE;
			break;
		}
		
		case MIMO_STATE_IDLE:
		{
			if(bTMR_HasTimerExpired(hFM_TimeoutTimer) == eTRUE ){
				hFM_TimeoutTimer = hTMR_StartTimer(FM_TIMEOUT);
				mimoData.state = MIMO_REQUEST_IGNITION_VALUE;
			}
			break;
		}
		
		case MIMO_STATE_EXIT:
		{
			vDataFrameBuilder_Continue();
			break;
		}
	
		case MIMO_REQUEST_IGNITION_VALUE:
		{
			//vDBG_LogState(MimoFsm, Info, "request fm mode");
			vMimo_RequestAcronymValue(FM_MD, MIMO_EVAL_TRIP_STATE); //IGNON
			break;
		}
		
		
		case MIMO_EVAL_TRIP_STATE:
		{
			mimoData.state = MIMO_STATE_EXIT;
			//1
			if(mimoData.acronymValue == 0 && (mimoData.tripState == UNDEFINED || mimoData.tripState == TRIP_ENDED)){
				mimoData.tripState = TRIP_INPROGRESS;
				mimoData.fileTransferState = ANY_FILES_TRANSFERRED;
				vDBG_LogState(MimoFsm, Info, "Trip In progress. Requesting velocity every second");
			}
			
			if(mimoData.tripState == TRIP_INPROGRESS){			
				vMimo_RequestAcronymValue(SPDKH, MIMO_UPDATE_VELOCITY);
				//vDBG_LogState(MimoFsm, Info, "request velocity");
			}
			//0
			if(mimoData.acronymValue == 1 && mimoData.tripState == TRIP_INPROGRESS){
				mimoData.tripState = TRIP_ENDED;
				vDBG_LogState(MimoFsm, Info, "Trip Ended. Wait period for file transfer");
				
				hMimo_StartFileTransferTimer = hTMR_StartTimer(PERIOD_FOR_FILE_TRANSFER);
				mimoData.state = MIMO_WAITING_FOR_START_FILE_TRANSFER;
				
			}
			break;			
		}
		
        case MIMO_UPDATE_VELOCITY:
        {
			mimoData.Velocity = mimoData.acronymValue;
			mimoData.state = MIMO_STATE_EXIT;
			/*sys.debugprint("Velocidad: "+str(mimoData.Velocity)+"\n");
			if(mimoData.Velocity < 4){
				vMimo_RequestAcronymValue(DRVID, MIMO_EVAL_DRIVER_ID);
			} */
			
            break;
        }
		
		/*case MIMO_EVAL_DRIVER_ID:
		{
			if(mimoData.acronymValue != U16OSIP_GetDriverId()){
				bMimo_Sleep(200);
				vFM_IdentifyDriver(U16OSIP_GetDriverId());
			} else {
				mimoData.state = MIMO_STATE_EXIT;
			}
			break;
		}*/
			
		case MIMO_WAITING_FOR_START_FILE_TRANSFER:
		{
			
			if (mimoData.acronymValue == 0 ){ //1
				mimoData.state = MIMO_STATE_EXIT;
				
			} else if(bTMR_HasTimerExpired(hMimo_StartFileTransferTimer) == eTRUE ){
					//vFTP_Loggin(IVU_USERNAME, IVU_PWD);
					mimoData.state = MIMO_TRANSFERING_FILES;//MIMO_LOG_IN_FTP_SERVER;
			} else {
				vMimo_RequestAcronymValue(FM_MD, MIMO_WAITING_FOR_START_FILE_TRANSFER); //IGNON
			}
			
			
			break;
		}
		/*case MIMO_LOG_IN_FTP_SERVER:
		{
			vDBG_LogState(MimoFsm, Info, "Log-in FTP server!");
			vFTP_Loggin(IVU_USERNAME, IVU_PWD);
			mimoData.state = MIMO_TRANSFERING_FILES;
			break;
		}*/
		case MIMO_TRANSFERING_FILES:
		{
			/*if (bFTP_isLoggedOn()) {
				vFileTransferTasks();
			}
			else if (bFTP_ErrorFlagActive()) {
				vDBG_LogError(MimoFsm, ErrDataWrong, "Log-in error. Lost connection");
				mimoData.state = MIMO_ERROR;
			}*/
			vFileTransferTasks();
			
			break;
		}

        case MIMO_WAITING_FILE_DOWNLOAD:
        {
            if(bALP_HasFileDownloaded() == eTRUE)
			{
				mimoData.state = MIMO_TRANSFERING_FILES;
			} else if (bALP_HasErrorOcurred() == eFALSE){
				hFM_TimeoutTimer = hTMR_StartTimer(100);
			} else if(bTMR_HasTimerExpired(hFM_TimeoutTimer) == eTRUE){
				  vDBG_LogError(MimoFsm, ErrTimeOut, "while downloading file");
				  mimoData.state = MIMO_ERROR;
			}
            break;
        }
		
		case MIMO_WAITING_FILE_UPLOAD:
		{
			if(bFTP_isFileUploaded()){
				vDBG_LogState(MimoFsm, Info, "File uploaded!");
				//vFTP_Quit();
				//mimoData.state = MIMO_LOG_IN_FTP_SERVER;
				mimoData.state = MIMO_TRANSFERING_FILES;
			} else if (bFTP_ErrorFlagActive()) {
				vDBG_LogError(MimoFsm, ErrDataWrong, "Upload file error");
				mimoData.state = MIMO_ERROR;
			}	
			break;	
		}
		
		case MIMO_WAITING_ACRONYM_VALUE:
		{
			if (bALP_GetAcronymValue(&(mimoData.acronymCode), 
									 &(mimoData.acronymValue)) == eTRUE)
			{
				mimoData.state = mimoData.nextState;
			} else if (bTMR_HasTimerExpired(hFM_TimeoutTimer) == eTRUE){
				vDBG_LogState(MimoFsm, ErrTimeOut, "while waiting acronym value");
				mimoData.state = MIMO_ERROR;
			}
			
			break;
		}
		
		case MIMO_ERROR:
        {
			if (mimoData.errorCount == 3){
				vDBG_LogError(MimoFsm, ErrFatal, "3+ Errors, FM possibly offline!\n");
				vFM_Reset();
				mimoData.state = MIMO_STATE_INIT;
				
				//sys.reboot();
			} else {
				mimoData.errorCount += 1;
				mimoData.state = MIMO_STATE_EXIT;//MIMO_STATE_IDLE;
			}
			
            break;
        }
		default:
			break;
	}
	/**
     * @endstatemachine
     */
}

void vFileTransferTasks()
{
	static string filename;
	switch(mimoData.fileTransferState){
		case ANY_FILES_TRANSFERRED:
		{
			filename = SYS_TEMP_FILENAME + str(getALP_VehicleIDValue()) +"_xxxx_" + getDateFromTibbo() ;
			mimoData.fileTransferState = START_DOWNLOAD_TACHO;
			break;
		}
			
		case START_DOWNLOAD_TACHO:
		{
			vDBG_LogState(MimoFsm, Info, "Try to start tacho download...");
			vMimo_DownloadFileFromFM(ALP_FULL_TACHO_DOWNLOAD);
			//si todo esta bien, cuando regrese a la maquina de estados, empezar�
			//a subir el archivo de tacho al servidor FTP.
			mimoData.fileTransferState = START_UPLOAD_TACHO;
			break;
		}
			
		case START_UPLOAD_TACHO:
		{
			vDBG_LogState(MimoFsm, Info, "Try to start tacho upload...");
			vMimo_UploadFileToFTP(filename+".tmp");
			mimoData.fileTransferState = START_DOWNLOAD_EVENT;
			break;
		}
			
			
		case START_DOWNLOAD_EVENT:
		{
			vDBG_LogState(MimoFsm, Info, "Try to start event download...");
			vMimo_DownloadFileFromFM(ALP_FULL_TACHO_DOWNLOAD); //ALP_FULL_EVENT_DOWNLOAD
			mimoData.fileTransferState = START_UPLOAD_EVENT;
			break;
		}
			
		case START_UPLOAD_EVENT:
		{
			vDBG_LogState(MimoFsm, Info, "Try to start event upload...");
			vMimo_UploadFileToFTP(filename+".tdt");
			mimoData.fileTransferState = ALL_FILES_TRANSFERED;
			break;
		}
			
		case ALL_FILES_TRANSFERED:
		{
			vDBG_LogState(MimoFsm, Info, "All files transfered, ftp quit.");
			vFTP_Quit();
			mimoData.state = MIMO_STATE_EXIT;
			break;
		}
	}
}


U8  U8Mimo_getVelocity(){
	return mimoData.Velocity;
}


tBOOL bMimo_HasTravelInProgress()
{
	if (mimoData.tripState == TRIP_INPROGRESS) return eTRUE;
	else return eFALSE;
}


tBOOL bMimo_Pause()
{
	//vDBG_LogState(MimoFsm, Info, "Mimo Paused!");
	switch(mimoData.state){
		case MIMO_WAITING_FILE_DOWNLOAD:
		case MIMO_WAITING_FILE_UPLOAD:
		//case MIMO_WAITING_ACRONYM_VALUE:
		case MIMO_TRANSFERING_FILES:
			return eFALSE;
			break;
		
		default:
			break;
			
	}
	vTSP_RequestReset();
	mimoData.state = MIMO_STATE_PAUSED;
	return eTRUE;
}

tBOOL bMimo_Sleep(tTMR_Handle sleep_period){
	tBOOL result = eFALSE;
	if(bMimo_Pause()){
		
		hFM_TimeoutTimer = hTMR_StartTimer(sleep_period);
		mimoData.state = MIMO_STATE_SLEEPING;
		result = eTRUE;
	}
	return result;
}

void vMimo_Continue()
{
	//vDBG_LogState(MimoFsm, Info, "Mimo Continue!");
	
	mimoData.state = MIMO_STATE_RESUMING;
}

void vMimo_RequestAcronymValue(U32 Acronym, MIMO_STATES xNextState){
	
	mimoData.nextState = xNextState;

	if(bALP_RequestAcronymValue(Acronym)){
		
		hFM_TimeoutTimer = hTMR_StartTimer(FM_TIMEOUT);
		
		mimoData.FM_Offline = eFALSE;
		mimoData.state = MIMO_WAITING_ACRONYM_VALUE;
	} else if (bTMR_HasTimerExpired(hFM_TimeoutTimer) == eTRUE){
		vDBG_LogError(MimoFsm, ErrTimeOut, "Request Acronym");
		mimoData.state = MIMO_ERROR;
	}

}

void vMimo_DownloadFileFromFM(U8 eDownloadType){

	if(bALP_StartDownload(SYS_TEMP_FILENAME, (tALP_DownloadType)eDownloadType)){
		mimoData.state = MIMO_WAITING_FILE_DOWNLOAD;
	}else {
		vDBG_LogError(MimoFsm, ErrUnableToRead, "Can't start download");
		mimoData.state = MIMO_ERROR;
	}
}

void vMimo_UploadFileToFTP(string filename){
	
	if (!bFTP_isLoggedOn()) {
		vDBG_LogState(MimoFsm, Info, "Log-in FTP server!");
		vFTP_Loggin(IVU_USERNAME, IVU_PWD);
	}
	
	while(!bFTP_isLoggedOn()) {
		doevents;

		if (bFTP_ErrorFlagActive()) {
			vDBG_LogError(MimoFsm, ErrDataWrong, "Log-in error. Lost connection");
			mimoData.state = MIMO_ERROR;
			break;
		} 
	}
	
			
	vFTP_StoreFile(SYS_TEMP_FILENAME, filename); 
	mimoData.state =  MIMO_WAITING_FILE_UPLOAD;
}