#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "cifms.h"
#include "system_config.h"
#include "date.h"
#include "..\dfb_fsm.h"

#define DBG_CTRL_PORT       "1000"
#define DBG_CTRL_PORT_NUM   1000

#define ACTIVE_VERBOSE_MODE		"av"
#define DEACTIVE_VERBOSE_MODE	"dv"
#define READ_LOGFILE			"rl"
#define CLEAR_LOGFILE			"cl"
#define FORMAT_MIMO				"fd"
#define REBOOT_MIMO				"rb"
#define READ_DATA_FRAME			"rd"

//this structure contains error codes compatibles with old Mimo source code.
struct CompErrorCode {
	U8 OsipClientError; //U8_UDP_ERROR
	U8 FtpClientError;  //U8_FTP_ERROR
	U8 StackFMError;    //U8_SDK_ERROR
	U8 DataFrameBuilderError;    //U8_JBX_ERROR y U8_UC_ERROR
	U8 SystemError;		//No existia anteriormente;
};

enum MODULE_CODE {
	StackFM_FM = 1,
	StackFM_AppLayer,
	StackFM_Transport,
	StackFM_HDLC,
	StackFM_PHY,
	
	FtpClient,
	OsipClient,
	
	KimaxListener,
	MemoListener,
	
	MimoFsm,
	DataFrameBuilderFsm,
	
	System_FlashDisk,
	System_RTC,
	
	MIMO = 15
	
};

enum TYPE_CODE {
	ErrTimeOut = 1,
	ErrUnknowCode,
	ErrInvalidResponse,
	ErrUnableToOpenConnection,
	ErrUnableToAuthenticate,
	ErrUnableToWrite,
	ErrUnableToRead,
	ErrUploadFile,
	ErrDownloadFile,
	ErrDataWrong,
	ErrFatal,
	
	Info = 15
};



EXTERN PUBLIC void  vDBG_Initialize();

EXTERN PUBLIC void  vDBG_ProcessCode( string<2>  sBuffer);

EXTERN PUBLIC void  vDBG_SendBufferContent();

EXTERN PUBLIC tBOOL bDBG_HasVerboseModeActived();

EXTERN PUBLIC void  vDBG_ActiveVerboseMode(tBOOL state);

EXTERN PUBLIC void  vDBG_LogError(MODULE_CODE module_code, TYPE_CODE type_code, string msg);

EXTERN PUBLIC void	vDBG_LogState(MODULE_CODE module_code, TYPE_CODE type_code, string msg);

EXTERN PUBLIC string<5>  sDBG_getCompErrorCode();

EXTERN PUBLIC void  vDBG_clearCompErrorCode();

PRIVATE void  vDBG_SendLogFile();

PRIVATE void  vDBG_WriteInErrorFile(string<1> code, string msg);

PRIVATE void  vDBG_ClearErrorFile();

PRIVATE void  vDBG_updateCompError(MODULE_CODE module_code, TYPE_CODE type_code);

#endif // __DEBUG_H__


