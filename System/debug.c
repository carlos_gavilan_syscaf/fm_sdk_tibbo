// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include "debug.h"

#define DBG_MODE 1

#ifdef DBG_MODE
	tBOOL bDBG_VerboseMode = eTRUE;
#else
	tBOOL bDBG_VerboseMode = eTRUE; //eFALSE; //To Do, agregar la opcion de guardar en la eeprom el estado de verbose
#endif

CompErrorCode compatErrorCode;

void  vDBG_Initialize(){	
	sock.num = DBG_CTRL_SOCK;			//This is the ctrl connection
	sock.allowedinterfaces	= "NET";
	sock.txbuffrq(1);
	sock.rxbuffrq(1);
	sock.protocol = PL_SOCK_PROTOCOL_TCP;
	sock.inconmode = PL_SOCK_INCONMODE_ANY_IP_ANY_PORT;
	sock.reconmode = PL_SOCK_RECONMODE_3; //original PL_SOCK_RECONMODE_1
	sock.connectiontout = 0;
	sock.targetip = IP_IVU;
	sock.localportlist = DBG_CTRL_PORT;
	sock.targetport = DBG_CTRL_PORT_NUM;
	
	sys.buffalloc();
	
	while(sock.statesimple == PL_SSTS_EST) {
		sock.notifysent(8);
	}
	
	//clean the rtc data. 
	rtc.setdata(0, 0, 0);
	
	
	//read de value of verbose mode flag
	string strVerboseMode = stor.getdata(0,2);
	if(strVerboseMode == ACTIVE_VERBOSE_MODE)
		bDBG_VerboseMode = eTRUE;
	else
		bDBG_VerboseMode = eFALSE;
	
}

void  vDBG_ProcessCode( string<2>  sBuffer){
	if(sBuffer == ACTIVE_VERBOSE_MODE){
		bDBG_VerboseMode = eTRUE;
		stor.setdata(sBuffer,0);
		vDBG_LogState(MIMO, Info, "Verbose Mode: Actived");
	}
	else if(sBuffer == DEACTIVE_VERBOSE_MODE){
		vDBG_LogState(MIMO, Info, "Verbose Mode: Deactived");
		stor.setdata(sBuffer,0);
		bDBG_VerboseMode = eFALSE;
	}
	else if (sBuffer == READ_LOGFILE){
		vDBG_SendLogFile();
	}
	else if (sBuffer == CLEAR_LOGFILE){
		vDBG_ClearErrorFile();
		vDBG_LogState(MIMO, Info, "Error Log File cleared!");
	}
	else if (sBuffer == READ_DATA_FRAME) {
		vDBG_LogState(DataFrameBuilderFsm, Info, vDataFrameBuilder_getDataFrame());
	}
	else if (sBuffer == FORMAT_MIMO){
		vDBG_LogState(MIMO, Info, "Formating Disk ...");
		if(fd.formatj((fd.availableflashspace/2),2,100)!=PL_FD_STATUS_OK) {
			vDBG_LogError(System_FlashDisk, ErrFatal,"An Error ocurred while attempt format");
		}
		vDBG_LogState(MIMO, Info, "Mounting Disk ...");
		if(fd.mount()!=PL_FD_STATUS_OK) {
			vDBG_LogError(System_FlashDisk, ErrUnableToOpenConnection,"An Error ocurred while attempt mount the Flash Disk. Possibly unformatted disck");
		}
	}
	else if (sBuffer == REBOOT_MIMO){
		vDBG_LogState(MIMO, Info, "Mimo rebooting now!");
		sys.reboot();
	}
}

void  vDBG_LogError(MODULE_CODE module_code, TYPE_CODE type_code, string msg){
	
	U8 err_code = ((module_code << 4) & 0xF0) | (type_code & 0x0F);
	
	msg+="\n";
	
	// write message in internal error file
	vDBG_WriteInErrorFile(chr(err_code), " "+msg);
	
	//update structure for compatibility with old mimo source code
	vDBG_updateCompError(module_code, type_code);
	
	if (bDBG_VerboseMode == eFALSE)	return;

	#ifdef DBG_MODE
		sys.debugprint(msg);

	#else
		sock.num = DBG_CTRL_SOCK;
		sock.setdata(chr(err_code)+msg); //Send next packet
		sock.send();
	#endif
	
}

void  vDBG_LogState(MODULE_CODE module_code, TYPE_CODE type_code, string msg){
	
	if (bDBG_VerboseMode == eFALSE)	return;
	
	U8  err_code  = ((module_code << 4) & 0xF0) | (type_code & 0x0F);

	msg+="\n";

	#ifdef DBG_MODE
		sys.debugprint(msg);

	#else
		sock.num = DBG_CTRL_SOCK;
		sock.setdata(chr(err_code)+msg); //Send next packet
		sock.send();
	#endif

}

void  vDBG_SendLogFile(){
	int filesize;
	U8  packetSize;
	
	fd.filenum = 2;
	sock.num = DBG_CTRL_SOCK;
	fd.open(SYS_LOG_FILENAME);
	
	filesize =  fd.filesize;
	
	if (filesize == 0){
		sock.setdata(chr(0xC9)+" Error Log: file empty!\n"); 
		sock.send();
		return;
	}
	
	while (filesize > 0){
		
		if(filesize < 255)
			packetSize = filesize;
		else 
			packetSize = 255;
			
		sock.setdata(fd.getdata(packetSize)); //Send next packet
		sock.send();
		
		filesize-=packetSize;	
	}
	
}


void  vDBG_ActiveVerboseMode(tBOOL state){
	bDBG_VerboseMode = state;
}

tBOOL bDBG_HasVerboseModeActived(){
	return bDBG_VerboseMode;
}


string<5>  sDBG_getCompErrorCode(){
	return	chr(compatErrorCode.OsipClientError) +
			chr(compatErrorCode.FtpClientError)  +
			chr(compatErrorCode.StackFMError)    +
			chr(compatErrorCode.DataFrameBuilderError)+
			chr(compatErrorCode.SystemError);
}

void  vDBG_clearCompErrorCode(){
	compatErrorCode.OsipClientError			= 0;
	compatErrorCode.FtpClientError 			= 0;
	compatErrorCode.StackFMError 			= 0;
	compatErrorCode.DataFrameBuilderError 	= 0;
	compatErrorCode.SystemError 			= 0;
}

void  vDBG_WriteInErrorFile(string<1> code, string msg){
	fd.filenum = 2;
	string new_line = code + getDateFromTibbo() + ": "+msg;
	
	fd.open(SYS_LOG_FILENAME);
	U8 len_new_line = len(new_line) + 1;
	if (fd.filesize + len_new_line > 1024) {
		fd.setpointer(1);
		fd.transactionstart();
		fd.setfilesize(len_new_line);
		fd.transactioncommit();
	}
		
	else 
		fd.setpointer(fd.filesize+1);
		
	fd.transactionstart();
    fd.setdata(new_line);
    fd.transactioncommit();
	
	fd.close();
}

void vDBG_ClearErrorFile(){
	fd.filenum = 2;
	fd.open(SYS_LOG_FILENAME);
	fd.setpointer(1);
	fd.transactionstart();
	fd.setfilesize(1);
	fd.transactioncommit();
	fd.close();
}

void  vDBG_updateCompError(MODULE_CODE module_code, TYPE_CODE error_code){
	switch(module_code){
		
		case OsipClient:
			compatErrorCode.OsipClientError = (U8)error_code;
			break;
		
		case FtpClient:
			compatErrorCode.FtpClientError = (U8)error_code;
			break;
		
		case StackFM_FM:
		case StackFM_AppLayer:
		case StackFM_Transport:
		case StackFM_HDLC:
		case StackFM_PHY:
			compatErrorCode.StackFMError = (U8)error_code;
			break;
		
		case MemoListener:
		case KimaxListener:
		case DataFrameBuilderFsm:
			compatErrorCode.DataFrameBuilderError = (U8)error_code;
			break;

		default:
			compatErrorCode.SystemError = (U8)error_code;
			break;
	}
}
