#ifndef __SYSTEM_CONFIG_H__
#define __SYSTEM_CONFIG_H__

// *****************************************************************************
// *****************************************************************************
// Section: System Service Configuration
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Common System Service Configuration Options
*/

#include "cifms.h"

#define SYS_VERSION_STR           "1.06"
#define SYS_VERSION               10600

   
/*** Interrupt System Service Configuration ***/
#define SYS_INT                     true

#define SYS_GW_IP					"172.20.201.18"

#define SYS_IP						"172.20.201.17"
#define SYS_IP_CSV					"172,20,201,17"
#define SYS_MASK					"255.255.255.0"

#define IP_IVU              		"172.20.201.18"

#define IVU_USERNAME 			"memo"
#define IVU_PWD		 			"memo2ivu"

#define SYS_TEMP_FILENAME 			"GPRSService_2223_"
#define SYS_LOG_FILENAME			"ErrorLog"
#define SER_NUM_FM		0
#define  RX_FM			PL_IO_NUM_0_RX0_INT0
#define  TX_FM			PL_IO_NUM_1_TX0_INT1
#define CTS_FM			PL_IO_NUM_14
#define RTS_FM			PL_IO_NUM_15

#define SER_NUM_KIMAX	2
#define RX_KIMAX		PL_IO_NUM_4_RX2_INT4
#define TX_KIMAX		PL_IO_NUM_7_TX3_INT7

#define SER_NUM_MEMO	1
#define RX_MEMO			PL_IO_NUM_2_RX1_INT2
#define TX_MEMO			PL_IO_NUM_3_TX1_INT3


#define FTP_CTRL_SOCK 		0
#define FTP_DATA_SOCK 		1
#define OSIP_SOCK 			2
#define DBG_CTRL_SOCK 		3


EXTERN PUBLIC void vPLL_Initialize();
EXTERN PUBLIC void vNet_Initialize();
EXTERN PUBLIC void vFlashDisk_Initialize();
EXTERN PUBLIC void vCreate_Files();

#endif // _SYSTEM_CONFIG_H


