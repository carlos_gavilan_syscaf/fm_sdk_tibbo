#ifndef __CIFMS_H__
#define __CIFMS_H__
/* ==========================================================================

Copyright (C) 2000 Control Instruments
(Fleet Management Services - CIFMS division)

COMPONENT:        Global Definitions
AUTHOR:           P.J. Conradie
DESCRIPTION:      This header file defines global types used in all projects.

VERSION:          $Revision: 2 $
DATED:            $Date: 2/02/01 16:01 $
LAST MODIFIED BY: $Author: Pieterc $

========================================================================== */
/* ___STANDARD INCLUDES___________________________________________________ */
/* ___PROJECT INCLUDES____________________________________________________ */
/* ___DEFINITIONS ________________________________________________________ */
/* ___TYPE DEFINITIONS____________________________________________________ */
/* Type definitions */

typedef unsigned char   U8;
typedef unsigned short  U16;
typedef unsigned long   U32;

		  

/* NULL definition */
#ifndef NULL
  #define NULL 0
#endif

/* Boolean definition */
typedef bool			  tBOOL;
#define eTRUE 		  true
#define eFALSE		  false

#define FAR

/* ___GLOBAL VARIABLES____________________________________________________ */
/* ___PUBLIC FUNCTION PROTOTYPES__________________________________________ */
/* ___MACROS______________________________________________________________ */
/* Scope definition */
#define PRIVATE     static
#define PUBLIC

#define EXTERN      extern

#endif
