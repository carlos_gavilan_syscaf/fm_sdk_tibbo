// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include "system_config.h"
#include "cifms.h"
#include "debug.h"

#include "Net\ftp_client.h"
#include "Net\osip_client.h"

#include "StackFM\serial.h"

#include "..\dfb_fsm.h"


//===================================================================
void vPLL_Initialize(){
	if (sys.currentpll== 0) {
		sys.newpll(1);
        sys.reboot();
	}
	vDBG_LogState(System_RTC, Info, "PLL Init Ok");
}
void vNet_Initialize(){
	// Configura de la direccion IP del TIBBO
	net.gatewayip = SYS_GW_IP; 
	net.ip 		  = SYS_IP; 				// Configura la direccion IP del TIBBO por medio del objeto net
	net.netmask	  = SYS_MASK;				// Configura la mascara de red de la direccion asignada por medio del objeto net
	
}
void vFlashDisk_Initialize(){
	//montar el disco

	if(fd.mount()!=PL_FD_STATUS_OK) {
		vDBG_LogError(System_FlashDisk, ErrUnableToOpenConnection,"An Error ocurred while attempt mount the Flash Disk. Possibly unformatted disck");
		//formatear el disco
		if(fd.formatj((fd.availableflashspace/2),2,100)!=PL_FD_STATUS_OK) {
			vDBG_LogError(System_FlashDisk, ErrFatal,"An Error ocurred while attempt format");
		}
		//reiniciamos el systema
		vDBG_LogState(System_FlashDisk, Info, "Disk Formatted, system reboot. I'll be Back!");
		sys.reboot();
	}
	vDBG_LogState(System_FlashDisk, Info, "Init Ok");
}

void vCreate_Files()
{
	fd.create(SYS_TEMP_FILENAME);
	if (fd.laststatus !=PL_FD_STATUS_OK && fd.laststatus !=PL_FD_STATUS_DUPLICATE_NAME){
		vDBG_LogError(System_FlashDisk, ErrUnableToWrite, "An Error ocurred while attempt to create a temporal file, code: "+str(fd.laststatus)+ " \n");
		return;
	}
	/*fd.create(SYS_TACHO_FILENAME);
	if (fd.laststatus !=PL_FD_STATUS_OK && fd.laststatus !=PL_FD_STATUS_DUPLICATE_NAME){
		vDBG_LogError(System_FlashDisk, ErrUnableToWrite, "An Error ocurred while attempt to create a tacho file, code: "+str(fd.laststatus)+ " \n");
		return;
	}*/
	fd.create(SYS_LOG_FILENAME);
	if (fd.laststatus !=PL_FD_STATUS_OK && fd.laststatus !=PL_FD_STATUS_DUPLICATE_NAME){
		vDBG_LogError(System_FlashDisk, ErrUnableToWrite, "An Error ocurred while attempt to create a log file, code: "+str(fd.laststatus)+ " \n");
		return;
	}
	vDBG_LogState(System_FlashDisk, Info, "Temporal files created");
}


void on_ser_data_arrival(){
	// Check for received data
	switch(ser.num){
		case SER_NUM_FM:
			vHDLC_OnRxData(ser.getdata(1));		
			break;
		
		case SER_NUM_KIMAX:
			vKimaxListener_ProcessCode(ser.getdata(1));
			break;
		
		case SER_NUM_MEMO:
			vMemoListener_ProcessCode(ser.getdata(1));
			break;
	}
	
}


void on_sock_data_arrival(){
	string sBuffer;
	switch(sock.num){
		case OSIP_SOCK:
			sBuffer = sock.getdata(255);
			vOSIP_ProcessCode(sBuffer);
			break;
		case FTP_CTRL_SOCK:
			sBuffer = sock.getdata(255);
			vFTP_ProcessCode(sBuffer);
			break;
		case DBG_CTRL_SOCK:
			vDBG_ProcessCode(sock.getdata(2));
			break;
		//case FTP_DATA_SOCK:
		//break;
	}
}

void on_sock_data_sent()
{
	switch(sock.num){
		case FTP_DATA_SOCK:
			vFTP_SendPacket();
			break;
		/*case DBG_DATA_SOCK:
			vDBG_SendBufferContent();
			break;*/
	}
}
