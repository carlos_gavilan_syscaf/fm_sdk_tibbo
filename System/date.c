// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include "date.h"


U16 DaysElapsedAtMonth[MONTH_PER_YEAR+1]= { 0, 
										   31,  59,  90, 
										  120, 151, 181, 
										  212, 243, 273, 
										  304, 334, 365};

//=====================================================================================================================
U8  bin2bcd(U8 binary);

U32 getEpochTimeFromTibbo(int offset){
	U16 day_elapsed=0, min_elapsed=0, sec_elapsed=0;
	rtc.getdata(day_elapsed, min_elapsed, sec_elapsed);
	return 	day_elapsed*SEC_PER_DAY + 
			min_elapsed*SEC_PER_MIN +
			sec_elapsed 		    + 
			EPOCH_1_JAN_2000		- //946684800
			offset*SEC_PER_HOUR;
}


void setTibboTimeFromEpoch(U32 epoch, int offset){
	U32 num_mm;
	U16 tibbo_dd, tibbo_mm, tibbo_ss;
	
	epoch -=  EPOCH_1_JAN_2000;
	epoch +=  offset*SEC_PER_HOUR;
	
	num_mm = epoch / SEC_PER_MIN;
	
	tibbo_dd = num_mm / MIN_PER_DAY;
	tibbo_mm = num_mm % MIN_PER_DAY;
	tibbo_ss = epoch % SEC_PER_MIN;
	
	rtc.setdata(tibbo_dd , tibbo_mm, tibbo_ss );
}

string getDateFromTibbo(){
	U16 day_elapsed=0, min_elapsed=0, sec_elapsed=0;
	rtc.getdata(day_elapsed, min_elapsed, sec_elapsed);
	string<2>   aux;
	string result = "";
	int a;
	//add year
	aux = str(year (day_elapsed));
	if(len(aux)==1) aux ="0"+aux;
	result += aux;

	//add month
	aux = str(month(day_elapsed));
	if(len(aux)==1) aux ="0"+aux;
	result += aux;
	//add day
	aux = str(date (day_elapsed));
	if(len(aux)==1) aux ="0"+aux;
	result += aux;
	//add separtor
	result += "-";
	//add hour
	aux = str(hours(min_elapsed));
	if(len(aux)==1) aux ="0"+aux;
	result += aux;

	//add min
	aux = str(minutes(min_elapsed));
	if(len(aux)==1) aux ="0"+aux;
	result += aux;
	//add sec
	aux = str(sec_elapsed);
	if(len(aux)==1) aux ="0"+aux;
	result += aux;
	
	return result;
}

void getBcdTimeFromEpoch(U32 epochtime, int offset, U32* bcd_date, U32* bcd_time){
	U32	num_mm, num_dd;
	U8  curr_ss, curr_mm, curr_hh, curr_dd, curr_MM, curr_yy, bis_corr, i;
	
	num_mm  =  epochtime / SEC_PER_MIN;
	num_dd  = (epochtime % SEC_PER_YEAR)/ (U32) SEC_PER_DAY; 
	
	curr_ss = (U8)(epochtime  % SEC_PER_MIN);
	curr_mm = (U8)(num_mm     % MIN_PER_HOUR);
	curr_hh = (U8)(((num_mm   / MIN_PER_HOUR) + offset) % HOUR_PER_DAY);
	curr_yy = (U8)((epochtime / SEC_PER_YEAR) - 30); //1970 - 2000;

	bis_corr = (curr_yy  % 4 ==0)?1:0;

	for(i=0;i<MONTH_PER_YEAR;i++){
		if((num_dd-bis_corr) < DaysElapsedAtMonth[i]) break;
	}
	curr_MM = i;
	curr_dd = num_dd - DaysElapsedAtMonth[i-1]+1;
	//curr_yy  curr_MM  curr_dd  curr_hh  curr_mm curr_ss
	// [ 00yy MMdd ]
	*bcd_date  = 0x00000000;
	*bcd_date |= bin2bcd(curr_yy);*bcd_date <<= 8;
	*bcd_date |= bin2bcd(curr_MM);*bcd_date <<= 8;
	*bcd_date |= bin2bcd(curr_dd);*bcd_date;
	

	// [ 00hh mmss ]
	*bcd_time  = 0x00000000;
	*bcd_time |= bin2bcd(curr_hh);*bcd_time <<= 8;
	*bcd_time |= bin2bcd(curr_mm);*bcd_time <<= 8;
	*bcd_time |= bin2bcd(curr_ss);*bcd_time;

}


U8  bin2bcd(U8 binary){
	U8 Tens = 0, Ones = 0, less_bit;
	U8 bcd;
	signed char i;
	
	if (binary > 99 ) return 0;
	
	for(i = 7; i>=0; i--){
		
		if(Tens>=5) Tens+=3;
		if(Ones>=5) Ones+=3;

		Tens = (Tens<<1)|((Ones  >>3)&0x01)&0x0F;
		Ones = ((Ones<<1)|(((i>0)?(binary>>i):binary)&0x01))&0x0F;
	}
	
	bcd = 	(Ones | Tens<<4 );
			
	return bcd;
	
}
