#ifndef __DATE_H__
#define __DATE_H__


#include "cifms.h"

#define GMT_BOG 			-5

#define EPOCH_1_JAN_2000	946684800

#define SEC_PER_MIN			60
#define MIN_PER_HOUR		60
#define HOUR_PER_DAY		24

#define DAY_PER_YEAR		365
#define MONTH_PER_YEAR		12

#define SEC_PER_YEAR		31556926
#define SEC_PER_DAY			86400

#define SEC_PER_HOUR		3600
#define MIN_PER_DAY			1440



string 	getDateFromTibbo();

U32		getEpochTimeFromTibbo(int offset);

void 	getBcdTimeFromEpoch(U32 epochtime, int offset, U32* bcd_date, U32* bcd_time);

void 	setTibboTimeFromEpoch(U32 epoch, int offset);


//U8  bin2bcd(U8 binary);

#endif // __DATE_H__


