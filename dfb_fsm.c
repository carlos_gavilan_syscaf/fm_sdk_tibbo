#include "dfb_fsm.h"
#include "mimo_fsm.h"
/* ___LOCAL DEFINITIONS___________________________________________________ */

DataFrameBuilder_data dataFrameBuilder;

U8* pU8IvuAlarm;

tTMR_Handle	hDFB_WaitingDataTimer;
tTMR_Handle	hDFB_ResetCommTimer;

string kimax_buffer;
string kimax_data;

string memo_buffer;
string memo_data;

tBOOL kimaxNoDataErrorFlag = eFALSE;
tBOOL  memoNoDataErrorFlag = eFALSE;

string scriptDataFrame;

/* ___PUBLIC FUNCTIONS___________________________________________________ */
void vDataFrameBuilder_Initialize(){
	vKimaxListener_Initialize();
	vKimax_SleepListener();
	
	vMemoListener_Initialize();
	vMemo_SleepListener();
	
	
	//hDFB_RequestTimer = hTMR_StartTimer(DFB_REQUEST_PERIOD);
	dataFrameBuilder.state = DFB_STATE_PAUSE;//DFB_STATE_INIT;
}

void vKimaxListener_Initialize(){
		
	ser.num = SER_NUM_KIMAX; 								
	ser.mode			= PL_SER_MODE_UART;
	ser.parity			= PL_SER_PR_NONE;
	ser.bits			= PL_SER_BB_8;
	ser.flowcontrol		= DISABLED;
	ser.baudrate		= ser.div9600;			
	ser.interface		= PL_SER_SI_FULLDUPLEX;
	ser.interchardelay	= 0;
	ser.esctype			= PL_SER_ET_DISABLED;
	
	io.num				= RX_KIMAX;
	io.enabled			= NO;
	
	kimax_data 	= "";
		
	io.num				= TX_KIMAX;
	io.enabled			= YES;
	io.state			= HIGH;
	
	ser.txbuffrq(1);
	ser.rxbuffrq(1);
	sys.buffalloc();
	
	ser.enabled = YES;
	
	
}

void vMemoListener_Initialize(){

	ser.num = SER_NUM_MEMO;								//Puerto serial 1: recepcion uC y transmision FM SCRIPT	
	ser.mode			= PL_SER_MODE_UART;		//Se escoje el modo UART
	ser.parity			= PL_SER_PR_NONE;		//Si tiene o no Paridad
	ser.bits			= PL_SER_BB_8;			//Cuantos bits componen un caracter
	ser.flowcontrol		= DISABLED;				//control transmicion y recepcion de datos
	ser.baudrate		= ser.div9600/2;		//9600*ser.div9600/19200 --- 19200bps velocidad de la comunicacion
	ser.interface		= PL_SER_SI_FULLDUPLEX;	//comunicacion bi direccional
	ser.interchardelay	= 0;					//retardo en recivir el siguiente caracter
	ser.esctype			= PL_SER_ET_DISABLED;	//identifica caracteres especiales
	
	io.num				= RX_MEMO;					//Se identifica el pin a configurar
	io.enabled			= NO;					//Se configura como IN
	
	io.num				= TX_MEMO;					//Se identifica el pin a configurar
	io.enabled			= YES;					//Se configura como OUT
	
	
	memo_data 	= "";
	
	ser.txbuffrq(1);
	ser.rxbuffrq(1);
	sys.buffalloc();
	
	ser.enabled = YES;
	
}

void vKimaxFrame_Reset(){
	ser.num 		= SER_NUM_KIMAX;
	ser.rxclear();
	//ser.enabled 	= NO;							//Reiniciar puerto serial.
	//ser.enabled 	= YES;
	io.lineset(TX_KIMAX,HIGH);							//Status LED Rojo controlado por uC
	kimax_buffer = "";
}

void vMemoFrame_Reset(){
	ser.num 		= SER_NUM_MEMO;
	ser.rxclear();
	//ser.enabled 	= NO;							//Reiniciar puerto serial.
	//ser.enabled 	= YES;
	memo_buffer = "";
}

void vKimax_SleepListener()
{
	ser.num = SER_NUM_KIMAX;
	ser.enabled			= NO;
}

void vMemo_SleepListener()
{
	ser.num = SER_NUM_MEMO;
	ser.enabled			= NO;
}

void vKimax_WakingListener()
{
	ser.num = SER_NUM_KIMAX;
	ser.enabled			= YES;					//Habilito el puerto con la configuracion	

}

void vMemo_WakingListener()
{
	ser.num = SER_NUM_MEMO;
	ser.txclear ();	
	ser.enabled			= YES;
}


void vKimaxListener_ProcessCode(string<KIMAX_DATA_LENGTH> sBuffer){

	if (dataFrameBuilder.state != DFB_WAITING_DATA) {
		ser.rxclear();
		return;
	}
		
	kimax_buffer += sBuffer;
	U8 U8LenBuffer  = len(kimax_buffer);
	string<6> sHeader = left(kimax_buffer,6);

	if ( sHeader == KIMAX_DATA_HEADER){
		if (U8LenBuffer == KIMAX_DATA_LENGTH){
			//Tomando los valores del Peso relevantes
			kimax_data 		= mid(kimax_buffer,7,16);
			kimax_buffer 		= "";
			dataFrameBuilder.kimaxDataReceivedFlag = eTRUE;
			io.lineset(TX_KIMAX,LOW); //Status LED Rojo controlado por uC OFF	
		} 
		else if (U8LenBuffer < KIMAX_DATA_LENGTH) return;
		else if (U8LenBuffer > KIMAX_DATA_LENGTH) vKimaxFrame_Reset();	
	} else vKimaxFrame_Reset();
	
}


void vMemoListener_ProcessCode(string<MEMO_DATA_LENGTH> sBuffer){
	
	if (dataFrameBuilder.state != DFB_WAITING_DATA) {
		ser.rxclear();
		return;
	}
	
	memo_buffer += sBuffer;
	U8 U8LenBuffer = len(memo_buffer);
	string<1> sHeader = left(memo_buffer,1);

	if (sHeader == MEMO_DATA_HEADER) { //Identifica el Inicio de trama
		if (U8LenBuffer == MEMO_DATA_LENGTH){ //Si la trama es del tamano esperado
			//Tomando los valores de la trama que contienen la info relevante
			memo_data 			= mid(memo_buffer,2,20);
			memo_buffer 		= "";
			
			dataFrameBuilder.memoDataReceivedFlag = eTRUE;
		} 						
		else if (U8LenBuffer < MEMO_DATA_LENGTH) return;
		else if (U8LenBuffer > MEMO_DATA_LENGTH) vMemoFrame_Reset();	//Trama con uno mas bytes de lo esperado
	} else vMemoFrame_Reset();			//Trama sin correspondencia de la cabecera
	
}

void vDataFrameBuilder_Tasks(){
	U8* pU8memo_data;
	
	switch ( dataFrameBuilder.state )
    {
        // Application's initial state. 
        case DFB_STATE_INIT:
        {    
			bMimo_Pause();
			vFM_SleepListeners();
			
			dataFrameBuilder. memoDataReceivedFlag = eFALSE;
			dataFrameBuilder.kimaxDataReceivedFlag = eFALSE;
			
			//vOSIP_ClearData();
			
			//vDBG_LogState(DataFrameBuilderFsm, Info, "Waking listeners");
			
			dataFrameBuilder.state = DFB_WAKING_LISTENERS; 
            break;
        }
		
		case DFB_WAKING_LISTENERS:
		{
			vMemo_WakingListener();
			vKimax_WakingListener();
			//vDBG_LogState(DataFrameBuilderFsm, Info, "Waiting data...");
			hDFB_WaitingDataTimer = hTMR_StartTimer(500);
			dataFrameBuilder.state = DFB_WAITING_DATA;
			break;
		}
		
		
		case DFB_WAITING_DATA:
        {     
			if(dataFrameBuilder.memoDataReceivedFlag  == eTRUE ||
			   dataFrameBuilder.kimaxDataReceivedFlag == eTRUE){
					pU8memo_data = memo_data;
					pU8IvuAlarm  = pU8memo_data +12;
					dataFrameBuilder.state = DFB_SLEEP_LISTENERS;
					
			} else if(bTMR_HasTimerExpired(hDFB_WaitingDataTimer) == eTRUE){
				vDBG_LogError(DataFrameBuilderFsm, ErrTimeOut, "while waiting data");
				dataFrameBuilder.state = DFB_SLEEP_LISTENERS;
				
			}
					
            break;
        }
		
		case DFB_SLEEP_LISTENERS:
		{
			vKimax_SleepListener();
			vMemo_SleepListener();
			//vDBG_LogState(DataFrameBuilderFsm, Info, "Sleep listeners...");
			dataFrameBuilder.state = DFB_EVAL_ENGINE_STATE; //DFB_STATE_EXIT;
			break;
		}
		
		case DFB_STATE_PAUSE:
		{
			break;
		}
		
		case DFB_STATE_EXIT:
		{
					
			vMimo_Continue();
			vFM_WakingListeners();
			
			
			if(bTMR_HasTimerExpired(hDFB_ResetCommTimer) == eTRUE){
				vOSIP_ResetComm();
				
				vDBG_LogState(DataFrameBuilderFsm, Info, "Reset OSIP comm over UDP");
				hDFB_ResetCommTimer = hTMR_StartTimer(DFB_RESET_PERIOD);
			}
			dataFrameBuilder.state = DFB_STATE_INIT;
			
			break;
		}
		/*case DFB_STATE_IDLE:
        {     
				
			if (bTMR_HasTimerExpired(hDFB_RequestTimer) == eTRUE)
            {

				hDFB_RequestTimer = hTMR_StartTimer(DFB_REQUEST_PERIOD);
				dataFrameBuilder.state = DFB_INIT;
            }
            break;
        }*/
		
		case DFB_EVAL_ENGINE_STATE:
        {     
			//vDBG_LogState(DataFrameBuilderFsm, Info, "Proccesing Data...");
			//Configuracion Byte ALARM 
			U8 U8FallaMotor				 = *(pU8memo_data + 5);
			//Falla de motor
			if (U8FallaMotor == 0xE0) {
				*pU8IvuAlarm |= 0x01;	//activa el bit que indica esa falla en el byte de alarmas
			} else {
				*pU8IvuAlarm &= 0x0E;	//si no se produce esa falla clarea el bit de la alarma, se ponen el enmascaramienti directamente aca
			}
				
			dataFrameBuilder.state = DFB_EVAL_DOORS_STATE;
            break;
        }
		case DFB_EVAL_DOORS_STATE:
        {   
			U16 result  = 	*(pU8memo_data + 1) + //PuertasServicioDelanteras
							*(pU8memo_data + 2) + //PuertasServicioTraseras
							*(pU8memo_data + 3) + //PuertasEmergencia1
							*(pU8memo_data + 4);  //PuertasEmergencia2
			//Puerta abierta
			//detecta si las puertas estan abiertas a cierta velocidad
			if ((result >= 0xE0) && (U8Mimo_getVelocity() > 5)) //
				*pU8IvuAlarm	|= 0x02;	// activa el bit que indica esa falla en el byte de alarmas
			else 
				*pU8IvuAlarm    &= 0x0D;	// si no se produce esa falla clarea el bit de la alarma,se ponen el enmascaramienti directamente aca
			
			dataFrameBuilder.state = DFB_EVAL_AC_STATE;
            break;
        }
		case DFB_EVAL_AC_STATE:
        {   
			U8 U8AC	 = *(pU8memo_data + 13);
			//Puerta aire acondicionado
			if (U8AC == 0x0F) 			//detecta si apagaron el aire acondicionado
				*pU8IvuAlarm    |= 0x04;	//activa el bit que indica esa falla en el byte de alarmas
			else 
				*pU8IvuAlarm    &= 0x0B;	//si no se produce esa falla clarea el bit de la alarma, se ponen el enmascaramienti directamente aca
			
			dataFrameBuilder.state = DFB_ADD_MEMO_DATA;
            break;
        }
		case DFB_ADD_MEMO_DATA:
        {    
			//vDBG_LogState(DataFrameBuilderFsm, Info, "Building data frame...");
			scriptDataFrame = DATA_FRAME_HEADER; //// "u"
			
			if(dataFrameBuilder.memoDataReceivedFlag == eFALSE){
				//trama por defecto
				memo_data = "";
				for(U8 i=0;i<MEMO_BUFFER_LENGTH;i++) memo_data += chr(0x00);
				//revisa si ya reporto el error, para no reportarlo mas
				if(memoNoDataErrorFlag){
					vDBG_LogError(DataFrameBuilderFsm, ErrUnableToRead, "There's no MEMO data. Sending default frame");
					memoNoDataErrorFlag = eTRUE;
				} 
			}
			memo_data = right(memo_data, MEMO_BUFFER_LENGTH); //elimina el primer elemento
			vAddToDataFrame(memo_data);
			dataFrameBuilder.state = DFB_ADD_KIMAX_DATA;
            break;
        }
		case DFB_ADD_KIMAX_DATA:
        {     
			if(dataFrameBuilder.kimaxDataReceivedFlag == eFALSE){
				kimax_data = "";
				for(U8 i=0;i<KIMAX_BUFFER_LENGTH;i++) kimax_data += chr(0xFF);
				//revisa si ya reporto el error, para no reportarlo mas
				if(kimaxNoDataErrorFlag){
					vDBG_LogError(DataFrameBuilderFsm, ErrUnableToRead, "There's no Kimax data. Sending default frame");
					kimaxNoDataErrorFlag = eTRUE;
				} 
				
			}
				
				
			vAddToDataFrame(kimax_data);
			dataFrameBuilder.state = DFB_ADD_OSIP_DATA_1;
            break;
        }
		case DFB_ADD_OSIP_DATA_1:
        {   

			vAddToDataFrame(sOSIP_GetVehicleId());
			vAddToDataFrame(sOSIP_GetDriverId());
			vAddToDataFrame(sOSIP_GetRouteId());
			
			dataFrameBuilder.state = DFB_ADD_OSIP_DATA_2;				
			
            break;
        }
		case DFB_ADD_OSIP_DATA_2:
        {     

			vAddToDataFrame(sOSIP_GetEventData());
			vAddToDataFrame(sOSIP_GetBusStopId());
			
			dataFrameBuilder.state = DFB_ADD_ERROR;				
			
            break;
        }
		
		case DFB_ADD_ERROR:
        {     			
			vAddToDataFrame(sDBG_getCompErrorCode());
		
			scriptDataFrame += DATA_FRAME_END; // "ux"
			dataFrameBuilder.state = DFB_SENDING_DATA;
			
			vDBG_clearCompErrorCode();
			
            break;
        }
		case DFB_SENDING_DATA:
        {   
						
			vMemo_WakingListener();
			//identifica el puerto por el que se va a enviar
			ser.setdata(scriptDataFrame);		//envia la trama al buffer de envio
			ser.send();
			
			hDFB_WaitingDataTimer = hTMR_StartTimer(4);
			dataFrameBuilder.state = DFB_WAIT_UNTIL_FRAME_SENT;
            break;
        }
		case DFB_WAIT_UNTIL_FRAME_SENT:
		{
			if(bTMR_HasTimerExpired(hDFB_WaitingDataTimer) == eTRUE){
				vMemo_SleepListener();
				dataFrameBuilder.state = DFB_STATE_EXIT;
				
			}
		}
		
		
	}
}



tBOOL bDataFrameBuilder_Pause()
{   
	//
	dataFrameBuilder.state = DFB_STATE_PAUSE;
	return eTRUE;
	//vDBG_LogState(DataFrameBuilderFsm, Info, "DataBuilderFrame Paused!");
}
void vDataFrameBuilder_Continue()
{
	//vDBG_LogState(DataFrameBuilderFsm, Info, "DataBuilderFrame Continue!");
	dataFrameBuilder.state = DFB_STATE_INIT;
}
		


void vAddToDataFrame(string data) {
	U8* pU8Source = data;
	U8 U8Lenght = len(data);
	while(U8Lenght-- != 0){
		scriptDataFrame += chr(*pU8Source++);
		if (*pU8Source == 0x75){
			scriptDataFrame += chr(0x75); //// "uu"
		}
	}
}
	

U8 U8DataFrameBuilder_GetIvuAlarm(){
	U8 aux = *pU8IvuAlarm;
	return *pU8IvuAlarm;
}


string vDataFrameBuilder_getDataFrame()
{
	return scriptDataFrame;
}