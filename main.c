
#include "mimo_fsm.h"
#include "dfb_fsm.h"

#include "Net\ftp_client.h"
#include "Net\osip_client.h"

#include "System\system_config.h"
#include "System\date.h"
#include "System\debug.h"

#include "StackFM\timer.h"


void on_sys_timer()
{	
	vTMR_Tick();
	
	vFTP_Tasks();

	vMimo_Tasks();
	
	vDataFrameBuilder_Tasks();

}

void on_sys_init(){
	
	vNet_Initialize();
	
	vDBG_Initialize();
	
	vPLL_Initialize();

	vFlashDisk_Initialize();
	
	vCreate_Files();
	
	//Init the Finite State Machines

	vDataFrameBuilder_Initialize();
	
	vMimo_Initialize();
	
	vOSIP_Initialize();
	
	vFTP_Initialize();
		
	sys.onsystimerperiod	= 1; //Cada 10 ms se haran las "interrupciones"

}

