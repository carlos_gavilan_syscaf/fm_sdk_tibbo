#ifndef __DFB_H__
#define __DFB_H__

#include "System\cifms.h"
#include "System\system_config.h"

#include "StackFM\timer.h"
#include "Net\osip_client.h"



#define DFB_REQUEST_PERIOD 	1000

#define DFB_RESET_PERIOD 	360000

#define KIMAX_DATA_HEADER "UUUUww"
#define KIMAX_DATA_LENGTH  		49 
#define KIMAX_BUFFER_LENGTH  	16 

#define MEMO_DATA_HEADER	"u"
#define MEMO_DATA_LENGTH	22
#define MEMO_BUFFER_LENGTH	19

#define DATA_FRAME_HEADER   "uu"
#define DATA_FRAME_END		"ux"

enum DataFrameBuilder_states
{
	// Application's state machine's initial state. 
	DFB_STATE_INIT=0,
	DFB_STATE_IDLE,
	// TODO: Define states used by the application state machine.
    DFB_WAIT_UNTIL_FRAME_SENT,
	
    DFB_WAITING_DATA,
    DFB_EVAL_ENGINE_STATE,
	DFB_EVAL_DOORS_STATE,
	DFB_EVAL_AC_STATE,
	DFB_ADD_KIMAX_DATA,
	DFB_ADD_MEMO_DATA,
	DFB_ADD_OSIP_DATA_1,
	DFB_ADD_OSIP_DATA_2,
	DFB_ADD_ERROR,
	DFB_SENDING_DATA,
	
	DFB_WAKING_LISTENERS,
	DFB_SLEEP_LISTENERS,
	
	DFB_STATE_PAUSE,
	DFB_STATE_EXIT,
	DFB_STATE_CONTINUE

} ;

struct DataFrameBuilder_data {

	DataFrameBuilder_states state;
	
	tBOOL kimaxDataReceivedFlag;
	tBOOL  memoDataReceivedFlag;
};


EXTERN PUBLIC void vKimaxListener_Initialize();
EXTERN PUBLIC void vMemoListener_Initialize();

EXTERN PUBLIC void vKimaxListener_ProcessCode(string<KIMAX_DATA_LENGTH> sBuffer);
EXTERN PUBLIC void vMemoListener_ProcessCode(string<MEMO_DATA_LENGTH> sBuffer);

EXTERN PUBLIC void vDataFrameBuilder_Tasks();

EXTERN PUBLIC void vDataFrameBuilder_Initialize();

EXTERN PUBLIC tBOOL bDataFrameBuilder_Pause();
EXTERN PUBLIC void  vDataFrameBuilder_Continue();

EXTERN PUBLIC U8 U8DataFrameBuilder_GetIvuAlarm();
EXTERN PUBLIC string vDataFrameBuilder_getDataFrame();


PRIVATE void vAddToDataFrame(string data);

PRIVATE void vKimaxFrame_Reset();
PRIVATE void vMemoFrame_Reset();

PRIVATE void vKimax_SleepListener();
PRIVATE void vKimax_WakingListener();

PRIVATE void vMemo_SleepListener();
PRIVATE void vMemo_WakingListener();

#endif