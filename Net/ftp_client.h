#ifndef __FTP_CLIENT_H__
#define __FTP_CLIENT_H__

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

#include "StackFM\AppLayer.h"
#include "StackFM\timer.h"

#include "System\system_config.h"
#include "System\debug.h"

#define FTP_PACKET_SIZE 255

#define FTP_TIMEOUT     900


#define FTP_CTRL_PORT       "21"
#define FTP_CTRL_PORT_NUM   21

#define FTP_DATA_PORT       "20"
#define FTP_DATA_PORT_NUM   20



enum FTP_STATES
{
	/* Application's state machine's initial state. */
	FTP_STATE_INIT=0,
    FTP_STATE_IDLE,
    FTP_STATE_OPENPORT,
    FTP_STATE_WAITING_WELCOME,
    FTP_STATE_WAITING_PWD_REQUEST,
    FTP_STATE_WAITING_LOG_RESULT,
	FTP_STATE_WAITING_DATASOCK_READY,
    FTP_STATE_SENDING_PORT,
    FTP_STATE_LOGGED_ON,
    FTP_STATE_TRANSFERING_DATA,
    FTP_STATE_TRANSFER_COMPLETED,
    FTP_STATE_RESET,
    FTP_STATE_ERROR
	

};



// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */

struct FTP_DATA
{
    /* The application's current state */
    FTP_STATES state;
    string sUser;
    string sPass;
    tBOOL Transfer_ok;
	tBOOL fatal_error_ocurred;
    U8 errorCount;
	U32 bytes_commited;
    /* TODO: Define any additional data used by the application. */

} ;



EXTERN PUBLIC void vFTP_Initialize();
EXTERN PUBLIC void vFTP_Tasks();
EXTERN PUBLIC void vFTP_SendPacket();
EXTERN PUBLIC void vFTP_ProcessCode( string  last_ftp_code);
EXTERN PUBLIC void vFTP_Loggin(string username, string pass);
EXTERN PUBLIC void vFTP_Quit();
EXTERN PUBLIC void vFTP_Reset();
EXTERN PUBLIC void vFTP_Connect();

EXTERN PUBLIC void vFTP_StoreFile(string fn_orig, string fn_dest);

EXTERN PUBLIC tBOOL bFTP_isLoggedOn();
EXTERN PUBLIC tBOOL bFTP_ErrorFlagActive();
EXTERN PUBLIC tBOOL bFTP_isFileUploaded();


#endif