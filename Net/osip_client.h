#ifndef __OSIP_CLIENT_H__
#define __OSIP_CLIENT_H__


#include "..\System\system_config.h"
#include "..\System\date.h"
#include "..\System\cifms.h"

#include "..\StackFM\fm.h"
#include "..\dfb_fsm.h"
#include "..\mimo_fsm.h"

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************


#define ON		0xE0
#define OFF		0x0F

#define TELEGRAM_VEHICLE_ID 			0x01
#define TELEGRAM_DATE					0x10
#define TELEGRAM_JOURNEY_INFO			0x11
#define TELEGRAM_LINE_INFO				0x12
#define TELEGRAM_DRIVER_SESSION_INFO	0x13
#define TELEGRAM_START_JOURNEY			0x14
#define TELEGRAM_END_JOURNEY			0x15
#define TELEGRAM_ARRIVAL_STOP			0x16
#define TELEGRAM_DEPARTURE_STOP			0x17
#define TELEGRAM_PASSING_STOP			0x18
#define TELEGRAM_DEPOT_LEAVING			0x19
#define STATUS_REQUEST					0x30
#define STATUS_REPLY					0x31

//This byte marks the beginning of a message. 
//It has the value of 0x02.
#define STX			0x02
//This byte marks the end of the message. 
//It has the value of 0x03
#define ETX			0x03

// (ASCII char "M") for the memo box
#define MEMO_BOX	0x4D 
#define MEMO_BOX_ID	0x31 
/*The sender id is used if more than one device 
of a sender is connected to the system. In case 
that only one device is connected the field will 
be filled with 0x31 (ASCII char "1").*/

#define OSIP_CTRL_PORT       "2195"
#define OSIP_CTRL_PORT_NUM   2195

//EXTERN PUBLIC OSIP_data osip_data;


EXTERN PUBLIC void vOSIP_ClearData();
EXTERN PUBLIC void vOSIP_Initialize();
EXTERN PUBLIC void vOSIP_ResetComm();
EXTERN PUBLIC void vOSIP_ProcessCode( string  sBuffer);

EXTERN PUBLIC string<4> sOSIP_GetVehicleId();
EXTERN PUBLIC string<4> sOSIP_GetRouteId();
EXTERN PUBLIC string<4> sOSIP_GetDriverId();
EXTERN PUBLIC string<4> sOSIP_GetBusStopId();

EXTERN PUBLIC string<5> sOSIP_GetEventData();

PRIVATE string<4> toByteArray(U32* U32Number);

PRIVATE void U32_CORRECT_ENDIAN(U32* U32Value);

#endif