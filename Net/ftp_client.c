
#include "Net\ftp_client.h"


FTP_DATA ftpData;

tTMR_Handle FTP_TimeoutTimer;
// Esta rutina establece la comunicacion FTP por el puerto de control
U16 U16RestBytes; 
U32 U32LimitBytes;

//=============================================================================

PUBLIC void vFTP_Initialize(){							// Inicia la transferencia FTP por la comunicacion de control  	

	sock.num = FTP_CTRL_SOCK;			//This is the ctrl connection
	sock.allowedinterfaces	= "NET";
	sock.txbuffrq(1);
	sock.rxbuffrq(1);
	sock.protocol = PL_SOCK_PROTOCOL_TCP;
	sock.inconmode = PL_SOCK_INCONMODE_ANY_IP_ANY_PORT;
	sock.reconmode = PL_SOCK_RECONMODE_3; //original PL_SOCK_RECONMODE_1
	sock.connectiontout = 0;
	sock.targetip = IP_IVU;
	sock.localportlist = FTP_CTRL_PORT;
	sock.targetport = FTP_CTRL_PORT_NUM;

	sock.num = FTP_DATA_SOCK;			//This is the data connection
	sock.allowedinterfaces	= "NET";
    sock.txbuffrq(6);
    sock.rxbuffrq(1);
    sock.protocol = PL_SOCK_PROTOCOL_TCP;
    sock.inconmode = PL_SOCK_INCONMODE_ANY_IP_ANY_PORT;
    sock.reconmode = PL_SOCK_RECONMODE_3; //original PL_SOCK_RECONMODE_1
	sock.targetip = IP_IVU;
	sock.localportlist = FTP_DATA_PORT;
	sock.targetport = FTP_DATA_PORT_NUM;
	
	sys.buffalloc();

	ftpData.state = FTP_STATE_IDLE;

	vDBG_LogState(FtpClient, Info, "Init OK");
			
}

PUBLIC void vFTP_Tasks(){
	switch ( ftpData.state )
	 {
	 	case FTP_STATE_INIT:
        {
            ftpData.errorCount = 0;
			ftpData.fatal_error_ocurred = eFALSE;
            ftpData.state = FTP_STATE_IDLE;
            break;
        }
        case FTP_STATE_IDLE:
        {
            break;
        }

        case FTP_STATE_WAITING_WELCOME:
        case FTP_STATE_WAITING_PWD_REQUEST:
        case FTP_STATE_WAITING_LOG_RESULT:
        case FTP_STATE_SENDING_PORT:
        {
            if (bTMR_HasTimerExpired(FTP_TimeoutTimer) == eTRUE)
            {
			  vDBG_LogError(FtpClient, ErrTimeOut, "Error while waiting ftp message");
              ftpData.state = FTP_STATE_RESET;
            }
            break;
        }
		
		case FTP_STATE_WAITING_DATASOCK_READY:
		{
			sock.num = FTP_DATA_SOCK;
			if(sock.statesimple == PL_SSTS_EST){
				sock.notifysent(8);
				
				fd.filenum = NUM_FILE_DEFAULT;
				ftpData.bytes_commited = 0;
				
				U16RestBytes  = fd.filesize%FTP_PACKET_SIZE;
				U32LimitBytes = fd.filesize - U16RestBytes;
				
				ftpData.state = FTP_STATE_TRANSFERING_DATA;
				
			} else if (bTMR_HasTimerExpired(FTP_TimeoutTimer) == eTRUE)
            {
			  vDBG_LogError(FtpClient, ErrTimeOut, "Error while waiting for data sock ready");
              ftpData.state = FTP_STATE_RESET;
            }
			break;
		}


        case FTP_STATE_TRANSFER_COMPLETED:
        {
            ftpData.state = FTP_STATE_LOGGED_ON;
            break;
        }

        case FTP_STATE_RESET:
        {

        	sock.num = FTP_CTRL_SOCK;
			sock.close();

			ftpData.fatal_error_ocurred = eTRUE;
			ftpData.state = FTP_STATE_INIT;

			break;
        }

        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
	 }
}

PUBLIC void vFTP_ProcessCode( string sBuffer ){
    string last_ftp_code = left(sBuffer, 3);
	
	switch ( ftpData.state )
	{
		/* Application's initial state. */
		case FTP_STATE_WAITING_WELCOME:
		{
			if (last_ftp_code == "220" ){ //Command Channel is open so login
				
				sock.setdata("USER " + ftpData.sUser + "\r\n");
				sock.send();
				ftpData.state = FTP_STATE_WAITING_PWD_REQUEST;
				FTP_TimeoutTimer = hTMR_StartTimer(FTP_TIMEOUT);
			} else {
				vDBG_LogError(FtpClient, ErrUnknowCode, "Can't process message "+last_ftp_code);
				ftpData.state = FTP_STATE_RESET;
			}
			
			break;
		}
		case FTP_STATE_WAITING_PWD_REQUEST:
		{
			if (last_ftp_code == "331"){ //Password needed
				sock.setdata("PASS " + ftpData.sPass + "\r\n");
				sock.send();
				ftpData.state = FTP_STATE_WAITING_LOG_RESULT;
				FTP_TimeoutTimer = hTMR_StartTimer(FTP_TIMEOUT);
			}else {
				ftpData.state = FTP_STATE_RESET;
			}

			break;
		}
		case FTP_STATE_WAITING_LOG_RESULT:
		{
			 if (last_ftp_code ==  "230"){            //Login OK so transmit the data socket port number
				sock.setdata("PORT "+SYS_IP_CSV+ ",0,"+ FTP_DATA_PORT + "\r\n");   //(PARA)Use Port 20 for data instead Default Port 20      
				sock.send();
				ftpData.state = FTP_STATE_SENDING_PORT;
				FTP_TimeoutTimer = hTMR_StartTimer(FTP_TIMEOUT);
			 } else {
				vDBG_LogError(FtpClient, ErrUnableToAuthenticate, "Can't identify user on ftp server");
				ftpData.state = FTP_STATE_RESET;
			}
			break;
		}
		case FTP_STATE_SENDING_PORT:
		{
			if (last_ftp_code ==  "200"){ 		//Port OK so listen on  port and send STOR command
				ftpData.state = FTP_STATE_LOGGED_ON;
				FTP_TimeoutTimer = hTMR_StartTimer(FTP_TIMEOUT);
			} else {
				ftpData.state = FTP_STATE_RESET;
			}          
			break;
		}
		case FTP_STATE_LOGGED_ON:
		{
			if (last_ftp_code ==  "150"){ 		//Data Channel Ready so begin to transmit data
				
				ftpData.state = FTP_STATE_WAITING_DATASOCK_READY;
				
				FTP_TimeoutTimer = hTMR_StartTimer(FTP_TIMEOUT);

			}
			

			if (last_ftp_code ==  "221"){ 		//Good Bye
				sock.num = FTP_DATA_SOCK;
				sock.close();
				
				ftpData.state = FTP_STATE_IDLE;
			}      

			break;
		}
		case FTP_STATE_WAITING_DATASOCK_READY:
		{
			
			if (last_ftp_code ==  "425"){ 		//Can't open data connection for transfer o
				sock.setdata("ABOR\r\n");   //(PARA)Use Port 20 for data instead Default Port 20      
				sock.send();
				
				vDBG_LogError(FtpClient, ErrUnableToOpenConnection, "Can't open data connection for transfer");
				ftpData.state = FTP_STATE_RESET;
			}

			break;
		}
		case FTP_STATE_TRANSFERING_DATA:
		{
			if (last_ftp_code ==  "226"){ 		//Transfer OK

				ftpData.Transfer_ok = true;
				vFTP_Quit();
				vDBG_LogState(FtpClient, Info, "File Transfer OK");
				//cierra el archivo abierto
				fd.close();
				ftpData.state = FTP_STATE_LOGGED_ON;

			} else {
				ftpData.state = FTP_STATE_RESET;
			}    
			break;
		}
	   
		default:
		{
			
			break;
		}
	}

}

PUBLIC void vFTP_StoreFile( string fn_orig, string fn_dest ){
	if (ftpData.state == FTP_STATE_LOGGED_ON){
		
		//open the file
		fd.filenum=NUM_FILE_DEFAULT;
		if (fd.open(fn_orig) != PL_FD_STATUS_OK) {
			vDBG_LogError(FtpClient, ErrUnableToOpenConnection, "Can't open file");
			return;
		}
		
		sock.num = FTP_CTRL_SOCK;
		sock.setdata("STOR "+fn_dest+"\r\n");    //S_FILE_VEHID + S_FILE_TIME   
		sock.send();
		
		ftpData.Transfer_ok = false;
		ftpData.bytes_commited = 0;
		
	} else {
		vDBG_LogError(FtpClient, ErrUnableToOpenConnection, "Not be logged in FTP Server");
	} 
	
	vDBG_LogState(FtpClient, Info, "Store file in FTP Server");
	
	
}

PUBLIC void vFTP_Quit(){
	sock.num = FTP_CTRL_SOCK;
	sock.setdata("QUIT\r\n");        
	sock.send();
}

PUBLIC tBOOL bFTP_isLoggedOn(){
	if (ftpData.state == FTP_STATE_LOGGED_ON){
		return eTRUE;
	}
	return eFALSE;
}

PUBLIC tBOOL bFTP_isFileUploaded( ){
	return ftpData.Transfer_ok;
}

PUBLIC void vFTP_Loggin(string username, string pass){
	ftpData.sUser		= username;			//(PARA)User - usuari para registrarse en el sevidor FTP
    ftpData.sPass		= pass;				//(PARA)Password - Pass para registrarse en el sevidor FTP){
	vFTP_Connect();
}

PUBLIC void vFTP_Connect(){
	ftpData.Transfer_ok = false;
	
	sock.num = FTP_CTRL_SOCK;
	switch(sock.state){
		case PL_SST_CLOSED :
		case PL_SST_CL_PCLOSED:
		case PL_SST_CL_ACLOSED:
			sock.connect();
		break;
	} 
	
	ftpData.state = FTP_STATE_WAITING_WELCOME;
	FTP_TimeoutTimer = hTMR_StartTimer(FTP_TIMEOUT);
}

PUBLIC void vFTP_Reset(){
	ftpData.state = FTP_STATE_RESET;
}

PUBLIC tBOOL bFTP_ErrorFlagActive(){
	return ftpData.fatal_error_ocurred;
}

//Esta subrutina es la encargada de enviar los datos initerrupidamente por medio de FTP, se creo así ya que con el evento los datos no llegaban completos

PUBLIC void vFTP_SendPacket ()	{  //función que envía los datos al FTP interrumpidamente
	U8 size;
	if(ftpData.state == FTP_STATE_TRANSFERING_DATA){
		
		sock.num = FTP_DATA_SOCK;
		fd.setpointer(ftpData.bytes_commited+1);

		if (ftpData.bytes_commited < U32LimitBytes){
			size = FTP_PACKET_SIZE;
			
		} else if(ftpData.bytes_commited < fd.filesize){
			size = U16RestBytes;
			
		} else {
			sock.num = FTP_DATA_SOCK;
	   	    sock.close();
			return;
		}
		
		sock.setdata(fd.getdata(size)); //Send next packet
		sock.send();
		sock.notifysent(size);
		ftpData.bytes_commited += size;
	}
}


	        	