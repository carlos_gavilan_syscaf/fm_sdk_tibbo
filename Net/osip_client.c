#include "osip_client.h"

U32 VehicleId;
U32 RouteId;
U32 DriverId;
U32 BusStopId;
U32 TimeStamp;

U8 OnJourney;
U8 DepotLeaving;		//salida de patios
U8 ArrivalBusStop;
U8 DepartureBusStop;
U8 PassBusStop;

U16 port;

void vOSIP_ClearData(){
	
	VehicleId = 0;
	TimeStamp = 0;
	RouteId = 0;
	DriverId = 0;
	BusStopId = 0;
	DepotLeaving = 0;		//salida de patios
	OnJourney = 0;
	ArrivalBusStop  = 0;
	DepartureBusStop = 0;
	PassBusStop = 0;
}

void vOSIP_Initialize(){
		
	sock.num				= OSIP_SOCK;								//Socket por el cual se realiza la la comunicacion
	sock.allowedinterfaces	= "NET"	;
	sock.protocol			= PL_SOCK_PROTOCOL_UDP;						//Se define UDP como protocolo de comunicacion
	sock.inconmode			= PL_SOCK_INCONMODE_SPECIFIC_IP_ANY_PORT;	//Configura que reciva mensajes por cualquier puerto pero de una ip especifica
	sock.reconmode			= PL_SOCK_RECONMODE_0; 						//reconnects accepted from the same IP, any port, port switchover off
	sock.acceptbcast 		= YES;										//se configura para que acepte los mensajes que vienen de broadcast
	sock.targetip			= IP_IVU;									//Configura la ip de la que los mensajes van a llegar
	sock.localportlist		= OSIP_CTRL_PORT;							//Lista de puertos por los que va a escuchar 
	sock.targetport			= OSIP_CTRL_PORT_NUM;						//puerto por el que envia la informacion
	sock.connectiontout		= 0;
	
	
	vOSIP_ClearData();
	
	sock.txbuffrq(1);
	sock.rxbuffrq(1);
	sys.buffalloc();
	
	vDBG_LogState(OsipClient, Info, "Init OK");
}

void vOSIP_ResetComm(){
	sock.num = OSIP_SOCK;
	sock.reset();
}

void vOSIP_ProcessCode(string sBuffer){					

	U8 U8Head;						//Guardan el inicio y fin de trama para operaciones
	U8 U8Tail;
	U8 U8Len;
	U8 U8MsgType;

	U8* pU8Data;	
				
	pU8Data = sBuffer;
	U8Len 	= len(sBuffer) - 1; //*(pU8Data-1)
	U8Head		= *pU8Data; 		//Identifica el inicio de trama, deberia ser 2
	U8MsgType   = *(pU8Data+3);
	U8Tail		= *(pU8Data+U8Len);	//Identifica el fin de trama, deberia ser 3
	
	if (U8Head != STX || U8Tail != ETX) return; //si no son inicio y fin de trama volver

	
	switch(U8MsgType){	//almacena el tipo de mensaje para poder interpretarlo
		case TELEGRAM_VEHICLE_ID : 
			//En este telegrama se reciven el radio id y el id del vehiculo como el radio id no lo utilizamos solo se captura el id del vehiculo
			//Se crea una variable de 32 bits aunque el numero es de 5 bytes porque el lenguaje no permite una varialbe mas grande
			//El telegrama es el 0x01 Vehicle identification telegram (Radio id	4 byte, Metrocali vehicle number 5 byte)				
			//realmente son 5 bytes
			VehicleId = *(U32*)(pU8Data+11);// info 12 13 14 15	
			U32_CORRECT_ENDIAN(&VehicleId);
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------			
		case TELEGRAM_DATE : 
			// El telegrama es el 0x10 - Date/Time telegram (Timestamp 4 byte) - se obtiene la hora y la fecha en timestamp
			// en esta direccion se sabe que fecha y hora es http://unixtime-converter.com/
			// el formato timestamp son los segundos transcurridos desde el January 1st, 1970 at 00:00 UTC 
			U32 U32Date, U32Time, diff;
			
			TimeStamp = *(U32*)(pU8Data+6);
			U32_CORRECT_ENDIAN(&TimeStamp);

			if (!bFM_HasTimeSetOnFM()){//si no se ha enviado informacion del tiempo al fm
				
				if (!bMimo_HasTravelInProgress() 	&&
					bMimo_Sleep(900)){
					//ha terminado el viaje y se puede pausar la maquina de estados del MIMO?
					//pauser tambien DataFrameBuilder para evitar que reactive la maquina de estados MIMO
					bDataFrameBuilder_Pause();
					//habilita al TiOS para cambiar  los estados de las maquinas.
					doevents;
					
					getBcdTimeFromEpoch(TimeStamp, GMT_BOG, &U32Date, &U32Time);
					vFM_SetDateTime(&U32Date, &U32Time);
					
					//mantiene pausadas las maquinas hasta que el FM confirme que se ha cambiado la hora o la
					//maquina MIMO "despierte"
					//while(!bFM_HasTimeSetOnFM()) doevents;
				}
			} 
			
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------			
		case TELEGRAM_JOURNEY_INFO : 
			// El telegrama es el 0x11 - Journey (Block 	5 byte, Journey 4 byte,	Line 4 byte, Route direction 1 byte, Journey type 1 byte, No. of stops 2 byte)
			// De este telegrama se obtiene la ruta, del campo line
			RouteId = *(U32*)(pU8Data+11);
			U32_CORRECT_ENDIAN(&RouteId);
		break;									
//---------------------------------------------------------------------------------------------------------------------------------------------						
		case TELEGRAM_DRIVER_SESSION_INFO : 
			//El telegrama es el 0x13 - Driver session information telegram (Driver id 5 byte)
			//De este telegrama se obtiene la identificacion del conductor, se descarta el byte mas significativo ya que en el tibbo la variable maxima es de 4 bytes
			//realmente son 5 bytes
			DriverId = *(U32*)(pU8Data+7); //DriverId_aux
			U32_CORRECT_ENDIAN(&DriverId);
			
			
				/*if ( !bMimo_HasTripInProgress() 	&&
					 bMimo_Sleep(900)){ //ha terminado el viaje y se puede pausar la maquina de estados del MIMO y DFB??
					//habilita al TiOS para cambiar  los estados de las maquinas.
					doevents;
					
					//DriverId = DriverId_aux;
					vFM_IdentifyDriver((U16)DriverId);
					
					//mantiene pausadas las maquinas hasta que el FM confirme que se ha identificado el conductor
					//while(!bFM_HasDriverLoggedInFM()) doevents;
					
				} else {
					vDBG_LogError(OsipClient, ErrUnableToAuthenticate, "device is not ready to identify driver");
				}*/

		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------						
		case TELEGRAM_START_JOURNEY : 
			// El telegrama es el 0x14 - Start of Journey telegram (Timestamp 4 bytes)
			// empezo el itinerario
			OnJourney		= ON;				// se asigna este numero ya que en el script del FM se identifica ente como inucio de itinerario
			DepotLeaving		= OFF;		// Inicio de itinerario - Limpia salida de patios
		break;																								
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------						
		case TELEGRAM_END_JOURNEY : 
			//El telegrama es el 0x15 - End of Journey telegram (Timestamp 4 bytes)
			//finalizo el itinerario
			OnJourney		= OFF;	//se asigna este numero ya que en el script del FM se identifica ente como fin de itinerario
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------						
		case TELEGRAM_ARRIVAL_STOP : 
			//El telegrama es el 0x16 - Arrival at a stop telegram (Stopcode 5 bytes, Pos. in journey 2 bytes)
			//el vehiculo llego a una parada y se captura la parada 
			//osip_data.ArrivalBusStop	= ON;
			//realmente son 5 bytes
		    BusStopId = *(U32*)(pU8Data+7);
			U32_CORRECT_ENDIAN(&BusStopId);
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------												
		case TELEGRAM_DEPARTURE_STOP :
			//El telegrama es el 0x17 - Departure at a stop telegram (Stopcode 5 bytes, Pos. in journey 2 bytes, Stoptime 2 bytes)
			//vehiculo salio de una paradad y se captura la parada 
			ArrivalBusStop	= OFF;
			DepartureBusStop	= ON;
			//realmente son 5 bytes
			BusStopId = *(U32*)(pU8Data+7);
			U32_CORRECT_ENDIAN(&BusStopId);
			
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------												
		case TELEGRAM_PASSING_STOP :
			//El telegrama es el 0x18 - Passing a stop telegram (Stopcode 5 bytes, Pos. in journey 2 bytes, Stoptime 2 bytes)
			//vehiculo se paso de una parada que tenia que hacer
			PassBusStop	= ON;
			//realmente son 5 bytes
			BusStopId = *(U32*)(pU8Data+7);
			U32_CORRECT_ENDIAN(&BusStopId);
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------												
		case TELEGRAM_DEPOT_LEAVING : 
			//El telegrama es el 0x19 - Depot leaving telegram (Timestamp 4 bytes)
			//vehiculo salio de patios
			DepotLeaving	= ON;
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------												
		case STATUS_REQUEST :
			//El telegrama es el 0x30 - Status request telegram 
			//tiene que dar una respuesta al mismo por medio del telegrama 0x31 indicando el estado y las alarmas en linea
			string status_request_msg;					
			status_request_msg =chr(STX) 		 	+ 
								chr(MEMO_BOX) 	 	+
								chr(MEMO_BOX_ID) 	+ 
								chr(STATUS_REPLY) 	+ 
								chr(0x00) 		+  // Message length MSB
								chr(0x02) 		+  // Message length LSB
								chr(0x00) 		+  // Message MSB
								chr(U8DataFrameBuilder_GetIvuAlarm()) 	+  // Message LSB
								chr(0x00) 					+  // Checksum Not used, always send as 0x00
								chr(ETX);  //se arma la trama segun la sintaxis dada
			
			sock.num = OSIP_SOCK;				//Numero del socket por el que se va a enviar la informacion en este caso UDP
			sock.setdata(status_request_msg);	//Se carga la informacion en el buffer para enviarla
			sock.send();					//Se envia la informacion que se encuentra en el buffer 
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		default: //Si no llega ninguno de los telegramas conocidos se habilita la falla del telegrama
			//U8_UDP_ERROR = 64 				' Falla en la trama entrante de UDP no coincide con ningun telegrama
		break;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	}
}


string<4> sOSIP_GetVehicleId(){
	return toByteArray(&VehicleId);
}

string<4> sOSIP_GetRouteId(){
	return toByteArray(&RouteId);
}

string<4> sOSIP_GetDriverId(){
	return toByteArray(&DriverId);
}

string<4> sOSIP_GetBusStopId(){
	return toByteArray(&BusStopId);
}

string<5> sOSIP_GetEventData(){
	string<5> result =  chr(OnJourney)+
						chr(DepotLeaving)+
						chr(ArrivalBusStop)+
						chr(DepartureBusStop)+
						chr(PassBusStop);
	return result;
}

string<4> toByteArray(U32* U32Number){
	string<4> result ="";
	
	U8* pData_aux = (U8*)U32Number;
	result += chr(*pData_aux++); 
	result += chr(*pData_aux++);
	result += chr(*pData_aux++);
	result += chr(*pData_aux);
	
	return result;
}

void U32_CORRECT_ENDIAN(U32* U32Value){
	  *U32Value = 	((*U32Value&0xff000000)>>24)  |
					((*U32Value&0x00ff0000)>>8)   |
					((*U32Value&0x0000ff00)<<8)   |
					((*U32Value&0x000000ff)<<24);
}

U16 U16OSIP_GetDriverId(){
	return (U16) DriverId;
}